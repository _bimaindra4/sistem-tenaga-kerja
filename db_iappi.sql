/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : db_iappi

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 15/08/2020 17:09:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_asosiasi
-- ----------------------------
DROP TABLE IF EXISTS `tbl_asosiasi`;
CREATE TABLE `tbl_asosiasi`  (
  `id_asosiasi` int(11) NOT NULL AUTO_INCREMENT,
  `id_asosiasi_url` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama_asosiasi` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `singkatan` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id_asosiasi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_asosiasi
-- ----------------------------
INSERT INTO `tbl_asosiasi` VALUES (1, 'JVe6WsX8EFRg67WIOg2t', 'Asosiasi Ahli K3 Konstruksi Indonesia ', 'A2K4-I', '2020-03-05 09:43:17', NULL, 1);
INSERT INTO `tbl_asosiasi` VALUES (2, '0h4vpHY2K7BeHvHmnw9I', 'Asosiasi Keselamatan Kesehatan Kerja dan Lingkungan', 'AK3L', '2020-03-05 09:43:44', NULL, 1);
INSERT INTO `tbl_asosiasi` VALUES (3, 'm5PKF4UR6FPEeNBTPgi1', 'Asosiasi Profesi Tenaga Konstruksi Indonesia ', 'APTAKINDO', '2020-03-05 09:44:09', NULL, 1);
INSERT INTO `tbl_asosiasi` VALUES (4, 'k3crkV0qajInHmcHEcWm', 'Asosiasi Profesionalis Elektrikal-Mekanikal Indonesia ', 'APEI', '2020-03-05 09:45:12', NULL, 1);
INSERT INTO `tbl_asosiasi` VALUES (5, 'coGGDfKPltzfBDdm3Iy2', 'Asosiasi Sumber Daya Manusia Konstruksi Indonesia ', 'ASDAMKINDO', '2020-03-05 09:45:28', NULL, 1);

-- ----------------------------
-- Table structure for tbl_bimtek
-- ----------------------------
DROP TABLE IF EXISTS `tbl_bimtek`;
CREATE TABLE `tbl_bimtek`  (
  `id_bimtek` int(11) NOT NULL AUTO_INCREMENT,
  `id_bimtek_url` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_tenaker_jabatan` int(11) NULL DEFAULT NULL,
  `bimtek_nama` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tgl_mulai_bimtek` date NULL DEFAULT NULL,
  `tgl_selesai_bimtek` date NULL DEFAULT NULL,
  `deskripsi` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `status_bimtek` int(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id_bimtek`) USING BTREE,
  INDEX `bimtek_ke_jabatan`(`id_tenaker_jabatan`) USING BTREE,
  CONSTRAINT `bimtek_ke_jabatan` FOREIGN KEY (`id_tenaker_jabatan`) REFERENCES `tbl_tenaker_jabatan` (`id_tenaker_jabatan`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_bimtek
-- ----------------------------
INSERT INTO `tbl_bimtek` VALUES (1, 'l2SDFkQWrptfYodxTZcg', 1, 'Bimtek Ceka', '2020-05-02', '2020-05-09', 'cek', 2, '2020-03-05 17:51:02', '2020-08-09 16:37:02', 1);
INSERT INTO `tbl_bimtek` VALUES (2, 'KLAA123WrptfYodxBA09', 1, 'Bimtek Ceka 2', '2020-07-07', '2020-07-10', 'cek', 1, '2020-07-07 22:54:44', '2020-08-15 10:08:31', 1);

-- ----------------------------
-- Table structure for tbl_bimtek_kehadiran
-- ----------------------------
DROP TABLE IF EXISTS `tbl_bimtek_kehadiran`;
CREATE TABLE `tbl_bimtek_kehadiran`  (
  `id_bimtek_kehadiran` int(11) NOT NULL AUTO_INCREMENT,
  `id_bimtek_kehadiran_url` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_bimtek` int(11) NULL DEFAULT NULL,
  `tgl_kehadiran` date NULL DEFAULT NULL,
  `kegiatan` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id_bimtek_kehadiran`) USING BTREE,
  INDEX `kehadiran_ke_bimtek`(`id_bimtek`) USING BTREE,
  CONSTRAINT `kehadiran_ke_bimtek` FOREIGN KEY (`id_bimtek`) REFERENCES `tbl_bimtek` (`id_bimtek`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_bimtek_kehadiran
-- ----------------------------
INSERT INTO `tbl_bimtek_kehadiran` VALUES (25, 'f7XFRHInqbQZaU3JrTVl', 1, '2020-07-15', 'Pertemuan Awal', '2020-07-18 00:00:00', NULL, 1);
INSERT INTO `tbl_bimtek_kehadiran` VALUES (26, 'M4swJRjDcU6mAk3WQY5S', 1, '2020-07-16', 'Teori dan Praktek', '2020-07-18 00:00:00', NULL, 1);

-- ----------------------------
-- Table structure for tbl_bimtek_kehadiran_detail
-- ----------------------------
DROP TABLE IF EXISTS `tbl_bimtek_kehadiran_detail`;
CREATE TABLE `tbl_bimtek_kehadiran_detail`  (
  `id_bimtek_kehadiran` int(11) NULL DEFAULT NULL,
  `id_bimtek_peserta` int(11) NULL DEFAULT NULL,
  `status_hadir` enum('hadir','tidak_hadir','sakit','izin') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  INDEX `detail_hadir_ke_kehadiran`(`id_bimtek_kehadiran`) USING BTREE,
  INDEX `detail_hadir_ke_peserta`(`id_bimtek_peserta`) USING BTREE,
  CONSTRAINT `detail_hadir_ke_kehadiran` FOREIGN KEY (`id_bimtek_kehadiran`) REFERENCES `tbl_bimtek_kehadiran` (`id_bimtek_kehadiran`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detail_hadir_ke_peserta` FOREIGN KEY (`id_bimtek_peserta`) REFERENCES `tbl_bimtek_peserta` (`id_bimtek_peserta`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_bimtek_kehadiran_detail
-- ----------------------------
INSERT INTO `tbl_bimtek_kehadiran_detail` VALUES (25, 1, 'hadir');
INSERT INTO `tbl_bimtek_kehadiran_detail` VALUES (25, 2, 'hadir');
INSERT INTO `tbl_bimtek_kehadiran_detail` VALUES (25, 3, 'hadir');
INSERT INTO `tbl_bimtek_kehadiran_detail` VALUES (26, 1, 'hadir');
INSERT INTO `tbl_bimtek_kehadiran_detail` VALUES (26, 2, 'tidak_hadir');
INSERT INTO `tbl_bimtek_kehadiran_detail` VALUES (26, 3, 'tidak_hadir');

-- ----------------------------
-- Table structure for tbl_bimtek_nilai
-- ----------------------------
DROP TABLE IF EXISTS `tbl_bimtek_nilai`;
CREATE TABLE `tbl_bimtek_nilai`  (
  `id_bimtek_nilai` int(11) NOT NULL AUTO_INCREMENT,
  `id_bimtek_nilai_url` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_bimtek` int(11) NULL DEFAULT NULL,
  `id_bimtek_peserta` int(11) NULL DEFAULT NULL,
  `nilai_hadir` int(3) NULL DEFAULT NULL,
  `nilai_pretest` int(3) NULL DEFAULT NULL,
  `nilai_posttest` int(3) NULL DEFAULT NULL,
  `nilai_mandiri` int(3) NULL DEFAULT NULL,
  `nilai_akhir` int(3) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id_bimtek_nilai`) USING BTREE,
  INDEX `nilai_ke_bimtek`(`id_bimtek`) USING BTREE,
  INDEX `nilai_ke_peserta`(`id_bimtek_peserta`) USING BTREE,
  CONSTRAINT `nilai_ke_bimtek` FOREIGN KEY (`id_bimtek`) REFERENCES `tbl_bimtek` (`id_bimtek`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nilai_ke_peserta` FOREIGN KEY (`id_bimtek_peserta`) REFERENCES `tbl_bimtek_peserta` (`id_bimtek_peserta`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_bimtek_nilai
-- ----------------------------
INSERT INTO `tbl_bimtek_nilai` VALUES (1, 'l6hjAmQO3iqzW1n0VsRU', 1, 1, 100, 100, 100, 90, NULL, '2020-06-08 20:33:01', '2020-08-09 16:34:51', 1);
INSERT INTO `tbl_bimtek_nilai` VALUES (2, 'bY6DLHaA8z7mwQrRqeB9', 1, 2, 50, 80, 90, 100, NULL, '2020-06-08 20:33:01', '2020-08-09 16:34:51', 1);
INSERT INTO `tbl_bimtek_nilai` VALUES (3, 'Dngymwtk43rsC07VlAjM', 1, 3, 50, 100, 90, 90, NULL, '2020-06-08 20:33:01', '2020-08-09 16:34:51', 1);
INSERT INTO `tbl_bimtek_nilai` VALUES (8, 'U0D79Njfzy5idZaTCevx', 2, 6, 0, 0, 0, 0, NULL, '2020-07-19 12:39:26', NULL, 1);
INSERT INTO `tbl_bimtek_nilai` VALUES (9, 'IwBb7G82SjOgAELuQemR', 1, 1, 0, 0, 0, 0, NULL, '2020-08-09 16:36:56', NULL, 1);

-- ----------------------------
-- Table structure for tbl_bimtek_peserta
-- ----------------------------
DROP TABLE IF EXISTS `tbl_bimtek_peserta`;
CREATE TABLE `tbl_bimtek_peserta`  (
  `id_bimtek_peserta` int(11) NOT NULL AUTO_INCREMENT,
  `id_bimtek_peserta_url` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_bimtek` int(11) NULL DEFAULT NULL,
  `id_tenaga_kerja` int(11) NOT NULL,
  `status_peserta` int(1) NULL DEFAULT NULL,
  `status_lulus` int(1) NULL DEFAULT NULL,
  `status_kompeten` int(1) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id_bimtek_peserta`) USING BTREE,
  INDEX `peserta_ke_bimtek`(`id_bimtek`) USING BTREE,
  CONSTRAINT `peserta_ke_bimtek` FOREIGN KEY (`id_bimtek`) REFERENCES `tbl_bimtek` (`id_bimtek`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_bimtek_peserta
-- ----------------------------
INSERT INTO `tbl_bimtek_peserta` VALUES (1, '5HZfrnzB3X1y2Vw39vvH', 1, 4, 1, 1, 1, '2020-03-15 16:02:50', NULL, 1);
INSERT INTO `tbl_bimtek_peserta` VALUES (2, 'OiLAStDFueS7zTt8UJ2D', 1, 9, 1, 1, 1, '2020-03-15 16:03:10', NULL, 1);
INSERT INTO `tbl_bimtek_peserta` VALUES (3, 's4mdIuq8SW6swxlAhC72', 1, 14, 1, NULL, NULL, '2020-03-15 16:03:20', NULL, 1);
INSERT INTO `tbl_bimtek_peserta` VALUES (6, 'IkpNH0sMGF6o8L5PEZcb', 2, 4, 1, NULL, NULL, '2020-07-18 22:43:06', NULL, 1);

-- ----------------------------
-- Table structure for tbl_master_ap3i
-- ----------------------------
DROP TABLE IF EXISTS `tbl_master_ap3i`;
CREATE TABLE `tbl_master_ap3i`  (
  `id_ap3i` int(11) NOT NULL AUTO_INCREMENT,
  `id_ap3i_url` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ap3i_nama` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ap3i_username` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ap3i_password` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_at` datetime(0) NOT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id_ap3i`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_master_ap3i
-- ----------------------------
INSERT INTO `tbl_master_ap3i` VALUES (1, 'ARNTSJnOgELcTUN', 'AP3I Satu', 'ap3i', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2019-12-21 00:00:00', NULL, 1);

-- ----------------------------
-- Table structure for tbl_master_balai_jasa
-- ----------------------------
DROP TABLE IF EXISTS `tbl_master_balai_jasa`;
CREATE TABLE `tbl_master_balai_jasa`  (
  `id_balai_jasa` int(11) NOT NULL AUTO_INCREMENT,
  `id_balai_jasa_url` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `baljas_nama` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `baljas_username` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `baljas_password` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_at` datetime(0) NOT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id_balai_jasa`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_master_balai_jasa
-- ----------------------------
INSERT INTO `tbl_master_balai_jasa` VALUES (1, 'sHuz1wPdVDgaiQF', 'Balai Jasa Konstruksi Wil. III', 'balaijasa', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2019-12-21 00:00:00', NULL, 1);

-- ----------------------------
-- Table structure for tbl_master_iappi
-- ----------------------------
DROP TABLE IF EXISTS `tbl_master_iappi`;
CREATE TABLE `tbl_master_iappi`  (
  `id_iappi` int(11) NOT NULL AUTO_INCREMENT,
  `id_iappi_url` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `iappi_nama` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `iappi_alamat` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `iappi_tempat_lahir` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `iappi_tanggal_lahir` date NULL DEFAULT NULL,
  `iappi_jkel` enum('L','P') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `iappi_pendidikan` enum('SD','SMP','SMA','SMK','D1','D2','D3','D4','S1','S2') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `iappi_pekerjaan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `iappi_kewarganegaraan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `iappi_sertifikat_ahli` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `iappi_notelp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `iappi_email` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `iappi_username` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `iappi_password` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_at` datetime(0) NOT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id_iappi`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_master_iappi
-- ----------------------------
INSERT INTO `tbl_master_iappi` VALUES (1, 'z2RW0PtSjUOQ9bQ', 'IAPPI Satu', 'Malang', 'Malang', '1990-12-26', 'L', 'SMK', 'Pelajar', 'Indonesia', NULL, NULL, 'iappisatu@gmail.com', 'iappi', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2019-12-21 00:00:00', NULL, 1);
INSERT INTO `tbl_master_iappi` VALUES (2, 'rf8GQPyBxpvCVJ3', 'IAPPI Dua', 'Malang', 'Malang', '1970-01-01', 'L', 'SMK', 'Pelajar', 'Indonesia', NULL, '023210392019', 'iappidua@gmail.com', 'uY15Lp6a', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2019-12-26 21:26:55', NULL, 1);
INSERT INTO `tbl_master_iappi` VALUES (3, '6gojQ23dDRrkmpe', 'IAPPI Tiga', 'Malang', 'Malang', '1991-11-11', 'L', 'SMA', 'Pelajar', 'Indonesia', NULL, '213132131', 'iappitiga@gmail.com', 'y2jbDBxZ', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2019-12-26 21:31:56', NULL, 1);

-- ----------------------------
-- Table structure for tbl_master_pupr
-- ----------------------------
DROP TABLE IF EXISTS `tbl_master_pupr`;
CREATE TABLE `tbl_master_pupr`  (
  `id_kemenpupr` int(11) NOT NULL,
  `id_kemenpupr_url` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pupr_nama` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pupr_tempat_lhr` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pupr_tgl_lhr` date NULL DEFAULT NULL,
  `pupr_jkel` enum('L','P') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pupr_nohp` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pupr_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pupr_username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pupr_password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id_kemenpupr`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_master_pupr
-- ----------------------------
INSERT INTO `tbl_master_pupr` VALUES (1, '4HFWFeY531', 'KEMENPUPR', NULL, NULL, NULL, NULL, NULL, 'kemenpupr', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2020-01-17 10:44:56', NULL, 1);

-- ----------------------------
-- Table structure for tbl_master_tenaga_kerja
-- ----------------------------
DROP TABLE IF EXISTS `tbl_master_tenaga_kerja`;
CREATE TABLE `tbl_master_tenaga_kerja`  (
  `id_tenaga_kerja` int(11) NOT NULL AUTO_INCREMENT,
  `id_tenaga_kerja_url` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_tenaker_utusan` int(11) NULL DEFAULT NULL,
  `id_tenaker_jabatan` int(11) NULL DEFAULT NULL,
  `tenaker_nama` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tenaker_nik` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tenaker_alamat` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tenaker_provinsi` int(11) NULL DEFAULT NULL,
  `tenaker_kabkot` int(11) NULL DEFAULT NULL,
  `tenaker_kelurahan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tenaker_kodepos` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tenaker_tempat_lahir` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tenaker_tanggal_lahir` date NULL DEFAULT NULL,
  `tenaker_jkel` enum('L','P') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tenaker_kewarganegaraan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tenaker_npwp` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tenaker_no_npwp` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tenaker_proyek` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tenaker_pendidikan` enum('SD','SMP','SMA','SMK','D1','D2','D3','D4','S1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tenaker_institusi` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tenaker_jurusan` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tenaker_tanggal_lulus` date NULL DEFAULT NULL,
  `tenaker_email` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tenaker_nohp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tenaker_foto` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tenaker_ktp` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tenaker_ijazah` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tenaker_cv` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tenaker_username` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tenaker_password` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_at` datetime(0) NOT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id_tenaga_kerja`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 22 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_master_tenaga_kerja
-- ----------------------------
INSERT INTO `tbl_master_tenaga_kerja` VALUES (4, 'Sj7u5aa3lY08zOX', 6, 2, 'Ir. Abdul Rachman', '3510211308690004', 'JL JATI BENDUNGAN NO.6 RT / RW 006 / 003 KEL JATI KEC PULOGADUNG', 31, 3101, NULL, NULL, 'Jakarta', '1974-07-05', 'L', 'Indonesia', NULL, NULL, NULL, 'S1', NULL, NULL, '1970-01-01', NULL, '', NULL, NULL, NULL, NULL, 'tenagakerja', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2020-01-07 10:52:33', '2020-06-22 19:29:44', 1);
INSERT INTO `tbl_master_tenaga_kerja` VALUES (5, '4btxKpWDvoCAsUN', 2, 4, 'Achmad Alfi', '3175051007930005', 'JL H TAIMAN RT / RW 004 / 002 KEL GEDONG KEC PASAR REBO', 11, 1101, NULL, NULL, 'Jakarta', '1993-07-10', 'L', 'Indonesia', NULL, NULL, NULL, 'S1', NULL, NULL, '1970-01-01', NULL, '', NULL, NULL, NULL, NULL, '', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', '2020-01-07 11:13:16', '2020-02-20 11:25:48', 1);
INSERT INTO `tbl_master_tenaga_kerja` VALUES (6, 'jZpo14b9cEgzR30', 5, 1, 'Agus Budhikusumo', '3210072608650021', 'JL PAHLAWAN GG. SARKOSI NO.17 RT / RW 002 / 006 KEL MAJALENGKA KULON KEC MAJALENGKA', NULL, NULL, NULL, NULL, 'Majalengka', '1965-08-26', 'L', 'Indonesia', NULL, NULL, NULL, 'S1', NULL, NULL, '1970-01-01', NULL, '', NULL, NULL, NULL, NULL, 'BcbCfzU4', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2020-01-07 11:18:17', NULL, 1);
INSERT INTO `tbl_master_tenaga_kerja` VALUES (7, 'CkTBPV87LJ5giXf', 3, 1, 'Agung Laksono Jati', '', '', NULL, NULL, NULL, NULL, '', NULL, 'L', 'Indonesia', NULL, NULL, NULL, 'SD', NULL, NULL, '1970-01-01', NULL, '', NULL, NULL, NULL, NULL, 'MqnVGgW5', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2020-01-07 11:24:17', NULL, 1);
INSERT INTO `tbl_master_tenaga_kerja` VALUES (8, 'pOFXTl7tAJCQ26c', 4, 1, 'Agung Suparjono, ST', '', '', NULL, NULL, NULL, NULL, '', NULL, 'L', 'Indonesia', NULL, NULL, NULL, 'SD', NULL, NULL, '1970-01-01', NULL, '', NULL, NULL, NULL, NULL, '1sFLBQqZ', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2020-01-07 12:23:39', NULL, 1);
INSERT INTO `tbl_master_tenaga_kerja` VALUES (9, 'pRbE86aNeKuQvM1', 6, 6, 'Agust Harry Widodo Putro', '3510211308690004', 'Perumahan Erfina Kencana Regency Kv. Football 34, RT/RW 04/10, Naggewereh, Cibinong', NULL, NULL, NULL, NULL, 'Banyuwangi', NULL, 'L', 'Indonesia', NULL, NULL, NULL, 'S1', 'Universitas Muhammadiyah Jember', 'Teknik Sipil', '2000-12-01', NULL, '', NULL, NULL, NULL, NULL, 'dgynEAcT', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2020-01-07 12:29:58', NULL, 1);
INSERT INTO `tbl_master_tenaga_kerja` VALUES (10, '8RQabBFk06WviJg', 7, 2, 'Ahmad Bernadi', '', '', NULL, NULL, NULL, NULL, '', NULL, 'L', 'Indonesia', NULL, NULL, NULL, 'SD', NULL, NULL, '1970-01-01', NULL, '', NULL, NULL, NULL, NULL, 'qCzcNBYk', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2020-01-07 12:31:36', NULL, 1);
INSERT INTO `tbl_master_tenaga_kerja` VALUES (11, 'sB6Py2DUmEh7QSV', 8, 1, 'Ahmad Najmuddin', '', '', NULL, NULL, NULL, NULL, '', NULL, 'L', 'Indonesia', NULL, NULL, NULL, 'SD', NULL, NULL, '1970-01-01', NULL, '', NULL, NULL, NULL, NULL, 'nrdWCRJu', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2020-01-07 14:30:05', NULL, 1);
INSERT INTO `tbl_master_tenaga_kerja` VALUES (12, 'yZt7Ox4rs8XBPA9', 9, 1, 'Sri Pardiyanti', '3275111905680009', 'Kp. Cikting, RT/RW 03/11, Mustikajaya, Mustikajaya', 11, 1101, NULL, NULL, 'Klaten', '1978-08-09', 'P', 'Indonesia', NULL, NULL, NULL, 'S1', 'Universitas Sebelas Maret', 'Pendidikan Teknik Bangunan', '1970-01-01', NULL, '', NULL, NULL, NULL, NULL, '', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', '2020-01-07 14:32:49', '2020-02-17 16:35:02', 1);
INSERT INTO `tbl_master_tenaga_kerja` VALUES (13, 'ABd5qeNr7pcC2kS', 10, 1, 'Agung Prabowo', '3301130604970001', 'Ciawitali, RT/RW 02/04, Rejodadi, Cimanggu', NULL, NULL, NULL, NULL, 'Cilacap', '1997-06-04', 'L', 'Indonesia', NULL, NULL, NULL, 'S1', 'Universitas Muhammadiyah Yogyakarta', 'Teknik Sipil', '1970-01-01', NULL, '', NULL, NULL, NULL, NULL, 'x1A6FNiI', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2020-01-07 14:51:11', NULL, 1);
INSERT INTO `tbl_master_tenaga_kerja` VALUES (14, '5VZRivYFe32mx1B', 8, 3, 'Rezki Pratama', '1904061401920001', 'Jorong Pisau Hilang,  RT/RW 000/000, Lolo, Pantai Cermin', NULL, NULL, NULL, NULL, 'Jambi', NULL, 'L', 'Indonesia', NULL, NULL, NULL, 'SD', 'Universitas Putra Indonesia \"YPTK\"', 'Teknik Sipil', '2014-04-12', NULL, '', NULL, NULL, NULL, NULL, 'HQcAZVkL', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2020-01-07 14:53:20', NULL, 1);
INSERT INTO `tbl_master_tenaga_kerja` VALUES (15, 'CsPcKY75xfDd9pR', NULL, 1, 'Tri Haryono', '3175062011790010', 'Malaka Jaya, RT/RW 07/11, Rorotan, Cilincing', NULL, NULL, NULL, NULL, 'Sragen', NULL, 'L', 'Indonesia', NULL, NULL, NULL, 'S1', 'Universitas Muhammadiyah Surakarta', 'Teknik Sipil', '2005-03-06', NULL, '', NULL, NULL, NULL, NULL, 'sMukaROW', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2020-01-07 14:57:53', NULL, 1);

-- ----------------------------
-- Table structure for tbl_super_user
-- ----------------------------
DROP TABLE IF EXISTS `tbl_super_user`;
CREATE TABLE `tbl_super_user`  (
  `id_sadmin` int(11) NOT NULL AUTO_INCREMENT,
  `id_sadmin_url` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `sadmin_nama` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `sadmin_username` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `sadmin_password` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_at` datetime(0) NOT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id_sadmin`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_super_user
-- ----------------------------
INSERT INTO `tbl_super_user` VALUES (1, 'e3cfO', 'Super User', 'demo', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '2019-12-21 00:00:00', NULL, 1);

-- ----------------------------
-- Table structure for tbl_sys_kabkot
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sys_kabkot`;
CREATE TABLE `tbl_sys_kabkot`  (
  `id_kabkot` char(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `id_provinsi` char(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `kabkot_nama` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `longitude` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_kabkot`) USING BTREE,
  INDEX `regencies_province_id_index`(`id_provinsi`) USING BTREE,
  CONSTRAINT `kabkot_ke_prov` FOREIGN KEY (`id_provinsi`) REFERENCES `tbl_sys_provinsi` (`id_provinsi`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_sys_kabkot
-- ----------------------------
INSERT INTO `tbl_sys_kabkot` VALUES ('1101', '11', 'KABUPATEN SIMEULUE', '2.583333', '96.083333');
INSERT INTO `tbl_sys_kabkot` VALUES ('1102', '11', 'KABUPATEN ACEH SINGKIL', '2.3589459', '97.87216');
INSERT INTO `tbl_sys_kabkot` VALUES ('1103', '11', 'KABUPATEN ACEH SELATAN', '3.3115056', '97.3516558');
INSERT INTO `tbl_sys_kabkot` VALUES ('1104', '11', 'KABUPATEN ACEH TENGGARA', '3.3088666', '	97.6982272');
INSERT INTO `tbl_sys_kabkot` VALUES ('1105', '11', 'KABUPATEN ACEH TIMUR', '5.255443', '95.9885456');
INSERT INTO `tbl_sys_kabkot` VALUES ('1106', '11', 'KABUPATEN ACEH TENGAH', '	4.4482641', '96.8350999');
INSERT INTO `tbl_sys_kabkot` VALUES ('1107', '11', 'KABUPATEN ACEH BARAT', '4.4542745', '96.1526985');
INSERT INTO `tbl_sys_kabkot` VALUES ('1108', '11', 'KABUPATEN ACEH BESAR', '5.4529168', '95.4777811');
INSERT INTO `tbl_sys_kabkot` VALUES ('1109', '11', 'KABUPATEN PIDIE', '5.0742659', '	95.940971');
INSERT INTO `tbl_sys_kabkot` VALUES ('1110', '11', 'KABUPATEN BIREUEN', '5.18254', '96.89005');
INSERT INTO `tbl_sys_kabkot` VALUES ('1111', '11', 'KABUPATEN ACEH UTARA', '4.9786331', '97.2221421');
INSERT INTO `tbl_sys_kabkot` VALUES ('1112', '11', 'KABUPATEN ACEH BARAT DAYA', '3.0512643', '97.3368031');
INSERT INTO `tbl_sys_kabkot` VALUES ('1113', '11', 'KABUPATEN GAYO LUES', '3.955165', '97.3516558');
INSERT INTO `tbl_sys_kabkot` VALUES ('1114', '11', 'KABUPATEN ACEH TAMIANG', '4.2328871', '98.0028892');
INSERT INTO `tbl_sys_kabkot` VALUES ('1115', '11', 'KABUPATEN NAGAN RAYA', '4.1248406', '96.4929797');
INSERT INTO `tbl_sys_kabkot` VALUES ('1116', '11', 'KABUPATEN ACEH JAYA', '4.7873684', '95.6457951');
INSERT INTO `tbl_sys_kabkot` VALUES ('1117', '11', 'KABUPATEN BENER MERIAH', '	4.7748348', '97.0068393');
INSERT INTO `tbl_sys_kabkot` VALUES ('1118', '11', 'KABUPATEN PIDIE JAYA', '	5.1548063', '	96.195132');
INSERT INTO `tbl_sys_kabkot` VALUES ('1171', '11', 'KOTA BANDA ACEH', '5.55', '95.3166667');
INSERT INTO `tbl_sys_kabkot` VALUES ('1172', '11', 'KOTA SABANG', '	5.8946929', '	95.3192982');
INSERT INTO `tbl_sys_kabkot` VALUES ('1173', '11', 'KOTA LANGSA', '4.48', '97.9633333');
INSERT INTO `tbl_sys_kabkot` VALUES ('1174', '11', 'KOTA LHOKSEUMAWE', '5.1880556', '97.1402778');
INSERT INTO `tbl_sys_kabkot` VALUES ('1175', '11', 'KOTA SUBULUSSALAM', '2.6449927', '98.0165205');
INSERT INTO `tbl_sys_kabkot` VALUES ('1201', '12', 'KABUPATEN NIAS', '	-8.1712591', '113.7111274');
INSERT INTO `tbl_sys_kabkot` VALUES ('1202', '12', 'KABUPATEN MANDAILING NATAL', '0.7432372', '99.3673084');
INSERT INTO `tbl_sys_kabkot` VALUES ('1203', '12', 'KABUPATEN TAPANULI SELATAN', '1.5774933', '99.2785583');
INSERT INTO `tbl_sys_kabkot` VALUES ('1204', '12', 'KABUPATEN TAPANULI TENGAH', '1.8493299', '98.704075');
INSERT INTO `tbl_sys_kabkot` VALUES ('1205', '12', 'KABUPATEN TAPANULI UTARA', '	2.0405246', '99.1013498');
INSERT INTO `tbl_sys_kabkot` VALUES ('1206', '12', 'KABUPATEN TOBA SAMOSIR', '	2.3502398', '	99.2785583');
INSERT INTO `tbl_sys_kabkot` VALUES ('1207', '12', 'KABUPATEN LABUHAN BATU', '	2.3439863', '	100.1703257');
INSERT INTO `tbl_sys_kabkot` VALUES ('1208', '12', 'KABUPATEN ASAHAN', '2.8174722', '99.634135');
INSERT INTO `tbl_sys_kabkot` VALUES ('1209', '12', 'KABUPATEN SIMALUNGUN', '	2.9781612', '99.2785583');
INSERT INTO `tbl_sys_kabkot` VALUES ('1210', '12', 'KABUPATEN DAIRI', '	2.8675801', '98.265058');
INSERT INTO `tbl_sys_kabkot` VALUES ('1211', '12', 'KABUPATEN KARO', '3.1052909', '	98.265058');
INSERT INTO `tbl_sys_kabkot` VALUES ('1212', '12', 'KABUPATEN DELI SERDANG', '3.4201802', '98.704075');
INSERT INTO `tbl_sys_kabkot` VALUES ('1213', '12', 'KABUPATEN LANGKAT', '3.8653916', '	98.3088441');
INSERT INTO `tbl_sys_kabkot` VALUES ('1214', '12', 'KABUPATEN NIAS SELATAN', '0.7086091', '97.8286368');
INSERT INTO `tbl_sys_kabkot` VALUES ('1215', '12', 'KABUPATEN HUMBANG HASUNDUTAN', '	2.1988508', '	98.5721016');
INSERT INTO `tbl_sys_kabkot` VALUES ('1216', '12', 'KABUPATEN PAKPAK BHARAT', '2.545786', '98.299838');
INSERT INTO `tbl_sys_kabkot` VALUES ('1217', '12', 'KABUPATEN SAMOSIR', '	2.5833333', '98.8166667');
INSERT INTO `tbl_sys_kabkot` VALUES ('1218', '12', 'KABUPATEN SERDANG BEDAGAI', '3.3371694', '	99.0571089');
INSERT INTO `tbl_sys_kabkot` VALUES ('1219', '12', 'KABUPATEN BATU BARA', '	3.1740979', '	99.5006143');
INSERT INTO `tbl_sys_kabkot` VALUES ('1220', '12', 'KABUPATEN PADANG LAWAS UTARA', '1.5758644', '99.634135');
INSERT INTO `tbl_sys_kabkot` VALUES ('1221', '12', 'KABUPATEN PADANG LAWAS', '1.1186977', '99.8124935');
INSERT INTO `tbl_sys_kabkot` VALUES ('1222', '12', 'KABUPATEN LABUHAN BATU SELATAN', '	1.8799353', '	100.1703257');
INSERT INTO `tbl_sys_kabkot` VALUES ('1223', '12', 'KABUPATEN LABUHAN BATU UTARA', '	2.3465638', '	99.8124935');
INSERT INTO `tbl_sys_kabkot` VALUES ('1224', '12', 'KABUPATEN NIAS UTARA', '1.1255279', '97.5247243');
INSERT INTO `tbl_sys_kabkot` VALUES ('1225', '12', 'KABUPATEN NIAS BARAT', '	1.1255279', '97.5247243');
INSERT INTO `tbl_sys_kabkot` VALUES ('1271', '12', 'KOTA SIBOLGA', '1.7403745', '	98.7827988');
INSERT INTO `tbl_sys_kabkot` VALUES ('1272', '12', 'KOTA TANJUNG BALAI', '	2.965122', '99.800331');
INSERT INTO `tbl_sys_kabkot` VALUES ('1273', '12', 'KOTA PEMATANG SIANTAR', '2.96', '	99.06');
INSERT INTO `tbl_sys_kabkot` VALUES ('1274', '12', 'KOTA TEBING TINGGI', '3.3856205', '99.2009815');
INSERT INTO `tbl_sys_kabkot` VALUES ('1275', '12', 'KOTA MEDAN', '	3.585242', '	98.6755979');
INSERT INTO `tbl_sys_kabkot` VALUES ('1276', '12', 'KOTA BINJAI', '	3.594462', '	98.482246');
INSERT INTO `tbl_sys_kabkot` VALUES ('1277', '12', 'KOTA PADANGSIDIMPUAN', '	1.380424', '	99.273972');
INSERT INTO `tbl_sys_kabkot` VALUES ('1278', '12', 'KOTA GUNUNGSITOLI', '1.281964', '	97.61594');
INSERT INTO `tbl_sys_kabkot` VALUES ('1301', '13', 'KABUPATEN KEPULAUAN MENTAWAI', '-1.426001', '	98.9245343');
INSERT INTO `tbl_sys_kabkot` VALUES ('1302', '13', 'KABUPATEN PESISIR SELATAN', '	-1.7223147', '100.8903099');
INSERT INTO `tbl_sys_kabkot` VALUES ('1303', '13', 'KABUPATEN SOLOK', '-0.803027', '	100.644402');
INSERT INTO `tbl_sys_kabkot` VALUES ('1304', '13', 'KABUPATEN SIJUNJUNG', '	-0.6881586', '	100.997658');
INSERT INTO `tbl_sys_kabkot` VALUES ('1305', '13', 'KABUPATEN TANAH DATAR', '-0.4797043', '100.5746224');
INSERT INTO `tbl_sys_kabkot` VALUES ('1306', '13', 'KABUPATEN PADANG PARIAMAN', '-0.5546757', '100.2151578');
INSERT INTO `tbl_sys_kabkot` VALUES ('1307', '13', 'KABUPATEN AGAM', '	-0.2209392', '100.1703257');
INSERT INTO `tbl_sys_kabkot` VALUES ('1308', '13', 'KABUPATEN LIMA PULUH KOTA', '3.168216', '99.4187929');
INSERT INTO `tbl_sys_kabkot` VALUES ('1309', '13', 'KABUPATEN PASAMAN', '	0.1288752', '99.7901781');
INSERT INTO `tbl_sys_kabkot` VALUES ('1310', '13', 'KABUPATEN SOLOK SELATAN', '	-1.4157329', '101.2523792');
INSERT INTO `tbl_sys_kabkot` VALUES ('1311', '13', 'KABUPATEN DHARMASRAYA', '-1.1120568', '101.6157773');
INSERT INTO `tbl_sys_kabkot` VALUES ('1312', '13', 'KABUPATEN PASAMAN BARAT', '0.2213005', '99.634135');
INSERT INTO `tbl_sys_kabkot` VALUES ('1371', '13', 'KOTA PADANG', '	-0.95', '	100.3530556');
INSERT INTO `tbl_sys_kabkot` VALUES ('1372', '13', 'KOTA SOLOK', '	-0.803027', '100.644402');
INSERT INTO `tbl_sys_kabkot` VALUES ('1373', '13', 'KOTA SAWAH LUNTO', '-0.6810286', '100.7763604');
INSERT INTO `tbl_sys_kabkot` VALUES ('1374', '13', 'KOTA PADANG PANJANG', '-0.470679', '100.4059456');
INSERT INTO `tbl_sys_kabkot` VALUES ('1375', '13', 'KOTA BUKITTINGGI', '	-0.3055556', '100.3691667');
INSERT INTO `tbl_sys_kabkot` VALUES ('1376', '13', 'KOTA PAYAKUMBUH', '-0.22887', '100.632301');
INSERT INTO `tbl_sys_kabkot` VALUES ('1377', '13', 'KOTA PARIAMAN', '	-0.6264389', '100.1179574');
INSERT INTO `tbl_sys_kabkot` VALUES ('1401', '14', 'KABUPATEN KUANTAN SINGINGI', '	-0.4411596', '101.5248055');
INSERT INTO `tbl_sys_kabkot` VALUES ('1402', '14', 'KABUPATEN INDRAGIRI HULU', '-0.7361181', '	102.2547919');
INSERT INTO `tbl_sys_kabkot` VALUES ('1403', '14', 'KABUPATEN INDRAGIRI HILIR', '	-0.1456733', '	102.989615');
INSERT INTO `tbl_sys_kabkot` VALUES ('1404', '14', 'KABUPATEN PELALAWAN', '	0.441415', '102.088699');
INSERT INTO `tbl_sys_kabkot` VALUES ('1405', '14', 'KABUPATEN S I A K', '	-0.789275', '113.921327');
INSERT INTO `tbl_sys_kabkot` VALUES ('1406', '14', 'KABUPATEN KAMPAR', '	0.146671', '101.1617356');
INSERT INTO `tbl_sys_kabkot` VALUES ('1407', '14', 'KABUPATEN ROKAN HULU', '1.0410934', '100.439656');
INSERT INTO `tbl_sys_kabkot` VALUES ('1408', '14', 'KABUPATEN BENGKALIS', '	1.4897222', '	102.0797222');
INSERT INTO `tbl_sys_kabkot` VALUES ('1409', '14', 'KABUPATEN ROKAN HILIR', '1.6463978', '100.8000051');
INSERT INTO `tbl_sys_kabkot` VALUES ('1410', '14', 'KABUPATEN KEPULAUAN MERANTI', '0.9208765', '	102.6675575');
INSERT INTO `tbl_sys_kabkot` VALUES ('1471', '14', 'KOTA PEKANBARU', '	0.5333333', '101.45');
INSERT INTO `tbl_sys_kabkot` VALUES ('1473', '14', 'KOTA D U M A I', '	1.665742', '101.447601');
INSERT INTO `tbl_sys_kabkot` VALUES ('1501', '15', 'KABUPATEN KERINCI', '	-1.697', '101.264');
INSERT INTO `tbl_sys_kabkot` VALUES ('1502', '15', 'KABUPATEN MERANGIN', '-2.1752789', '101.9804613');
INSERT INTO `tbl_sys_kabkot` VALUES ('1503', '15', 'KABUPATEN SAROLANGUN', '-2.2654937', '102.6905326');
INSERT INTO `tbl_sys_kabkot` VALUES ('1504', '15', 'KABUPATEN BATANG HARI', '-1.7083922', '103.0817903');
INSERT INTO `tbl_sys_kabkot` VALUES ('1505', '15', 'KABUPATEN MUARO JAMBI', '-1.596672', '103.615799');
INSERT INTO `tbl_sys_kabkot` VALUES ('1506', '15', 'KABUPATEN TANJUNG JABUNG TIMUR', '	-1.3291599', '	103.89973');
INSERT INTO `tbl_sys_kabkot` VALUES ('1507', '15', 'KABUPATEN TANJUNG JABUNG BARAT', '	-1.2332122', '	103.7984428');
INSERT INTO `tbl_sys_kabkot` VALUES ('1508', '15', 'KABUPATEN TEBO', '	-1.2592999', '	102.3463875');
INSERT INTO `tbl_sys_kabkot` VALUES ('1509', '15', 'KABUPATEN BUNGO', '	-1.6401338', '101.8891721');
INSERT INTO `tbl_sys_kabkot` VALUES ('1571', '15', 'KOTA JAMBI', '	-1.596672', '	103.615799');
INSERT INTO `tbl_sys_kabkot` VALUES ('1572', '15', 'KOTA SUNGAI PENUH', '-2.06314', '	101.387199');
INSERT INTO `tbl_sys_kabkot` VALUES ('1601', '16', 'KABUPATEN OGAN KOMERING ULU', '-4.0283486', '104.0072348');
INSERT INTO `tbl_sys_kabkot` VALUES ('1602', '16', 'KABUPATEN OGAN KOMERING ILIR', '	-3.4559744', '	105.2194808');
INSERT INTO `tbl_sys_kabkot` VALUES ('1603', '16', 'KABUPATEN MUARA ENIM', '-3.651581', '103.770798');
INSERT INTO `tbl_sys_kabkot` VALUES ('1604', '16', 'KABUPATEN LAHAT', '	-3.7863889', '	103.5427778');
INSERT INTO `tbl_sys_kabkot` VALUES ('1605', '16', 'KABUPATEN MUSI RAWAS', '-2.8625305', '102.989615');
INSERT INTO `tbl_sys_kabkot` VALUES ('1606', '16', 'KABUPATEN MUSI BANYUASIN', '-2.5442029', '	103.7289167');
INSERT INTO `tbl_sys_kabkot` VALUES ('1607', '16', 'KABUPATEN BANYU ASIN', '-2.6095639', '	104.7520939');
INSERT INTO `tbl_sys_kabkot` VALUES ('1608', '16', 'KABUPATEN OGAN KOMERING ULU SELATAN', '-4.6681951', '104.0072348');
INSERT INTO `tbl_sys_kabkot` VALUES ('1609', '16', 'KABUPATEN OGAN KOMERING ULU TIMUR', '	-3.8567934', '	104.7520939');
INSERT INTO `tbl_sys_kabkot` VALUES ('1610', '16', 'KABUPATEN OGAN ILIR', '-3.426544', '	104.6121475');
INSERT INTO `tbl_sys_kabkot` VALUES ('1611', '16', 'KABUPATEN EMPAT LAWANG', '	-3.7286029', '102.8975098');
INSERT INTO `tbl_sys_kabkot` VALUES ('1612', '16', 'KABUPATEN PENUKAL ABAB LEMATANG ILIR', '-3.204829', '103.982567');
INSERT INTO `tbl_sys_kabkot` VALUES ('1613', '16', 'KABUPATEN MUSI RAWAS UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1671', '16', 'KOTA PALEMBANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1672', '16', 'KOTA PRABUMULIH', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1673', '16', 'KOTA PAGAR ALAM', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1674', '16', 'KOTA LUBUKLINGGAU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1701', '17', 'KABUPATEN BENGKULU SELATAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1702', '17', 'KABUPATEN REJANG LEBONG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1703', '17', 'KABUPATEN BENGKULU UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1704', '17', 'KABUPATEN KAUR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1705', '17', 'KABUPATEN SELUMA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1706', '17', 'KABUPATEN MUKOMUKO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1707', '17', 'KABUPATEN LEBONG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1708', '17', 'KABUPATEN KEPAHIANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1709', '17', 'KABUPATEN BENGKULU TENGAH', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1771', '17', 'KOTA BENGKULU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1801', '18', 'KABUPATEN LAMPUNG BARAT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1802', '18', 'KABUPATEN TANGGAMUS', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1803', '18', 'KABUPATEN LAMPUNG SELATAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1804', '18', 'KABUPATEN LAMPUNG TIMUR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1805', '18', 'KABUPATEN LAMPUNG TENGAH', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1806', '18', 'KABUPATEN LAMPUNG UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1807', '18', 'KABUPATEN WAY KANAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1808', '18', 'KABUPATEN TULANGBAWANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1809', '18', 'KABUPATEN PESAWARAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1810', '18', 'KABUPATEN PRINGSEWU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1811', '18', 'KABUPATEN MESUJI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1812', '18', 'KABUPATEN TULANG BAWANG BARAT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1813', '18', 'KABUPATEN PESISIR BARAT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1871', '18', 'KOTA BANDAR LAMPUNG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1872', '18', 'KOTA METRO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1901', '19', 'KABUPATEN BANGKA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1902', '19', 'KABUPATEN BELITUNG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1903', '19', 'KABUPATEN BANGKA BARAT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1904', '19', 'KABUPATEN BANGKA TENGAH', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1905', '19', 'KABUPATEN BANGKA SELATAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1906', '19', 'KABUPATEN BELITUNG TIMUR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('1971', '19', 'KOTA PANGKAL PINANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('2101', '21', 'KABUPATEN KARIMUN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('2102', '21', 'KABUPATEN BINTAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('2103', '21', 'KABUPATEN NATUNA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('2104', '21', 'KABUPATEN LINGGA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('2105', '21', 'KABUPATEN KEPULAUAN ANAMBAS', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('2171', '21', 'KOTA B A T A M', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('2172', '21', 'KOTA TANJUNG PINANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3101', '31', 'KABUPATEN KEPULAUAN SERIBU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3171', '31', 'KOTA JAKARTA SELATAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3172', '31', 'KOTA JAKARTA TIMUR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3173', '31', 'KOTA JAKARTA PUSAT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3174', '31', 'KOTA JAKARTA BARAT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3175', '31', 'KOTA JAKARTA UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3201', '32', 'KABUPATEN BOGOR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3202', '32', 'KABUPATEN SUKABUMI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3203', '32', 'KABUPATEN CIANJUR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3204', '32', 'KABUPATEN BANDUNG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3205', '32', 'KABUPATEN GARUT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3206', '32', 'KABUPATEN TASIKMALAYA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3207', '32', 'KABUPATEN CIAMIS', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3208', '32', 'KABUPATEN KUNINGAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3209', '32', 'KABUPATEN CIREBON', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3210', '32', 'KABUPATEN MAJALENGKA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3211', '32', 'KABUPATEN SUMEDANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3212', '32', 'KABUPATEN INDRAMAYU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3213', '32', 'KABUPATEN SUBANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3214', '32', 'KABUPATEN PURWAKARTA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3215', '32', 'KABUPATEN KARAWANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3216', '32', 'KABUPATEN BEKASI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3217', '32', 'KABUPATEN BANDUNG BARAT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3218', '32', 'KABUPATEN PANGANDARAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3271', '32', 'KOTA BOGOR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3272', '32', 'KOTA SUKABUMI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3273', '32', 'KOTA BANDUNG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3274', '32', 'KOTA CIREBON', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3275', '32', 'KOTA BEKASI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3276', '32', 'KOTA DEPOK', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3277', '32', 'KOTA CIMAHI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3278', '32', 'KOTA TASIKMALAYA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3279', '32', 'KOTA BANJAR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3301', '33', 'KABUPATEN CILACAP', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3302', '33', 'KABUPATEN BANYUMAS', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3303', '33', 'KABUPATEN PURBALINGGA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3304', '33', 'KABUPATEN BANJARNEGARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3305', '33', 'KABUPATEN KEBUMEN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3306', '33', 'KABUPATEN PURWOREJO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3307', '33', 'KABUPATEN WONOSOBO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3308', '33', 'KABUPATEN MAGELANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3309', '33', 'KABUPATEN BOYOLALI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3310', '33', 'KABUPATEN KLATEN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3311', '33', 'KABUPATEN SUKOHARJO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3312', '33', 'KABUPATEN WONOGIRI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3313', '33', 'KABUPATEN KARANGANYAR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3314', '33', 'KABUPATEN SRAGEN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3315', '33', 'KABUPATEN GROBOGAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3316', '33', 'KABUPATEN BLORA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3317', '33', 'KABUPATEN REMBANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3318', '33', 'KABUPATEN PATI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3319', '33', 'KABUPATEN KUDUS', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3320', '33', 'KABUPATEN JEPARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3321', '33', 'KABUPATEN DEMAK', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3322', '33', 'KABUPATEN SEMARANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3323', '33', 'KABUPATEN TEMANGGUNG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3324', '33', 'KABUPATEN KENDAL', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3325', '33', 'KABUPATEN BATANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3326', '33', 'KABUPATEN PEKALONGAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3327', '33', 'KABUPATEN PEMALANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3328', '33', 'KABUPATEN TEGAL', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3329', '33', 'KABUPATEN BREBES', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3371', '33', 'KOTA MAGELANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3372', '33', 'KOTA SURAKARTA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3373', '33', 'KOTA SALATIGA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3374', '33', 'KOTA SEMARANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3375', '33', 'KOTA PEKALONGAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3376', '33', 'KOTA TEGAL', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3401', '34', 'KABUPATEN KULON PROGO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3402', '34', 'KABUPATEN BANTUL', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3403', '34', 'KABUPATEN GUNUNG KIDUL', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3404', '34', 'KABUPATEN SLEMAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3471', '34', 'KOTA YOGYAKARTA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3501', '35', 'KABUPATEN PACITAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3502', '35', 'KABUPATEN PONOROGO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3503', '35', 'KABUPATEN TRENGGALEK', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3504', '35', 'KABUPATEN TULUNGAGUNG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3505', '35', 'KABUPATEN BLITAR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3506', '35', 'KABUPATEN KEDIRI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3507', '35', 'KABUPATEN MALANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3508', '35', 'KABUPATEN LUMAJANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3509', '35', 'KABUPATEN JEMBER', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3510', '35', 'KABUPATEN BANYUWANGI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3511', '35', 'KABUPATEN BONDOWOSO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3512', '35', 'KABUPATEN SITUBONDO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3513', '35', 'KABUPATEN PROBOLINGGO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3514', '35', 'KABUPATEN PASURUAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3515', '35', 'KABUPATEN SIDOARJO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3516', '35', 'KABUPATEN MOJOKERTO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3517', '35', 'KABUPATEN JOMBANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3518', '35', 'KABUPATEN NGANJUK', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3519', '35', 'KABUPATEN MADIUN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3520', '35', 'KABUPATEN MAGETAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3521', '35', 'KABUPATEN NGAWI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3522', '35', 'KABUPATEN BOJONEGORO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3523', '35', 'KABUPATEN TUBAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3524', '35', 'KABUPATEN LAMONGAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3525', '35', 'KABUPATEN GRESIK', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3526', '35', 'KABUPATEN BANGKALAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3527', '35', 'KABUPATEN SAMPANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3528', '35', 'KABUPATEN PAMEKASAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3529', '35', 'KABUPATEN SUMENEP', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3571', '35', 'KOTA KEDIRI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3572', '35', 'KOTA BLITAR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3573', '35', 'KOTA MALANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3574', '35', 'KOTA PROBOLINGGO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3575', '35', 'KOTA PASURUAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3576', '35', 'KOTA MOJOKERTO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3577', '35', 'KOTA MADIUN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3578', '35', 'KOTA SURABAYA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3579', '35', 'KOTA BATU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3601', '36', 'KABUPATEN PANDEGLANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3602', '36', 'KABUPATEN LEBAK', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3603', '36', 'KABUPATEN TANGERANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3604', '36', 'KABUPATEN SERANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3671', '36', 'KOTA TANGERANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3672', '36', 'KOTA CILEGON', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3673', '36', 'KOTA SERANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('3674', '36', 'KOTA TANGERANG SELATAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5101', '51', 'KABUPATEN JEMBRANA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5102', '51', 'KABUPATEN TABANAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5103', '51', 'KABUPATEN BADUNG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5104', '51', 'KABUPATEN GIANYAR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5105', '51', 'KABUPATEN KLUNGKUNG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5106', '51', 'KABUPATEN BANGLI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5107', '51', 'KABUPATEN KARANG ASEM', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5108', '51', 'KABUPATEN BULELENG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5171', '51', 'KOTA DENPASAR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5201', '52', 'KABUPATEN LOMBOK BARAT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5202', '52', 'KABUPATEN LOMBOK TENGAH', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5203', '52', 'KABUPATEN LOMBOK TIMUR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5204', '52', 'KABUPATEN SUMBAWA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5205', '52', 'KABUPATEN DOMPU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5206', '52', 'KABUPATEN BIMA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5207', '52', 'KABUPATEN SUMBAWA BARAT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5208', '52', 'KABUPATEN LOMBOK UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5271', '52', 'KOTA MATARAM', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5272', '52', 'KOTA BIMA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5301', '53', 'KABUPATEN SUMBA BARAT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5302', '53', 'KABUPATEN SUMBA TIMUR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5303', '53', 'KABUPATEN KUPANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5304', '53', 'KABUPATEN TIMOR TENGAH SELATAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5305', '53', 'KABUPATEN TIMOR TENGAH UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5306', '53', 'KABUPATEN BELU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5307', '53', 'KABUPATEN ALOR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5308', '53', 'KABUPATEN LEMBATA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5309', '53', 'KABUPATEN FLORES TIMUR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5310', '53', 'KABUPATEN SIKKA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5311', '53', 'KABUPATEN ENDE', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5312', '53', 'KABUPATEN NGADA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5313', '53', 'KABUPATEN MANGGARAI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5314', '53', 'KABUPATEN ROTE NDAO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5315', '53', 'KABUPATEN MANGGARAI BARAT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5316', '53', 'KABUPATEN SUMBA TENGAH', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5317', '53', 'KABUPATEN SUMBA BARAT DAYA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5318', '53', 'KABUPATEN NAGEKEO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5319', '53', 'KABUPATEN MANGGARAI TIMUR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5320', '53', 'KABUPATEN SABU RAIJUA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5321', '53', 'KABUPATEN MALAKA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('5371', '53', 'KOTA KUPANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6101', '61', 'KABUPATEN SAMBAS', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6102', '61', 'KABUPATEN BENGKAYANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6103', '61', 'KABUPATEN LANDAK', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6104', '61', 'KABUPATEN MEMPAWAH', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6105', '61', 'KABUPATEN SANGGAU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6106', '61', 'KABUPATEN KETAPANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6107', '61', 'KABUPATEN SINTANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6108', '61', 'KABUPATEN KAPUAS HULU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6109', '61', 'KABUPATEN SEKADAU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6110', '61', 'KABUPATEN MELAWI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6111', '61', 'KABUPATEN KAYONG UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6112', '61', 'KABUPATEN KUBU RAYA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6171', '61', 'KOTA PONTIANAK', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6172', '61', 'KOTA SINGKAWANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6201', '62', 'KABUPATEN KOTAWARINGIN BARAT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6202', '62', 'KABUPATEN KOTAWARINGIN TIMUR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6203', '62', 'KABUPATEN KAPUAS', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6204', '62', 'KABUPATEN BARITO SELATAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6205', '62', 'KABUPATEN BARITO UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6206', '62', 'KABUPATEN SUKAMARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6207', '62', 'KABUPATEN LAMANDAU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6208', '62', 'KABUPATEN SERUYAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6209', '62', 'KABUPATEN KATINGAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6210', '62', 'KABUPATEN PULANG PISAU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6211', '62', 'KABUPATEN GUNUNG MAS', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6212', '62', 'KABUPATEN BARITO TIMUR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6213', '62', 'KABUPATEN MURUNG RAYA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6271', '62', 'KOTA PALANGKA RAYA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6301', '63', 'KABUPATEN TANAH LAUT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6302', '63', 'KABUPATEN KOTA BARU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6303', '63', 'KABUPATEN BANJAR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6304', '63', 'KABUPATEN BARITO KUALA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6305', '63', 'KABUPATEN TAPIN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6306', '63', 'KABUPATEN HULU SUNGAI SELATAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6307', '63', 'KABUPATEN HULU SUNGAI TENGAH', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6308', '63', 'KABUPATEN HULU SUNGAI UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6309', '63', 'KABUPATEN TABALONG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6310', '63', 'KABUPATEN TANAH BUMBU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6311', '63', 'KABUPATEN BALANGAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6371', '63', 'KOTA BANJARMASIN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6372', '63', 'KOTA BANJAR BARU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6401', '64', 'KABUPATEN PASER', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6402', '64', 'KABUPATEN KUTAI BARAT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6403', '64', 'KABUPATEN KUTAI KARTANEGARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6404', '64', 'KABUPATEN KUTAI TIMUR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6405', '64', 'KABUPATEN BERAU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6409', '64', 'KABUPATEN PENAJAM PASER UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6411', '64', 'KABUPATEN MAHAKAM HULU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6471', '64', 'KOTA BALIKPAPAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6472', '64', 'KOTA SAMARINDA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6474', '64', 'KOTA BONTANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6501', '65', 'KABUPATEN MALINAU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6502', '65', 'KABUPATEN BULUNGAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6503', '65', 'KABUPATEN TANA TIDUNG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6504', '65', 'KABUPATEN NUNUKAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('6571', '65', 'KOTA TARAKAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7101', '71', 'KABUPATEN BOLAANG MONGONDOW', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7102', '71', 'KABUPATEN MINAHASA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7103', '71', 'KABUPATEN KEPULAUAN SANGIHE', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7104', '71', 'KABUPATEN KEPULAUAN TALAUD', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7105', '71', 'KABUPATEN MINAHASA SELATAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7106', '71', 'KABUPATEN MINAHASA UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7107', '71', 'KABUPATEN BOLAANG MONGONDOW UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7108', '71', 'KABUPATEN SIAU TAGULANDANG BIARO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7109', '71', 'KABUPATEN MINAHASA TENGGARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7110', '71', 'KABUPATEN BOLAANG MONGONDOW SELATAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7111', '71', 'KABUPATEN BOLAANG MONGONDOW TIMUR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7171', '71', 'KOTA MANADO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7172', '71', 'KOTA BITUNG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7173', '71', 'KOTA TOMOHON', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7174', '71', 'KOTA KOTAMOBAGU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7201', '72', 'KABUPATEN BANGGAI KEPULAUAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7202', '72', 'KABUPATEN BANGGAI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7203', '72', 'KABUPATEN MOROWALI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7204', '72', 'KABUPATEN POSO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7205', '72', 'KABUPATEN DONGGALA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7206', '72', 'KABUPATEN TOLI-TOLI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7207', '72', 'KABUPATEN BUOL', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7208', '72', 'KABUPATEN PARIGI MOUTONG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7209', '72', 'KABUPATEN TOJO UNA-UNA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7210', '72', 'KABUPATEN SIGI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7211', '72', 'KABUPATEN BANGGAI LAUT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7212', '72', 'KABUPATEN MOROWALI UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7271', '72', 'KOTA PALU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7301', '73', 'KABUPATEN KEPULAUAN SELAYAR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7302', '73', 'KABUPATEN BULUKUMBA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7303', '73', 'KABUPATEN BANTAENG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7304', '73', 'KABUPATEN JENEPONTO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7305', '73', 'KABUPATEN TAKALAR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7306', '73', 'KABUPATEN GOWA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7307', '73', 'KABUPATEN SINJAI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7308', '73', 'KABUPATEN MAROS', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7309', '73', 'KABUPATEN PANGKAJENE DAN KEPULAUAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7310', '73', 'KABUPATEN BARRU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7311', '73', 'KABUPATEN BONE', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7312', '73', 'KABUPATEN SOPPENG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7313', '73', 'KABUPATEN WAJO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7314', '73', 'KABUPATEN SIDENRENG RAPPANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7315', '73', 'KABUPATEN PINRANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7316', '73', 'KABUPATEN ENREKANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7317', '73', 'KABUPATEN LUWU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7318', '73', 'KABUPATEN TANA TORAJA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7322', '73', 'KABUPATEN LUWU UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7325', '73', 'KABUPATEN LUWU TIMUR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7326', '73', 'KABUPATEN TORAJA UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7371', '73', 'KOTA MAKASSAR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7372', '73', 'KOTA PAREPARE', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7373', '73', 'KOTA PALOPO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7401', '74', 'KABUPATEN BUTON', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7402', '74', 'KABUPATEN MUNA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7403', '74', 'KABUPATEN KONAWE', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7404', '74', 'KABUPATEN KOLAKA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7405', '74', 'KABUPATEN KONAWE SELATAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7406', '74', 'KABUPATEN BOMBANA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7407', '74', 'KABUPATEN WAKATOBI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7408', '74', 'KABUPATEN KOLAKA UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7409', '74', 'KABUPATEN BUTON UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7410', '74', 'KABUPATEN KONAWE UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7411', '74', 'KABUPATEN KOLAKA TIMUR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7412', '74', 'KABUPATEN KONAWE KEPULAUAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7413', '74', 'KABUPATEN MUNA BARAT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7414', '74', 'KABUPATEN BUTON TENGAH', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7415', '74', 'KABUPATEN BUTON SELATAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7471', '74', 'KOTA KENDARI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7472', '74', 'KOTA BAUBAU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7501', '75', 'KABUPATEN BOALEMO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7502', '75', 'KABUPATEN GORONTALO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7503', '75', 'KABUPATEN POHUWATO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7504', '75', 'KABUPATEN BONE BOLANGO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7505', '75', 'KABUPATEN GORONTALO UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7571', '75', 'KOTA GORONTALO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7601', '76', 'KABUPATEN MAJENE', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7602', '76', 'KABUPATEN POLEWALI MANDAR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7603', '76', 'KABUPATEN MAMASA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7604', '76', 'KABUPATEN MAMUJU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7605', '76', 'KABUPATEN MAMUJU UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('7606', '76', 'KABUPATEN MAMUJU TENGAH', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8101', '81', 'KABUPATEN MALUKU TENGGARA BARAT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8102', '81', 'KABUPATEN MALUKU TENGGARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8103', '81', 'KABUPATEN MALUKU TENGAH', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8104', '81', 'KABUPATEN BURU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8105', '81', 'KABUPATEN KEPULAUAN ARU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8106', '81', 'KABUPATEN SERAM BAGIAN BARAT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8107', '81', 'KABUPATEN SERAM BAGIAN TIMUR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8108', '81', 'KABUPATEN MALUKU BARAT DAYA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8109', '81', 'KABUPATEN BURU SELATAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8171', '81', 'KOTA AMBON', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8172', '81', 'KOTA TUAL', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8201', '82', 'KABUPATEN HALMAHERA BARAT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8202', '82', 'KABUPATEN HALMAHERA TENGAH', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8203', '82', 'KABUPATEN KEPULAUAN SULA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8204', '82', 'KABUPATEN HALMAHERA SELATAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8205', '82', 'KABUPATEN HALMAHERA UTARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8206', '82', 'KABUPATEN HALMAHERA TIMUR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8207', '82', 'KABUPATEN PULAU MOROTAI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8208', '82', 'KABUPATEN PULAU TALIABU', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8271', '82', 'KOTA TERNATE', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('8272', '82', 'KOTA TIDORE KEPULAUAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9101', '91', 'KABUPATEN FAKFAK', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9102', '91', 'KABUPATEN KAIMANA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9103', '91', 'KABUPATEN TELUK WONDAMA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9104', '91', 'KABUPATEN TELUK BINTUNI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9105', '91', 'KABUPATEN MANOKWARI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9106', '91', 'KABUPATEN SORONG SELATAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9107', '91', 'KABUPATEN SORONG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9108', '91', 'KABUPATEN RAJA AMPAT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9109', '91', 'KABUPATEN TAMBRAUW', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9110', '91', 'KABUPATEN MAYBRAT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9111', '91', 'KABUPATEN MANOKWARI SELATAN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9112', '91', 'KABUPATEN PEGUNUNGAN ARFAK', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9171', '91', 'KOTA SORONG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9401', '94', 'KABUPATEN MERAUKE', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9402', '94', 'KABUPATEN JAYAWIJAYA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9403', '94', 'KABUPATEN JAYAPURA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9404', '94', 'KABUPATEN NABIRE', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9408', '94', 'KABUPATEN KEPULAUAN YAPEN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9409', '94', 'KABUPATEN BIAK NUMFOR', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9410', '94', 'KABUPATEN PANIAI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9411', '94', 'KABUPATEN PUNCAK JAYA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9412', '94', 'KABUPATEN MIMIKA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9413', '94', 'KABUPATEN BOVEN DIGOEL', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9414', '94', 'KABUPATEN MAPPI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9415', '94', 'KABUPATEN ASMAT', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9416', '94', 'KABUPATEN YAHUKIMO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9417', '94', 'KABUPATEN PEGUNUNGAN BINTANG', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9418', '94', 'KABUPATEN TOLIKARA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9419', '94', 'KABUPATEN SARMI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9420', '94', 'KABUPATEN KEEROM', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9426', '94', 'KABUPATEN WAROPEN', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9427', '94', 'KABUPATEN SUPIORI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9428', '94', 'KABUPATEN MAMBERAMO RAYA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9429', '94', 'KABUPATEN NDUGA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9430', '94', 'KABUPATEN LANNY JAYA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9431', '94', 'KABUPATEN MAMBERAMO TENGAH', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9432', '94', 'KABUPATEN YALIMO', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9433', '94', 'KABUPATEN PUNCAK', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9434', '94', 'KABUPATEN DOGIYAI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9435', '94', 'KABUPATEN INTAN JAYA', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9436', '94', 'KABUPATEN DEIYAI', NULL, NULL);
INSERT INTO `tbl_sys_kabkot` VALUES ('9471', '94', 'KOTA JAYAPURA', NULL, NULL);

-- ----------------------------
-- Table structure for tbl_sys_provinsi
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sys_provinsi`;
CREATE TABLE `tbl_sys_provinsi`  (
  `id_provinsi` char(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `provinsi_nama` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_provinsi`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_sys_provinsi
-- ----------------------------
INSERT INTO `tbl_sys_provinsi` VALUES ('11', 'ACEH');
INSERT INTO `tbl_sys_provinsi` VALUES ('12', 'SUMATERA UTARA');
INSERT INTO `tbl_sys_provinsi` VALUES ('13', 'SUMATERA BARAT');
INSERT INTO `tbl_sys_provinsi` VALUES ('14', 'RIAU');
INSERT INTO `tbl_sys_provinsi` VALUES ('15', 'JAMBI');
INSERT INTO `tbl_sys_provinsi` VALUES ('16', 'SUMATERA SELATAN');
INSERT INTO `tbl_sys_provinsi` VALUES ('17', 'BENGKULU');
INSERT INTO `tbl_sys_provinsi` VALUES ('18', 'LAMPUNG');
INSERT INTO `tbl_sys_provinsi` VALUES ('19', 'KEPULAUAN BANGKA BELITUNG');
INSERT INTO `tbl_sys_provinsi` VALUES ('21', 'KEPULAUAN RIAU');
INSERT INTO `tbl_sys_provinsi` VALUES ('31', 'DKI JAKARTA');
INSERT INTO `tbl_sys_provinsi` VALUES ('32', 'JAWA BARAT');
INSERT INTO `tbl_sys_provinsi` VALUES ('33', 'JAWA TENGAH');
INSERT INTO `tbl_sys_provinsi` VALUES ('34', 'DI YOGYAKARTA');
INSERT INTO `tbl_sys_provinsi` VALUES ('35', 'JAWA TIMUR');
INSERT INTO `tbl_sys_provinsi` VALUES ('36', 'BANTEN');
INSERT INTO `tbl_sys_provinsi` VALUES ('51', 'BALI');
INSERT INTO `tbl_sys_provinsi` VALUES ('52', 'NUSA TENGGARA BARAT');
INSERT INTO `tbl_sys_provinsi` VALUES ('53', 'NUSA TENGGARA TIMUR');
INSERT INTO `tbl_sys_provinsi` VALUES ('61', 'KALIMANTAN BARAT');
INSERT INTO `tbl_sys_provinsi` VALUES ('62', 'KALIMANTAN TENGAH');
INSERT INTO `tbl_sys_provinsi` VALUES ('63', 'KALIMANTAN SELATAN');
INSERT INTO `tbl_sys_provinsi` VALUES ('64', 'KALIMANTAN TIMUR');
INSERT INTO `tbl_sys_provinsi` VALUES ('65', 'KALIMANTAN UTARA');
INSERT INTO `tbl_sys_provinsi` VALUES ('71', 'SULAWESI UTARA');
INSERT INTO `tbl_sys_provinsi` VALUES ('72', 'SULAWESI TENGAH');
INSERT INTO `tbl_sys_provinsi` VALUES ('73', 'SULAWESI SELATAN');
INSERT INTO `tbl_sys_provinsi` VALUES ('74', 'SULAWESI TENGGARA');
INSERT INTO `tbl_sys_provinsi` VALUES ('75', 'GORONTALO');
INSERT INTO `tbl_sys_provinsi` VALUES ('76', 'SULAWESI BARAT');
INSERT INTO `tbl_sys_provinsi` VALUES ('81', 'MALUKU');
INSERT INTO `tbl_sys_provinsi` VALUES ('82', 'MALUKU UTARA');
INSERT INTO `tbl_sys_provinsi` VALUES ('91', 'PAPUA BARAT');
INSERT INTO `tbl_sys_provinsi` VALUES ('94', 'PAPUA');

-- ----------------------------
-- Table structure for tbl_sys_role
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sys_role`;
CREATE TABLE `tbl_sys_role`  (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `id_role_url` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `role_nama` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `role_order` int(3) NOT NULL,
  `created_at` datetime(0) NOT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id_role`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_sys_role
-- ----------------------------
INSERT INTO `tbl_sys_role` VALUES (1, 'XlFwMqSXEz', 'Kemenpu PR', 1, '2019-12-21 00:00:00', NULL, 1);
INSERT INTO `tbl_sys_role` VALUES (2, 'hokeL80SVl', 'AP3I', 2, '2019-12-21 00:00:00', NULL, 1);
INSERT INTO `tbl_sys_role` VALUES (3, 'CZXcCV8TIO', 'IAPPI', 3, '2019-12-21 00:00:00', NULL, 1);
INSERT INTO `tbl_sys_role` VALUES (4, 'OsRfNHj4Zn', 'Balai Jasa', 4, '2019-12-21 00:00:00', NULL, 1);
INSERT INTO `tbl_sys_role` VALUES (5, '9olXqvsRgN', 'Tenaga Kerja', 5, '2019-12-21 00:00:00', NULL, 1);
INSERT INTO `tbl_sys_role` VALUES (6, '60tnNqd0Ox', 'Super Admin', 1, '2019-12-21 00:00:00', NULL, 1);

-- ----------------------------
-- Table structure for tbl_sys_role_detail
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sys_role_detail`;
CREATE TABLE `tbl_sys_role_detail`  (
  `id_user` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `user_table` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_sys_role_detail
-- ----------------------------
INSERT INTO `tbl_sys_role_detail` VALUES (1, 2, NULL, '2019-12-21 00:00:00', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (1, 3, NULL, '2019-12-21 00:00:00', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (1, 4, NULL, '2019-12-21 00:00:00', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (6, 5, NULL, '2020-01-07 11:18:17', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (1, 6, NULL, '2019-12-21 00:00:00', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (4, 5, NULL, '0000-00-00 00:00:00', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (5, 5, NULL, '2020-01-07 11:13:16', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (7, 5, NULL, '2020-01-07 11:24:17', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (8, 5, NULL, '2020-01-07 12:23:39', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (9, 5, NULL, '2020-01-07 12:29:58', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (10, 5, NULL, '2020-01-07 12:31:36', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (11, 5, NULL, '2020-01-07 14:30:05', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (12, 5, NULL, '2020-01-07 14:32:49', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (13, 5, NULL, '2020-01-07 14:51:11', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (14, 5, NULL, '2020-01-07 14:53:20', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (15, 5, NULL, '2020-01-07 14:57:53', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (1, 1, NULL, '0000-00-00 00:00:00', 0);
INSERT INTO `tbl_sys_role_detail` VALUES (16, 5, NULL, '2020-02-11 23:10:24', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (17, 5, NULL, '2020-02-11 23:10:24', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (18, 5, NULL, '2020-02-11 23:13:05', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (19, 5, NULL, '2020-02-11 23:15:51', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (20, 5, NULL, '2020-02-11 23:17:27', 1);
INSERT INTO `tbl_sys_role_detail` VALUES (21, 5, NULL, '2020-02-11 23:19:26', 1);

-- ----------------------------
-- Table structure for tbl_sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sys_role_menu`;
CREATE TABLE `tbl_sys_role_menu`  (
  `id_role_menu` int(11) NOT NULL AUTO_INCREMENT,
  `id_role_menu_url` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `menu_parent` int(11) NULL DEFAULT NULL,
  `menu_nama` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `menu_icon` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `menu_url` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_at` datetime(0) NOT NULL,
  `modified_at` datetime(0) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id_role_menu`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_sys_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_sys_role_menu_group
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sys_role_menu_group`;
CREATE TABLE `tbl_sys_role_menu_group`  (
  `id_role_menu_group` int(11) NOT NULL AUTO_INCREMENT,
  `id_role_menu_group_url` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `menu_group_nama` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `sorting` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `modified_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_role_menu_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_sys_role_menu_group
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_sys_role_menu_privileges
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sys_role_menu_privileges`;
CREATE TABLE `tbl_sys_role_menu_privileges`  (
  `id_role_menu_privileges` int(11) NOT NULL AUTO_INCREMENT,
  `id_role_menu_privileges_url` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `id_role_menu_group` int(11) NULL DEFAULT NULL,
  `id_role` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `modified_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_role_menu_privileges`) USING BTREE,
  INDEX `tbl_sys_role_menu_privileges_ibfk_2`(`id_role`) USING BTREE,
  INDEX `id_role_menu_group`(`id_role_menu_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_sys_role_menu_privileges
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_tenaker_jabatan
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tenaker_jabatan`;
CREATE TABLE `tbl_tenaker_jabatan`  (
  `id_tenaker_jabatan` int(11) NOT NULL AUTO_INCREMENT,
  `id_tenaker_jabatan_url` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jabatan_nama` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jabatan_klasifikasi` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jabatan_kualifikasi` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id_tenaker_jabatan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_tenaker_jabatan
-- ----------------------------
INSERT INTO `tbl_tenaker_jabatan` VALUES (1, 'BahReEiKJV', 'Ahli Supervisi Terowongan', 'Sipil', 'Ahli', '2019-12-28 09:57:34', NULL, 1);
INSERT INTO `tbl_tenaker_jabatan` VALUES (2, 'EYQAPBa7kl', 'Ahli Desain Terowongan SDA', 'Sipil', 'Ahli', '2019-12-28 09:58:06', NULL, 1);
INSERT INTO `tbl_tenaker_jabatan` VALUES (3, 'C16T3bi6iF', 'Ahli Mutu Pekerjaan Sumber Daya Air', 'Sipil', 'Ahli', '2019-12-28 09:58:43', NULL, 1);
INSERT INTO `tbl_tenaker_jabatan` VALUES (4, 'DQ62hSnp78', 'Ahli Desain Interior', 'Arsitektur', 'Ahli', '2020-01-27 22:05:07', NULL, 1);
INSERT INTO `tbl_tenaker_jabatan` VALUES (5, 'ft7YsQh3Di', 'Ahli Supervisi Konstruksi Jaringan Irigasi', 'Sipil', 'Ahli', '2020-01-27 22:07:38', NULL, 1);
INSERT INTO `tbl_tenaker_jabatan` VALUES (6, 'VDetUCzk6J', 'Ahli Supervisi Struktur Bangunan Irigasi', 'Sipil', 'Ahli', '2020-01-27 22:11:53', NULL, 1);
INSERT INTO `tbl_tenaker_jabatan` VALUES (7, '8t1cHsFjhC', 'Ahli Muda Perencana Irigasi', 'Sipil', 'Ahli', '2020-01-27 22:13:19', NULL, 1);

-- ----------------------------
-- Table structure for tbl_tenaker_sertifikat
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tenaker_sertifikat`;
CREATE TABLE `tbl_tenaker_sertifikat`  (
  `id_tenaker_sertifikat` int(11) NOT NULL AUTO_INCREMENT,
  `id_tenaker_sertifikat_url` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_tenaga_kerja` int(11) NULL DEFAULT NULL,
  `id_tenaker_jabatan` int(11) NULL DEFAULT NULL,
  `sertifikat_jenis` enum('ahli','terampil') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sertifikat_nomor` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sertifikat_tanggal` date NULL DEFAULT NULL,
  `sertifikat_asosiasi` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sertifikat_file` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id_tenaker_sertifikat`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_tenaker_sertifikat
-- ----------------------------
INSERT INTO `tbl_tenaker_sertifikat` VALUES (3, '7GowbiTyX9mOWAI8nNc0', 4, 1, 'ahli', NULL, '2023-08-09', NULL, NULL, '2020-08-09 20:23:22', NULL, 1);
INSERT INTO `tbl_tenaker_sertifikat` VALUES (4, 'z3gC5qf2BG6QwxSEnkRX', 9, 1, 'ahli', NULL, '2020-08-09', NULL, NULL, '2020-08-09 20:23:22', NULL, 1);

-- ----------------------------
-- Table structure for tbl_tenaker_utusan
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tenaker_utusan`;
CREATE TABLE `tbl_tenaker_utusan`  (
  `id_tenaker_utusan` int(11) NOT NULL AUTO_INCREMENT,
  `id_tenaker_utusan_url` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `utusan_tenaker` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `utusan_alamat` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `utusan_lat` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `utusan_long` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id_tenaker_utusan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tbl_tenaker_utusan
-- ----------------------------
INSERT INTO `tbl_tenaker_utusan` VALUES (1, 'vWBKwQjzsHBm9VvNylXb', 'Dinas Perumahan RKP DKI', NULL, '-6.180081', '106.811214', '2020-01-07 14:59:47', NULL, 1);
INSERT INTO `tbl_tenaker_utusan` VALUES (2, 'WZaXnMQwhYzBuHA2Epv9', 'PT Amarta Karya', NULL, '-6.243600', '106.998630', '2020-01-07 14:59:50', NULL, 1);
INSERT INTO `tbl_tenaker_utusan` VALUES (3, 'EfNbjIEQgMw3gF8FoqNe', 'PT. Brantas Abipraya (UIII Depok)', NULL, '-6.243600', '106.998630', '2020-01-07 15:03:26', NULL, 1);
INSERT INTO `tbl_tenaker_utusan` VALUES (4, 'TVMNYaZZLo1GHuGePHU5', 'Dinas Perumahan Rakyat dan Kawasan Permukiman Provinsi DKI Jakarta', NULL, '-6.180088', '106.811233', '2020-01-07 15:03:48', NULL, 1);
INSERT INTO `tbl_tenaker_utusan` VALUES (5, 'DH3quo3iaNkf7zSNjDKF', 'SMKN 1 Majalengka', NULL, '-6.825016', '108.249979', '2020-01-07 15:04:10', NULL, 1);
INSERT INTO `tbl_tenaker_utusan` VALUES (6, 'boKjYyhDeS2x29vEL5qL', 'PT. Rekayasa Industri', NULL, '-6.259386', '106.845807', '2020-01-07 15:04:25', NULL, 1);
INSERT INTO `tbl_tenaker_utusan` VALUES (7, 'kG56AqTFHH8bYHuU28ke', 'PT Nindya Karya (Persero) Wilayah I Medan', NULL, '3.537323', '98.708480', '2020-01-07 15:05:04', NULL, 1);
INSERT INTO `tbl_tenaker_utusan` VALUES (8, 'bvyinF4Eh5dpHLFNEliG', 'PT. Acset Indonusa', NULL, '-6.169740', '106.820958', '2020-01-07 15:05:28', NULL, 1);
INSERT INTO `tbl_tenaker_utusan` VALUES (9, 'va8OWAJnGxDza0bLhQgr', 'SMKN 1 Cikarang Barat', NULL, '-6.275802', '107.088740', '2020-01-07 15:05:42', NULL, 1);
INSERT INTO `tbl_tenaker_utusan` VALUES (10, 'LRstD3wz2aPYnJYisczQ', 'PT. Wijaya Karya', NULL, '-6.242182', '106.876595', '2020-01-07 15:05:57', NULL, 1);
INSERT INTO `tbl_tenaker_utusan` VALUES (11, 'L38CvybwZIE3qMm3ki9g', 'PT. Adhi Karya', NULL, '-6.265448', '106.845286', '2020-01-07 15:06:17', NULL, 1);

-- ----------------------------
-- Table structure for tbl_tenaker_utusan_detail
-- ----------------------------
DROP TABLE IF EXISTS `tbl_tenaker_utusan_detail`;
CREATE TABLE `tbl_tenaker_utusan_detail`  (
  `id_tenaker_utusan_detail` int(11) NOT NULL,
  `id_tenaker_utusan` int(11) NULL DEFAULT NULL,
  `id_tenaga_kerja` int(11) NULL DEFAULT NULL,
  `jabatan` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tgl_mulai` date NULL DEFAULT NULL,
  `tgl_akhir` date NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `modified_at` datetime(0) NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id_tenaker_utusan_detail`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_tenaker_utusan_detail
-- ----------------------------
INSERT INTO `tbl_tenaker_utusan_detail` VALUES (1, 6, 4, 'Mandor', '2019-12-09', '2020-07-06', '2020-08-08 20:49:38', NULL, NULL);
INSERT INTO `tbl_tenaker_utusan_detail` VALUES (2, 2, 5, NULL, NULL, NULL, '2020-08-08 20:49:55', NULL, NULL);
INSERT INTO `tbl_tenaker_utusan_detail` VALUES (3, 5, 6, NULL, NULL, NULL, '2020-08-08 20:50:23', NULL, NULL);
INSERT INTO `tbl_tenaker_utusan_detail` VALUES (4, 3, 7, NULL, NULL, NULL, '2020-08-08 20:51:50', NULL, NULL);
INSERT INTO `tbl_tenaker_utusan_detail` VALUES (5, 4, 8, NULL, NULL, NULL, '2020-08-08 20:51:50', NULL, NULL);
INSERT INTO `tbl_tenaker_utusan_detail` VALUES (6, 6, 9, NULL, NULL, NULL, '2020-08-08 20:51:50', NULL, NULL);
INSERT INTO `tbl_tenaker_utusan_detail` VALUES (7, 7, 10, NULL, NULL, NULL, '2020-08-08 20:51:50', NULL, NULL);
INSERT INTO `tbl_tenaker_utusan_detail` VALUES (8, 8, 11, NULL, NULL, NULL, '2020-08-08 20:51:50', NULL, NULL);
INSERT INTO `tbl_tenaker_utusan_detail` VALUES (9, 9, 12, NULL, NULL, NULL, '2020-08-08 20:51:50', NULL, NULL);
INSERT INTO `tbl_tenaker_utusan_detail` VALUES (10, 10, 13, NULL, NULL, NULL, '2020-08-08 20:51:50', NULL, NULL);
INSERT INTO `tbl_tenaker_utusan_detail` VALUES (11, 8, 14, NULL, NULL, NULL, '2020-08-08 20:51:50', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
