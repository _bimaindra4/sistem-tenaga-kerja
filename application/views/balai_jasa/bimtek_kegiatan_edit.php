<?php echo $header ?>

<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Edit Kegiatan Bimtek</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item">
							<a href="<?php echo site_url('bimtek/' . segment(2)) ?>">Bimbingan Teknis</a>
						</li>
						<li class="breadcrumb-item"><a href="#">Kegiatan Bimtek</a></li>
						<li class="breadcrumb-item active">Edit</li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<form id="kegiatan" method="post" class="form-horizontal" action="<?php echo site_url('Bimtek/EditKegiatanBimtek/'.segment(2)) ?>">
				<div class="row">
					<div class="col-md-6">
						<div class="card card-warning card-outline">
							<div class="card-header p-2">
								<h3 class="card-title">Deskripsi Kegiatan</h3>
							</div>
							<div class="card-body">
								<div class="form-body">
									<input type="hidden" name="id_bimtek" value="<?php echo segment(2) ?>">
									<div class="form-group row">
										<label class="control-label col-md-3">Tanggal</label>
										<div class="col-md-9">
											<input type="text" name="tgl_bimtek" id="tgl_bimtek" class="form-control"
												   value="<?= date("m/d/Y", strtotime($detail['kegiatan']->tgl_kehadiran)) ?>"
												   data-inputmask-alias="datetime"
												   data-inputmask-inputformat="mm/dd/yyyy" data-mask required>
										</div>
									</div>
									<div class="form-group row">
										<label class="control-label col-md-3">Kegiatan</label>
										<div class="col-md-9">
											<textarea name="kegiatan" class="form-control"><?= $detail['kegiatan']->kegiatan ?></textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="card-footer">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="card card-warning card-outline">
							<div class="card-header p-2">
								<h3 class="card-title">Daftar Peserta</h3>
							</div>
							<div class="card-body">
								<table class="table table-bordered table-striped">
									<thead>
									<tr>
										<th>No</th>
										<th>Nama Peserta</th>
										<th>Keterangan</th>
									</tr>
									</thead>
									<tbody>
									<?php $no = 1; foreach ($detail['kehadiran'] as $row) { ?>
										<tr>
											<td><?php echo $no ?></td>
											<td><?php echo $row->nama ?></td>
											<td>
												<input type="hidden" name="id[]" value="<?php echo $row->id ?>">
												<select name="keterangan[]" class="form-control-sm">
													<option value="hadir" <?= ($row->status == "hadir" ? 'selected' : '') ?>>Hadir</option>
													<option value="sakit" <?= ($row->status == "sakit" ? 'selected' : '') ?>>Sakit</option>
													<option value="Izin" <?= ($row->status == "izin" ? 'selected' : '') ?>>Izin</option>
													<option value="tidak_hadir" <?= ($row->status == "tidak_hadir" ? 'selected' : '') ?>>Tidak Hadir</option>
												</select>
											</td>
										</tr>
										<?php $no++; } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</section>
</div>

<?php echo $footer ?>

<script>
    $(document).ready(function () {
        $('#tgl_bimtek').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'});
    });
</script>
