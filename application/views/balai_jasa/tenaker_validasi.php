<?php echo $header ?>

<div class="content-wrapper">
  	<section class="content-header">
    	<div class="container-fluid">
      		<div class="row mb-2">
        		<div class="col-sm-6">
          			<h1>Tenaga Kerja</h1>
        		</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item active"><a href="#">Tenaga Kerja</a></li>
					</ol>
				</div>
      		</div>
    	</div>
  	</section>
	<section class="content">
		<div class="card card-outline card-warning mx-2">
			<div class="card-header">
				<h3 class="card-title">Data Kelulusan Tenaga Kerja</h3>
				<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
						<i class="fas fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIK</th>
                            <th>Nama Lengkap</th>
                            <th>Keahlian</th>
                            <th>JK</th>
                            <th>SKA / SKT</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; foreach($tenaker as $row) { ?> 
                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $row->tenaker_nik ?></td>
                                <td><?php echo $row->tenaker_nama ?></td>
                                <td><?php echo $row->jabatan_nama ?></td>
                                <td><?php echo ($row->tenaker_jkel == "L" ? "Laki-Laki" : "Perempuan") ?></td>
                                <td><?php echo $row->skat ?></td>
                                <td>
                                    <a class="btn btn-warning btn-xs validasi" href="<?php echo site_url('tenaker/detail/'.$row->id_tenaga_kerja_url) ?>">Detail</a>
                                </td>
                            </tr>
                        <?php $no++; } ?>
                    </tbody>
                </table>
			</div>
		</div>
	</section>
</div>

<?php echo $footer ?>

<script>
    $(document).ready(function() {
        $("#example1").DataTable();
    });
</script>
