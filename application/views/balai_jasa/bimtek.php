<?php echo $header ?>

<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Bimbingan Teknis</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item active"><a href="#">Bimbingan Teknis</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<button type="button" class="btn btn-sm btn-warning mx-2 mb-3" data-toggle="modal" data-target="#tambahBimtek">
			<i class="fas fa-plus mr-1"></i> Tambah Bimtek</button>
		<div class="card card-outline card-warning mx-2">
			<div class="card-header">
				<h3 class="card-title">Data Bimbingan Teknis</h3>
				<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
						<i class="fas fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="card-body">
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Bimtek</th>
							<th>Tanggal</th>
							<th>Kompetensi</th>
							<th>Status</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php $no=1; foreach($bimtek as $row) { ?>
							<tr>
								<td><?php echo $no ?></td>
								<td><?php echo $row->bimtek_nama ?></td>
								<td><?php echo $this->AppModel->DateIndo($row->tgl_mulai_bimtek)." s/d ".$this->AppModel->DateIndo($row->tgl_selesai_bimtek) ?></td>
								<td><?php echo $row->kompetensi ?></td>
								<td>
									<?php
										if($row->status == 0) {
											echo "<span class='badge badge-primary'>Pendaftaran</span>";
										} else if($row->status == 1) {
											echo "<span class='badge badge-success'>On Going</span>";
										} else if($row->status == 2) {
											echo "<span class='badge badge-danger'>Selesai</span>";
										}
									?>
								</td>
								<td>
									<a class="btn btn-warning btn-xs" href="<?php echo site_url('bimtek/'.$row->id_bimtek_url) ?>">Detail</a>
									<button class="btn btn-info btn-xs btnEditBimtek" data-id="<?php echo $row->id_bimtek_url ?>">Edit</button>
									<button class="btn btn-danger btn-xs btnHapusBimtek" data-id="<?php echo $row->id_bimtek_url ?>">Hapus</button>
								</td>
							</tr>
						<?php $no++; } ?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</div>

<div class="modal fade" id="tambahBimtek">
	<div class="modal-dialog">
		<div class="modal-content">
			<form class="form-horizontal" action="<?php echo site_url('bimtek/tambah') ?>" method="post" style="font-size: 15px">
				<div class="modal-header bg-yellow">
					<h4 class="modal-title hapus-title">Tambah Bimbingan Teknis</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group row">
						<label for="nama" class="col-sm-3 col-form-label">Nama Bimtek</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="nama" placeholder="Nama Bimtek" autocomplete="off" required>
						</div>
					</div>
					<div class="form-group row">
						<label for="nama" class="col-sm-3 col-form-label">Kompetensi</label>
						<div class="col-sm-9">
							<select class="form-control select2 kompetensi" name="kompetensi" required></select>
						</div>
					</div>
					<div class="form-group row">
						<label for="nama" class="col-sm-3 col-form-label">Tanggal Mulai Bimtek</label>
						<div class="col-sm-9">
							<input type="date" name="tgl_bimtek" id="tgl_mulai" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="mm/dd/yyyy" data-mask required>
						</div>
					</div>
					<div class="form-group row">
						<label for="nama" class="col-sm-3 col-form-label">Tanggal Selesai Bimtek</label>
						<div class="col-sm-9">
							<input type="date" name="tgl_bimtek" id="tgl_selesai" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="mm/dd/yyyy" data-mask required>
						</div>
					</div>
					<div class="form-group row">
						<label for="nama" class="col-sm-3 col-form-label">Deskripsi</label>
						<div class="col-sm-9">
							<textarea name="deskripsi" class="form-control" rows="4"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-success">Tambah</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="editBimtek">
	<div class="modal-dialog">
		<div class="modal-content">
			<form class="form-horizontal" id="formEditBimtek" method="post" style="font-size: 15px">
				<div class="modal-header bg-yellow">
					<h4 class="modal-title hapus-title">Edit Bimbingan Teknis</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group row">
						<label for="nama" class="col-sm-3 col-form-label">Nama Bimtek</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="nama" id="editNama" placeholder="Nama Bimtek" autocomplete="off" required>
						</div>
					</div>
					<div class="form-group row">
						<label for="nama" class="col-sm-3 col-form-label">Kompetensi</label>
						<div class="col-sm-9">
							<select class="form-control select2 kompetensi" name="kompetensi" id="editKompetensi" required></select>
						</div>
					</div>
					<div class="form-group row">
						<label for="nama" class="col-sm-3 col-form-label">Tanggal Mulai Bimtek</label>
						<div class="col-sm-9">
							<input type="date" name="tgl_mulai_bimtek" id="editTanggalMulai" class="form-control" required>
						</div>
					</div>
					<div class="form-group row">
						<label for="nama" class="col-sm-3 col-form-label">Tanggal Selesai Bimtek</label>
						<div class="col-sm-9">
							<input type="date" name="tgl_selesai_bimtek" id="editTanggalSelesai" class="form-control" required>
						</div>
					</div>
					<div class="form-group row">
						<label for="nama" class="col-sm-3 col-form-label">Deskripsi</label>
						<div class="col-sm-9">
							<textarea name="deskripsi" id="editDeskripsi" class="form-control" rows="4"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-success">Edit</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="hapusBimtek">
	<div class="modal-dialog">
		<div class="modal-content">
			<form class="form-horizontal" id="formHapusBimtek" method="post" style="font-size: 15px">
				<div class="modal-header bg-red">
					<h4 class="modal-title hapus-title">Hapus Bimbingan Teknis</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Apakah anda yakin akan menghapus data ini?</p>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="submit" class="btn btn-danger">Hapus</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php echo $footer ?>

<script>
    $(document).ready(function () {
		<?php
			$statusAct = $this->session->flashdata("status");
			$act = $this->session->flashdata("act");

			if($statusAct == 1) { ?>
				toastr.success('<?php echo $act ?> berhasil dilakukan')
			<?php } else if($statusAct == 2) { ?>
				toastr.error('Terjadi kesalahan pada saat <?php echo $act ?>')
			<?php }
		?>

        $("#example1").DataTable();
        $('.select2').select2();
        $(".kompetensi").load("<?php echo site_url('User/show_data_jabatan_tenaker') ?>");

        $(".btnEditBimtek").on("click", function() {
			let id = $(this).data("id");
			$.ajax({
				url: "<?php echo site_url('Bimtek/get_data_bimtek/') ?>"+id,
				dataType: "JSON",
				success: function(data) {
				    let href = "<?php echo site_url('bimtek/') ?>"+id+"/edit";
				    $("#editBimtek").modal("show");

				    $("#formEditBimtek").attr("action", href);
					$("#editNama").val(data.bimtek_nama);
					$("#editKompetensi").val(data.kompetensi);
					$("#editKompetensi").select2().trigger("change");
					$("#editTanggalMulai").val(data.tgl_mulai_bimtek);
					$("#editTanggalSelesai").val(data.tgl_selesai_bimtek);
					$("#editDeskripsi").text(data.deskripsi);
					console.log(data.tgl_mulai_bimtek);
				}
			});
		});

		$(".btnHapusBimtek").on("click", function() {
			let id = $(this).data("id");
			let href = "<?php echo site_url('bimtek/') ?>"+id+"/edit";
			
			$("#hapusBimtek").modal("show");
			$("#formHapusBimtek").attr("action", href);
		});
    });
</script>
