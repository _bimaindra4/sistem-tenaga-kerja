<?php echo $header ?>
<?php
    if($mode == "insert") {
        $url_form = 'bimtek/'.$id_bimtek.'/nilai/atur';
    } else if($mode == "edit") {
        $url_form = 'bimtek/'.$id_bimtek.'/nilai/edit';
    }
?>

<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Atur Nilai Peserta</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item">
							<a href="<?php echo site_url('bimtek') ?>">Bimbingan Teknis</a>
						</li>
						<li class="breadcrumb-item">
							<a href="<?php echo site_url('bimtek/'.$id_bimtek) ?>">Detail Bimtek</a>
						</li>
						<li class="breadcrumb-item active">
							<a href="#">Atur Nilai Peserta</a>
						</li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-warning mx-2">
                        <div class="card-header">
                            <h3 class="card-title">Nilai Peserta <?= $url_form ?></h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="<?php echo site_url($url_form) ?>" method="post">
                                <table id="nilai-bimtek" class="table table-bordered table-striped">
                                    <input type="hidden" name="id_bimtek" value="<?php echo $id_bimtek ?>">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th width="250">Nama Peserta</th>
                                            <th>Nilai Pre-Test</th>
                                            <th>Nilai Post-Test</th>
                                            <th>Nilai Mandiri</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no=1; foreach($peserta as $row) { ?>
                                            <?php if($mode == 'insert') { ?>
                                                <tr>
                                                    <td><?php echo $no ?></td>
                                                    <td>
                                                        <?php echo $row->tenaker_nama ?>
                                                        <input type="hidden" name="id_peserta[]" value="<?php echo $row->id_bimtek_peserta_url ?>">
                                                    </td>
                                                    <td><input type="number" class="form-control" name="pretest[]"></td>
                                                    <td><input type="number" class="form-control" name="posttest[]"></td>
                                                    <td><input type="number" class="form-control" name="mandiri[]"></td>
                                                </tr>
                                            <?php } else if($mode == 'edit') { ?>
                                                <tr>
                                                    <td><?php echo $no ?></td>
                                                    <td>
                                                        <?php echo $row->peserta ?>
                                                        <input type="hidden" name="id_nilai[]" value="<?php echo $row->id_nilai ?>">
                                                    </td>
                                                    <td><input type="number" class="form-control" name="pretest[]" value=<?php echo $row->pretest ?>></td>
                                                    <td><input type="number" class="form-control" name="posttest[]" value=<?php echo $row->posttest ?>></td>
                                                    <td><input type="number" class="form-control" name="mandiri[]" value=<?php echo $row->mandiri ?>></td>
                                                </tr>
                                            <?php } ?>
                                        <?php $no++; } ?>
                                    </tbody>
                                </table>
                                <button class="btn btn-primary btn-md mt-3">Submit Nilai</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</section>
</div>

<?php echo $footer ?>

<script>
    $('.form-control').change(function () {
        let pretest = $(this).val();
        if(pretest < 0) {
            $(this).val(0);
        } else if(pretest > 100) {
            $(this).val(100);
        }
    });
</script>