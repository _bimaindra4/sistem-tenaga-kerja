<?php echo $header ?>

<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Dashboard</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item active"><a href="#">Dashboard</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<?php
			$status_log = $this->session->flashdata("status_log");
			if ($status_log != NULL) {
				if ($status_log == "1") {
					?>
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fas fa-check"></i> Login Sukses!</h5>
						Selamat Datang <?php echo $usernama ?>
					</div>
				<?php }
			} ?>
			<div class="row">
				<div class="col-lg-4 col-6">
					<div class="small-box bg-success">
						<div class="inner ml-2">
							<h3><?= $skat_aktif->num_rows() ?></h3>
							<p>SKA/T yang Aktif</p>
						</div>
						<div class="icon">
							<i class="fa fa-certificate"></i>
						</div>
						<a href="#" id="skat-aktif" class="small-box-footer">Info Selanjutnya <i class="fas fa-arrow-circle-right ml-2"></i></a>
					</div>
				</div>
				<div class="col-lg-4 col-6">
					<div class="small-box bg-warning">
						<div class="inner ml-2">
							<h3><?= $skat_hampir->num_rows() ?></h3>
							<p>SKA/T Hampir Kadaluarsa</p>
						</div>
						<div class="icon">
							<i class="fa fa-certificate"></i>
						</div>
						<a href="#" id="skat-hampir" class="small-box-footer">Info Selanjutnya <i class="fas fa-arrow-circle-right ml-2"></i></a>
					</div>
				</div>
				<div class="col-lg-4 col-6">
					<div class="small-box bg-danger">
						<div class="inner ml-2">
							<h3><?= $skat_kadaluarsa->num_rows() ?></h3>
							<p>SKA/T yang Kadaluarsa</p>
						</div>
						<div class="icon">
							<i class="fa fa-certificate"></i>
						</div>
						<a href="#" id="skat-kadaluarsa" class="small-box-footer">Info Selanjutnya <i class="fas fa-arrow-circle-right ml-2"></i></a>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Peta Sebaran Tenaga Kerja</h3>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
								title="Collapse">
							<i class="fas fa-minus"></i>
						</button>
						<button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip"
								title="Remove">
							<i class="fas fa-times"></i>
						</button>
					</div>
				</div>
				<div class="card-body">
					<div id='map' style="width: 100%; height: 450px"></div>
				</div>
			</div>
		</div>
	</section>
</div>

<div class="modal fade" id="daftarSKATAktif">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
			<div class="modal-header bg-success">
				<h4 class="modal-title">Daftar SKAT Aktif</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="tbl_skat_aktif"></div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
        </div>
    </div>
</div>

<div class="modal fade" id="daftarSKATHampir">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
			<div class="modal-header bg-warning">
				<h4 class="modal-title">Daftar SKAT Hampir Kadaluarsa</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="tbl_skat_hampir"></div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
        </div>
    </div>
</div>

<div class="modal fade" id="daftarSKATKadaluarsa">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
			<div class="modal-header bg-danger">
				<h4 class="modal-title">Daftar SKAT Kadaluarsa</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" id="tbl_skat_kadaluarsa"></div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
        </div>
    </div>
</div>

<?php echo $footer ?>

<script>
    var googleRoadmap = new L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });
    var googleSatellite = new L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });
    var googleHybrid = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });
    var mpn = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

    var baseMaps = {
        'Google Roadmap': googleRoadmap, 'Google Satellite': googleSatellite,
        'Google Hybrid': googleHybrid, 'Mapnik': mpn
    };

    // Marker
    var poi = L.icon({
        iconUrl: '<?php echo base_url() ?>assets/marker.png',
        iconSize: [30, 40],
        iconAnchor: [15, 35]
    });

    var map = new L.Map('map', {
        center: new L.LatLng(-2.8020048, 118.3481111),
        zoom: 5,
        layers: [googleRoadmap]
    });

    // Init Marker Cluster
    var markers = new L.MarkerClusterGroup();
    var markersList = [];

    function tenaker() {
		<?php foreach($sebaran_tenaker as $row) { ?>
			<?php 
				$lat = (float) $row->lat;
				$lng = (float) $row->lng;
				$nama = $row->utusan_tenaker;
				$alamat = "";

				if($row->utusan_alamat == NULL) {
					$alamat = "Tidak ada alamat";
				} else {
					$alamat = $row->utusan_alamat;
				}
			?>

			var content = '<p><?= $nama ?></p>'+
			'<p><?= $alamat ?></p>'+
			'<a href="<?= site_url('utusan/detail/').$row->id ?>" class="btn btn-xs btn-primary detail-map text-white">Detail</a>';

            var m = new L.Marker(new L.LatLng(<?= $lat ?>, <?= $lng ?>), {icon: poi}).bindPopup(content);
            markersList.push(m);
            markers.addLayer(m);
		<?php } ?>

        return false;
    }

    tenaker();
    map.addLayer(markers);

    map.addControl(new L.Control.Scale());
	map.addControl(new L.Control.Layers(baseMaps));

	$(document).ready(function () {
		$("#skat-aktif").on("click", function() {
			$.ajax({
				url: "<?php echo site_url('dashboard/get_skat_aktif') ?>",
				dataType: "JSON",
				success: function(data) {
					$("#daftarSKATAktif").modal("show");
					$("#tbl_skat_aktif").html(data.output);
				}
			});
		});

		$("#skat-hampir").on("click", function() {
			$.ajax({
				url: "<?php echo site_url('dashboard/get_skat_hampir_kadaluarsa') ?>",
				dataType: "JSON",
				success: function(data) {
					$("#daftarSKATHampir").modal("show");
					$("#tbl_skat_hampir").html(data.output);
				}
			});
		});

		$("#skat-kadaluarsa").on("click", function() {
			$.ajax({
				url: "<?php echo site_url('dashboard/get_skat_kadaluarsa') ?>",
				dataType: "JSON",
				success: function(data) {
					$("#daftarSKATKadaluarsa").modal("show");
					$("#tbl_skat_kadaluarsa").html(data.output);
				}
			});
		});
	});
</script>