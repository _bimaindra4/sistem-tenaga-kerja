<?php echo $header ?>

<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1><?php echo $bimtek->bimtek_nama ?></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item">
							<a href="<?php echo site_url('bimtek') ?>">Bimbingan Teknis</a>
						</li>
						<li class="breadcrumb-item active">
							<a href="#"><?php echo $bimtek->bimtek_nama ?></a>
						</li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="deskripsi-tab" data-toggle="pill" href="#deskripsi" role="tab" aria-controls="deskripsi" aria-selected="true">Deskripsi</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="peserta-tab" data-toggle="pill" href="#peserta" role="tab" aria-controls="peserta" aria-selected="false">Peserta</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="kegiatan-tab" data-toggle="pill" href="#kegiatan" role="tab" aria-controls="kegiatan" aria-selected="false">Kegiatan</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="nilai-tab" data-toggle="pill" href="#nilai" role="tab" aria-controls="nilai" aria-selected="false">Nilai</a>
				</li>
			</ul>
			<div class="tab-content" id="custom-content-below-tabContent">
				<div class="tab-pane fade show active" id="deskripsi" role="tabpanel" aria-labelledby="deskripsi-tab">
					<div class="row my-3">
						<div class="col-md-3">
							<?php
								$status = $bimtek->status_bimtek;
								if($status == 0) {
									$color = "primary";
									$image = "registration.png";
									$text = "Pendaftaran";
								} else if($status == 1) {
									$color = "success";
									$image = "ongoing.png";
									$text = "Sedang Berlangsung";
								} else if($status == 2) {
									$color = "danger";
									$image = "closed.png";
									$text = "Selesai";
								} else {
									$color = "default";
									$image = "question.png";
									$text = "??????";
								}
							?>
							<div class="card card-<?php echo $color ?> card-outline">
								<div class="card-body box-profile">
									<div class="text-center my-2">
										<img class="img-fluid img-circle"
											 src="<?php echo base_url() ?>assets/images/<?php echo $image ?>"
											 alt="Status">
									</div>
									<h3 class="profile-username text-center mt-3">Status</h3>
									<p class="text-muted text-center"><?php echo $text ?></p>
									<button class="btn btn-<?php echo $color ?> btn-block mt-3" id="ubahStatus"
											data-status="<?php echo $status ?>" data-id="<?php echo $idbimtek ?>">Ubah Status</button>
								</div>
							</div>
							<div class="small-box bg-<?php echo $color ?>">
								<div class="inner ml-2">
									<h3>0</h3>
									<p>Jumlah Peserta</p>
								</div>
								<div class="icon">
									<i class="fa fa-users"></i>
								</div>
							</div>
						</div>
						<div class="col-md-9">
							<div class="card card-warning card-outline">
								<div class="card-header p-2">
									<h3 class="card-title">Deskripsi Bimtek</h3>
								</div>
								<div class="card-body">
									<table class="table table-bordered">
										<tr>
											<th width="25%">Deskripsi</th>
											<td><?php echo $bimtek->deskripsi ?></td>
										</tr>
										<tr>
											<th>Kompetensi</th>
											<td><?php echo $bimtek->kompetensi ?></td>
										</tr>
										<tr>
											<th>Tanggal Pelaksanaan</th>
											<td><?php echo $this->AppModel->DateIndo($bimtek->tgl_mulai_bimtek)." s/d ".$this->AppModel->DateIndo($bimtek->tgl_selesai_bimtek) ?></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="peserta" role="tabpanel" aria-labelledby="peserta-tab">
					<div class="row mt-3">
						<div class="col-md-3">
							<div class="small-box bg-success">
								<div class="inner ml-2">
									<h3><?php echo $lulus ?></h3>
									<p>Peserta Lulus</p>
								</div>
								<div class="icon">
									<i class="fa fa-users"></i>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="small-box bg-primary">
								<div class="inner ml-2">
									<h3><?php echo $kompeten ?></h3>
									<p>Peserta Kompeten</p>
								</div>
								<div class="icon">
									<i class="fa fa-users"></i>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<a href="<?php echo site_url('bimtek/'.$this->uri->segment(2).'/ijazah') ?>" class="btn btn-sm btn-primary mx-2 mb-3">Kirim Ijazah</a>
							<div class="card card-outline card-warning mx-2">
								<div class="card-header">
									<h3 class="card-title">Data Peserta</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
											<i class="fas fa-minus"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									<table id="peserta-bimtek" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>No</th>
												<th>NIK</th>
												<th>Nama Peserta</th>
												<th>JKel</th>
												<th>Status</th>
												<th>Hasil Akhir</th>
												<?php if($status == 0 || $status == 2) { ?>
													<th></th>
												<?php } ?>
											</tr>
										</thead>
										<tbody>
											<?php $no=1; foreach($peserta as $row) { ?>
												<tr>
													<td><?php echo $no ?></td>
													<td><?php echo $row->tenaker_nik ?></td>
													<td><?php echo $row->tenaker_nama ?></td>
													<td><?php echo $row->tenaker_jkel ?></td>
													<td>
														<?php
															$sttp = $row->status_peserta;
															if($sttp == 0) {
																echo "<span class='badge badge-danger'>Belum Diverifikasi</span>";
															} else if($sttp == 1) {
																echo "<span class='badge badge-success'>Telah Diverifikasi</span>";
															} else {
																echo "<span class='badge badge-default'>Tidak Diketahui</span>";
															}
														?>
													</td>
													<td>
														<?php
															$stll = $row->status_lulus;
															$stkp = $row->status_kompeten;
															if($stll == 0 && $stll != NULL) {
																echo "<span class='badge badge-danger'>Tidak Lulus</span>";
															} else if($stll == 1) {
																if($stkp == NULL) {
																	echo "<span class='badge badge-success'>Lulus</span>";
																} else if($stkp == 0 && $stll != NULL) {
																	echo "<span class='badge badge-danger'>Tidak Kompeten</span>";
																} else if($stkp == 1) {
																	echo "<span class='badge badge-success'>Kompeten</span>";
																} else {
																	echo "<span class='badge badge-default'>Tidak Diketahui</span>";
																}
															} else {
																echo "<span class='badge badge-default'>Tidak Diketahui</span>";
															}
														?>
													</td>
													<?php if($status == 0 || $status == 2) { ?>
														<td>
															<button class="btn btn-xs btn-warning edit-peserta" data-uid="<?php echo $row->id_bimtek_peserta_url ?>">Edit</button>
														</td>
													<?php } ?>
												</tr>
											<?php $no++; } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="kegiatan" role="tabpanel" aria-labelledby="kegiatan-tab">
					<?php if($status == 1 || $status == 2) { ?>
						<?php if($status == 1) { ?>
							<a href="<?php echo site_url('bimtek/'.$idbimtek.'/kegiatan/tambah/form') ?>" class="btn btn-sm btn-warning mx-2 mt-3">
								<i class="fas fa-plus mr-1"></i> Tambah Kegiatan
							</a>
						<?php } ?>
						<div class="row mt-3">
							<div class="col-md-12">
								<div class="card card-outline card-warning mx-2">
									<div class="card-header">
										<h3 class="card-title">Data Kegiatan</h3>
										<div class="card-tools">
											<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
												<i class="fas fa-minus"></i>
											</button>
										</div>
									</div>
									<div class="card-body">
										<table id="kegiatan-bimtek" class="table table-bordered table-striped">
											<thead>
											<tr>
												<th>No</th>
												<th>Kegiatan</th>
												<th>Tanggal Kegiatan</th>
												<th>Peserta Hadir</th>
												<?php if($status == 1) { ?>
													<th></th>
												<?php } ?>
											</tr>
											</thead>
											<tbody>
												<?php $no=1; foreach($kegiatan as $row) { ?>
													<tr>
														<td><?php echo $no ?></td>
														<td><?php echo $row->kegiatan ?></td>
														<td><?php echo $this->AppModel->DateIndo($row->tgl_kehadiran) ?></td>
														<td><?php echo $row->hadir." Orang" ?></td>
														<?php if($status == 1) { ?>
																<td>
																	<a href="<?php echo site_url('bimtek/'.$row->uid.'/kegiatan/edit/form') ?>" class="btn btn-xs btn-warning">Edit</a>
																	<button class="btn btn-xs btn-danger hapus-kegiatan" data-uid="<?php echo $row->uid ?>">Hapus</button>
																</td>
														<?php } ?>
													</tr>
												<?php $no++; } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					<?php } else { ?>
						<div class="row mt-3">
							<div class="col-md-12">
								<div class="alert alert-warning alert-dismissible">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<h5><i class="icon fas fa-exclamation-triangle"></i> Alert!</h5>
									Menu kegiatan belum dibuka, silahkan ganti status
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
				<div class="tab-pane fade" id="nilai" role="tabpanel" aria-labelledby="nilai-tab">
					<?php if($status == 2) { ?>
						<a href="<?php echo site_url('bimtek/'.$idbimtek.'/nilai/atur/form') ?>" class="btn btn-sm btn-warning mx-2 my-3">
							<i class="fas fa-plus mr-1"></i> Atur Nilai Peserta
						</a>
						<div class="row">
						<div class="col-md-12">
							<div class="card card-outline card-warning mx-2">
								<div class="card-header">
									<h3 class="card-title">Data Nilai Peserta</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
											<i class="fas fa-minus"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									<table id="nilai-bimtek" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>No</th>
												<th>Nama Peserta</th>
												<th>Kehadiran</th>
												<th>Pre-Test</th>
												<th>Post-Test</th>
												<th>Mandiri</th>
												<th>NA</th>
											</tr>
										</thead>
										<tbody>
											<?php $no=1; foreach($nilai->result() as $row) { ?>
												<tr>
													<td><?php echo $no ?></td>
													<td><?php echo $row->peserta ?></td>
													<td><?php echo $row->kehadiran ?></td>
													<td><?php echo $row->pretest ?></td>
													<td><?php echo $row->posttest ?></td>
													<td><?php echo $row->mandiri ?></td>
													<td></td>
												</tr>
											<?php $no++; } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php } else { ?>
						<div class="row mt-3">
							<div class="col-md-12">
								<div class="alert alert-warning alert-dismissible">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									<h5><i class="icon fas fa-exclamation-triangle"></i> Alert!</h5>
									Menu penilaian belum dibuka, silahkan ganti status
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>
</div>

<div class="modal fade" id="ubahStatusModal">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<form class="form-horizontal" id="formUbahStatus" action="#" method="post" style="font-size: 15px">
				<div class="modal-header bg-<?php echo $color ?>">
					<h5 class="modal-title">Ubah Status</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group row">
						<label for="nama" class="col-sm-3 col-form-label">Status</label>
						<div class="col-sm-9">
							<select name="status" id="status" class="form-control">
								<option value="0">Pendaftaran</option>
								<option value="1">Sedang Berlangsung</option>
								<option value="2">Selesai</option>
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-<?php echo $color ?>">Simpan Data</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="hapusKegiatanModal">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<form class="form-horizontal" id="hapusKegiatanForm" action="#" method="post" style="font-size: 15px">
				<div class="modal-header bg-danger">
					<h5 class="modal-title">Ubah Status</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<input type="hidden" name="id_kegiatan">
					<input type="text" name="id_bimtek" value="<?php echo $this->uri->segment(2) ?>">
					<b>Apakah Anda yakin akan menghapus data kegiatan ini?</b>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="submit" class="btn btn-danger">Hapus Data</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="editPeserta">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<form class="form-horizontal" id="editPesertaForm" method="post" style="font-size: 15px">
				<div class="modal-header bg-warning">
					<h5 class="modal-title">Edit Peserta</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<input type="hidden" name="url" value="<?php echo $this->uri->segment(2) ?>">
					<?php if($status == 0) { ?>
						<div class="form-group row">
							<label for="nama" class="col-sm-3 col-form-label">Status</label>
							<div class="col-sm-9">
								<select name="status" class="form-control">
									<option value="0">Belum Diverifikasi</option>
									<option value="1">Verifikasi</option>
								</select>
							</div>
						</div>
					<?php } ?>
					<?php if($status == 2) { ?>
						<div class="form-group row">
							<label for="nama" class="col-sm-3 col-form-label">Status Lulus</label>
							<div class="col-sm-9">
								<select name="lulus" class="form-control">
									<option value="0">Tidak Lulus</option>
									<option value="1">Lulus</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label for="nama" class="col-sm-3 col-form-label">Status Kompeten</label>
							<div class="col-sm-9">
								<select name="kompeten" class="form-control">
									<option value="0">Tidak Kompeten</option>
									<option value="1">Kompeten</option>
								</select>
							</div>
						</div>
					<?php } ?>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php echo $footer ?>

<script>
    $(document).ready(function() {
		<?php
			$statusAct = $this->session->flashdata("status");
			$act = $this->session->flashdata("act");

			if($statusAct == 1) { ?>
				toastr.success('<?php echo $act ?> berhasil dilakukan')
			<?php } else if($statusAct == 2) { ?>
				toastr.error('Terjadi kesalahan pada saat <?php echo $act ?>')
			<?php }
		?>
		
        $("#peserta-bimtek").DataTable();
        $("#kegiatan-bimtek").DataTable();
        $("#nilai-bimtek").DataTable();

        $("#ubahStatus").on("click", function(e) {
			let status = $(this).data("status");
			let id = $(this).data("id");

			$("#ubahStatusModal").modal("show");
			$("[name='status']").val(status);
			$("#formUbahStatus").attr("action", "<?php echo site_url('Bimtek/UbahStatus/') ?>"+id);
		});

        $(".hapus-kegiatan").on("click", function(e) {
            let uid = $(this).data("uid");

			$("#hapusKegiatanModal").modal("show");
			$("#hapusKegiatanForm").attr("action", "<?php echo site_url('Bimtek/HapusKegiatanBimtek/') ?>"+uid);
			$("[name='id_kegiatan']").val(uid);
		});

		$(".edit-peserta").on("click", function(e) {
			let uid = $(this).data("uid");
			
			$("#editPeserta").modal("show");
			$("#editPesertaForm").attr("action", "<?php echo site_url('Bimtek/EditStatusPeserta/') ?>"+uid);
		});
    });
</script>