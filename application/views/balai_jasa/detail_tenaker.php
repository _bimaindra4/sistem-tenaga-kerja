<?php echo $header ?>

<style>
	th {
		text-align: center;
	}
</style>

<div class="content-wrapper">
  	<section class="content-header">
    	<div class="container-fluid">
      		<div class="row mb-2">
        		<div class="col-sm-6">
          			<h1>Detail Tenaga Kerja</h1>
        		</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('tenaker') ?>">Tenaga Kerja</a></li>
						<li class="breadcrumb-item active"><a href="#">Detail Tenaga Kerja</a></li>
					</ol>
				</div>
      		</div>
    	</div>
  	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-3">
				<div class="card card-warning card-outline">
					<div class="card-body box-profile">
						<div class="text-center">
							<img class="profile-user-img img-fluid img-circle"
									src="<?php echo base_url() ?>assets/images/user.png"
									alt="User profile picture">
						</div>
						<h3 class="profile-username text-center mt-3"><?php echo $tenaker->tenaker_nama ?></h3>
						<p class="text-muted text-center"><?php echo $tenaker->tenaker_nik ?></p>
					</div>
				</div>
				<div class="card card-warning">
					<div class="card-header">
						<h3 class="card-title">Profil</h3>
					</div>
					<div class="card-body">
						<strong><i class="fas fa-map-marker-alt mr-1"></i> Alamat</strong>
						<p class="text-muted">
							<?php
								$alamat = $tenaker->tenaker_alamat;
								if($alamat == NULL || $alamat == "") {
									echo "<span class='badge badge-danger'>Belum ada alamat</span>";
								} else {
									echo $alamat;
								}
							?>
						</p>
						<hr>
						<strong><i class="fas fa-graduation-cap mr-1"></i> NPWP</strong>
						<p class="text-muted"><?php echo $tenaker->tenaker_no_npwp ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="card card-outline card-warning mx-2">
					<div class="card-header">
						<h3 class="card-title">Detail Tenaga Kerja</h3>
						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fas fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="card-body">
						<h5>Data Registrasi</h5>
						<table class="table table-bordered mb-3">
							<thead>
								<tr>
									<th>Sub Klasifikasi</th>
									<th>Kualifikasi</th>
									<th>Tanggal Registrasi</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($sertifikat as $row) { ?>
									<tr>
										<td><?php echo $row->jabatan_nama ?></td>
										<td><?php echo $row->sertifikat_jenis ?></td>
										<td><?php echo $row->sertifikat_tanggal ?></td>
										<td><?php echo $row->status ?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						<h5>Data Riwayat Pekerjaan</h5>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Nama Perusahaan</th>
									<th>Jabatan</th>
									<th>Tanggal Mulai s/d Tanggal Akhir</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($pekerjaan as $row) { ?>
									<tr>
										<td><?php echo $row->utusan_tenaker ?></td>
										<td><?php echo $row->jabatan ?></td>
										<td><?php echo $row->tgl_mulai." s/d ".$row->tgl_akhir ?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php echo $footer ?>