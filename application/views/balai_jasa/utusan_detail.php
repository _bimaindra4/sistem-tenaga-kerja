<?php echo $header ?>

<style>
	th {
		text-align: center;
	}
</style>

<div class="content-wrapper">
  	<section class="content-header">
    	<div class="container-fluid">
      		<div class="row mb-2">
        		<div class="col-sm-6">
          			<h1>Detail Utusan</h1>
        		</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('dashboard') ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="#">Detail Utusan</a></li>
					</ol>
				</div>
      		</div>
    	</div>
  	</section>
	<section class="content">
        <div class="row">
			<div class="col-md-3">
				<div class="card card-warning card-outline">
					<div class="card-body box-profile">
						<div class="text-center">
							<img class="profile-user-img img-fluid img-circle"
									src="<?php echo base_url() ?>assets/images/building.png"
									alt="User profile picture">
						</div>
						<h3 class="profile-username text-center mt-3"><?php echo $utusan->utusan_tenaker ?></h3>
						<p class="text-muted text-center"><?php echo $utusan->utusan_alamat ?></p>
					</div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card card-outline card-warning mx-2">
					<div class="card-header">
						<h3 class="card-title">Tenaga Kerja</h3>
						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fas fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tenaker</th>
                                    <th>Jabatan</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Akhir</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach($tenaker as $row) { ?>
                                    <tr>
                                        <td><?php echo $no ?></td>
                                        <td><?php echo $row->nama ?></td>
                                        <td><?php echo $row->jabatan ?></td>
                                        <td><?php echo $row->tgl_mulai ?></td>
                                        <td><?php echo $row->tgl_akhir ?></td>
                                    </tr>
                                <?php $no++; } ?>
                            </tbody>
                        </table>
					</div>
				</div>
            </div>
        </div>
	</section>
</div>

<?php echo $footer ?>

<script>
    $(document).ready(function () {
        $("#example1").DataTable();
    });
</script>