<?php echo $header ?>
<?php $this->load->model("AppModel") ?>

<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Profil Tenaga Kerja</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item active"><a href="#">Profil Tenaga Kerja</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-3">
					<div class="card card-warning card-outline">
						<div class="card-body box-profile">
							<div class="text-center">
								<img class="profile-user-img img-fluid img-circle"
									 src="<?php echo base_url() ?>assets/images/user.png"
									 alt="User profile picture">
							</div>
							<h3 class="profile-username text-center mt-3"><?php echo $tenaker->tenaker_nama ?></h3>
							<p class="text-muted text-center"><?php echo $tenaker->tenaker_nik ?></p>
							<a href="<?php echo site_url('profil/edit') ?>" class="btn btn-warning btn-block"><b>Edit Profil</b></a>
						</div>
					</div>
					<!-- About Me Box -->
					<div class="card card-warning">
						<div class="card-header">
							<h3 class="card-title">Profil</h3>
						</div>
						<div class="card-body">
							<strong><i class="fas fa-map-marker-alt mr-1"></i> Alamat</strong>
							<p class="text-muted">
								<?php
									$alamat = $tenaker->tenaker_alamat;
									if($alamat == NULL || $alamat == "") {
										echo "<span class='badge badge-danger'>Belum ada alamat</span>";
									} else {
										echo $alamat;
									}
								?>
							</p>
							<hr>
							<strong><i class="fas fa-graduation-cap mr-1"></i> Pendidikan Terakhir</strong>
							<p class="text-muted"><?php echo $tenaker->tenaker_pendidikan ?></p>
							<hr>
							<strong><i class="fas fa-flag mr-1"></i> Warga Negara</strong>
							<p class="text-muted"><?php echo $tenaker->tenaker_kewarganegaraan ?></p>
							<hr>
							<strong><i class="fas fa-certificate mr-1"></i> Jumlah Sertifikat</strong>
							<p class="text-muted"><?php echo $tenaker->skat ?></p>
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="card">
						<div class="card-header p-2">
							<ul class="nav nav-pills">
								<li class="nav-item"><a class="nav-link active" href="#datadiri" data-toggle="tab">Data Diri</a>
								</li>
								<li class="nav-item"><a class="nav-link" href="#bimtek" data-toggle="tab">Riwayat Bimtek</a>
								</li>
								<li class="nav-item"><a class="nav-link" href="#proyek" data-toggle="tab">Riwayat Proyek</a>
								</li>
							</ul>
						</div>
						<div class="card-body">
							<div class="tab-content">
								<div class="active tab-pane" id="datadiri">
									<table class="table table-bordered">
										<tr>
											<th width="30%">Tempat, Tanggal Lahir</th>
											<td><?php echo $tenaker->tenaker_tempat_lahir.", ".$this->AppModel->DateIndo($tenaker->tenaker_tanggal_lahir) ?></td>
										</tr>
										<tr>
											<th>Provinsi, Kabupaten / Kota</th>
											<td>
												<?php
													$prov = $tenaker->provinsi_nama;
													$kabkot = $tenaker->kabkot_nama;

													if($prov == NULL || $prov == "") {
														echo "<span class='badge badge-danger'>Belum ada provinsi</span>";
													} else {
														if($kabkot == NULL || $kabkot == "") {
															echo "<span class='badge badge-danger'>Belum ada kabupaten / kota</span>";
														} else {
															echo $prov.", ".$kabkot;
														}
													}
												?>
											</td>
										</tr>
										<tr>
											<th>Jenis Kelamin</th>
											<td><?php echo ($tenaker->tenaker_jkel == "L" ? "Laki-Laki" : "Perempuan") ?></td>
										</tr>
										<tr>
											<th>No HP</th>
											<td>
												<?php
													$nohp = $tenaker->tenaker_nohp;
													if($nohp == NULL || $nohp == "") {
														echo "<span class='badge badge-danger'>Belum ada nohp</span>";
													} else {
														echo $nohp;
													}
												?>
											</td>
										</tr>
										<tr>
											<th>Email</th>
											<td>
												<?php
													$email = $tenaker->tenaker_email;
													if($email == NULL || $email == "") {
														echo "<span class='badge badge-danger'>Belum ada email</span>";
													} else {
														echo $email;
													}
												?>
											</td>
										</tr>
										<tr>
											<th>Institusi Pendidikan</th>
											<td>
												<?php
													$inst = $tenaker->tenaker_institusi;
													if($inst == NULL || $inst == "") {
														echo "<span class='badge badge-danger'>Belum ada institusi</span>";
													} else {
														echo $inst;
													}
												?>
											</td>
										</tr>
										<tr>
											<th>Jurusan</th>
											<td>
												<?php
													$jur = $tenaker->tenaker_jurusan;
													if($jur == NULL || $jur == "") {
														echo "<span class='badge badge-danger'>Belum ada jurusan</span>";
													} else {
														echo $jur;
													}
												?>
											</td>
										</tr>
										<tr>
											<th>Tanggal Lulus</th>
											<td><?php echo $this->AppModel->DateIndo($tenaker->tenaker_tanggal_lulus) ?></td>
										</tr>
									</table>
								</div>
								<div class="tab-pane" id="bimtek">
									<div class="timeline timeline-inverse">
										<?php 
											foreach($bimtek as $row) { 
												$stts_bimtek = 0;
												if($row->status_bimtek == 0) {
													$stts_bimtek = "primary";
													$icon_bimtek = "pencil-alt";
												} else if($row->status_bimtek == 1) {
													$stts_bimtek = "success";
													$icon_bimtek = "spinner";
												} else if($row->status_bimtek == 2) {
													$stts_bimtek = "danger";
													$icon_bimtek = "check";
												}

												$color_peserta = "gray";
												$stts_peserta = "Tidak Diketahui";
												if($row->status_bimtek == 2) {
													if($row->lulus == 0) {
														$color_peserta = "danger";
														$stts_peserta = "Tidak Lulus";
													} else if($row->lulus == 1) {
														if($row->kompeten == 0) {
															$color_peserta = "danger";
															$stts_peserta = "Tidak Kompeten";
														} else if($row->kompeten == 1) {
															$color_peserta = "success";
															$stts_peserta = "Kompeten";
														}
													}
												}
											?>
											<div class="time-label">
												<span class="bg-primary"><?= date("j M Y", strtotime($row->mulai)) ?></span>
											</div>
											<div>
												<i class="fas fa-<?= $icon_bimtek ?> bg-<?= $stts_bimtek ?>"></i>
												<div class="timeline-item">
													<div class="timeline-body">
														<h5><?= $row->nama ?></h5>
														<p><?= $row->deskripsi ?></p>
														<span class="right badge badge-<?= $color_peserta ?>"><?= $stts_peserta ?></span>
													</div>
												</div>
											</div>
										<?php } ?>
										<div>
											<i class="far fa-clock bg-gray"></i>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="proyek">
									<table class="table table-bordered"><table class="table table-bordered">
										<thead>
											<tr>
												<th>Nama Perusahaan</th>
												<th>Jabatan</th>
												<th>Tanggal Mulai s/d Tanggal Akhir</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach($pekerjaan as $row) { ?>
												<tr>
													<td><?php echo $row->utusan_tenaker ?></td>
													<td><?php echo $row->jabatan ?></td>
													<td><?php echo $row->tgl_mulai." s/d ".$row->tgl_akhir ?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php echo $footer ?>