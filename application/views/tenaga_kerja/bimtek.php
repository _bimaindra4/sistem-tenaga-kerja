<?php echo $header ?>

<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Bimbingan Teknis</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo site_url('dashboard') ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="#">Bimbingan Teknis</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="custom-content-below-home-tab" data-toggle="pill" href="#custom-content-below-home" role="tab" aria-controls="custom-content-below-home" aria-selected="true">Daftar Kelas</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="custom-content-below-profile-tab" data-toggle="pill" href="#custom-content-below-profile" role="tab" aria-controls="custom-content-below-profile" aria-selected="false">Histori</a>
						</li>
					</ul>
					<div class="tab-content mt-2" id="custom-content-below-tabContent">
						<div class="tab-pane fade show active" id="custom-content-below-home" role="tabpanel" aria-labelledby="custom-content-below-home-tab">
							<div class="card card-outline card-warning">
								<div class="card-header">
									<h3 class="card-title">
										Kelas Bimtek
									</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									<table id="example1" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>No</th>
												<th>Nama Bimtek</th>
												<th>Tanggal</th>
												<th>Kompetensi</th>
												<th>Status</th>
												<th width="130"></th>
											</tr>
										</thead>
										<tbody>
											<?php $no=1; foreach($bimtek as $row) { ?>
												<tr>
													<td><?php echo $no ?></td>
													<td><?php echo $row->bimtek_nama ?></td>
													<td><?php echo $this->AppModel->DateIndo($row->tgl_mulai_bimtek)." s/d ".$this->AppModel->DateIndo($row->tgl_selesai_bimtek) ?></td>
													<td><?php echo $row->kompetensi ?></td>
													<td>
														<?php
															if($row->status == 0) {
																echo "<span class='badge badge-primary'>Pendaftaran</span>";
															} else if($row->status == 1) {
																echo "<span class='badge badge-success'>Sedang Berjalan</span>";
															} else if($row->status == 2) {
																echo "<span class='badge badge-danger'>Selesai</span>";
															}
														?>
													</td>
													<td class="aksi">
														<?php
															if($row->status_peserta == NULL) {
																if($row->status == 0) {
																	echo "<a class='btn btn-primary btn-xs btn-daftar' href='".site_url('bimtek/'.$row->id_bimtek_url.'/daftar')."'>Daftar</a>";
																} else if($row->status == 1 || $row->status == 2) {
																	echo "<button class='btn btn-primary disabled btn-xs'>Daftar</button>";
																}
															} else if($row->status_peserta == 0) {
																echo "<span class='badge badge-warning text-white'>Menunggu Verifikasi</span>";
															} else if($row->status_peserta == 1) {
																echo "<span class='badge badge-success'>Terverifikasi</span>";
															} else if($row->status_peserta == 2) {
																echo "<span class='badge badge-danger'>Ditolak</span>";
															}
														?>
													</td>
												</tr>
											<?php $no++; } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="custom-content-below-profile" role="tabpanel" aria-labelledby="custom-content-below-profile-tab">
							<div class="card card-outline card-warning">
								<div class="card-header">
									<h3 class="card-title">
										Histori Kelas
									</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									<table id="example2" class="table table-bordered table-striped">
										<thead>
											<tr>
												<th>No</th>
												<th>Nama Bimtek</th>
												<th>Tanggal</th>
												<th>Kompetensi</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
										<?php $no=1; foreach($bimtek_done as $row) { ?>
												<tr>
													<td><?php echo $no ?></td>
													<td><?php echo $row->bimtek_nama ?></td>
													<td><?php echo $this->AppModel->DateIndo($row->tgl_mulai_bimtek)." s/d ".$this->AppModel->DateIndo($row->tgl_selesai_bimtek) ?></td>
													<td><?php echo $row->kompetensi ?></td>
													<td class="aksi">
														<?php
															if($row->status == 2) {
																if($row->lulus == 0) {
																	echo "<span class='badge badge-danger'>Tidak Lulus</span>";
																} else if($row->lulus == 1) {
																	if($row->kompeten == 0) {
																		echo "<span class='badge badge-danger'>Tidak Kompeten</span>";
																	} else if($row->kompeten == 1) {
																		echo "<span class='badge badge-success'>Kompeten</span>";
																	}
																}
															}
														?>
													</td>
												</tr>
											<?php $no++; } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php echo $footer ?>

<script>
    $(document).ready(function () {
		<?php
			$statusAct = $this->session->flashdata("status");
			$act = $this->session->flashdata("act");

			if($statusAct == 1) { ?>
				toastr.success('<?php echo $act ?> berhasil dilakukan')
			<?php } else if($statusAct == 2) { ?>
				toastr.error('Terjadi kesalahan pada saat <?php echo $act ?>')
			<?php }
		?>

        $("#example1").DataTable();
        $("#example2").DataTable();
    });
</script>