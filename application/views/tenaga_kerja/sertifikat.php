<?php echo $header ?>
<?php $this->load->model("AppModel") ?>

<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Sertifikat Keahlian</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo site_url('dashboard') ?>">Dashboard</a></li>
						<li class="breadcrumb-item active"><a href="#">Sertifikat Keahlian</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card card-outline card-warning mx-2">
						<div class="card-header">
							<h3 class="card-title">Sertifikat Keahlian</h3>
							<div class="card-tools">
								<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
									<i class="fas fa-minus"></i>
								</button>
							</div>
						</div>
						<div class="card-body">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>Sertifikat</th>
										<th>Tanggal Berlaku</th>
										<th>Jenis</th>
										<th>No Sertifikat</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<?php $no=1; foreach($sertifikat->result() as $row) { ?>
										<tr>
											<td><?= $no ?></td>
											<td><?= $row->jabatan ?></td>
											<td><?= $this->AppModel->DateIndo($row->sertifikat_tanggal) ?></td>
											<td><?= ucfirst($row->sertifikat_jenis) ?></td>
											<td><?= $row->sertifikat_nomor ?></td>
											<td>
												<?php 
													$masa = $row->masa_berlaku;
													if($masa > 30) {
														echo "<span class='badge badge-success'>Aktif</span>";
													} else if($masa < 30 && $masa > 0) {
														echo "<span class='badge badge-warning'>Hampir Kadaluarsa</span>";
													} else if($masa < 0) {
														echo "<span class='badge badge-danger'>Kadaluarsa</span>";
													}
												?>
											</td>
										</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php echo $footer ?>

<script>
	$(document).ready(function () {
		$("#example1").DataTable();
	});
</script>