<?php echo $header ?>

<div class="content-wrapper">
  	<section class="content-header">
    	<div class="container-fluid">
      		<div class="row mb-2">
        		<div class="col-sm-6">
          			<h1>Tenaga Kerja</h1>
        		</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Manajemen User</a></li>
						<li class="breadcrumb-item active"><a href="#">Tenaga Kerja</a></li>
					</ol>
				</div>
      		</div>
    	</div>
  	</section>
	<section class="content">
		<div class="card card-outline card-warning mx-2">
			<div class="card-header">
				<h3 class="card-title">Data Kelulusan Tenaga Kerja</h3>
				<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
						<i class="fas fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Lengkap</th>
                            <th>NIK</th>
                            <th>JK</th>
                            <th>Jabatan</th>
                            <th>SKA / SKT</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; foreach($tenaker as $row) { ?> 
                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $row->tenaker_nama ?></td>
                                <td><?php echo $row->tenaker_nik ?></td>
                                <td><?php echo $row->jabatan_nama ?></td>
                                <td><?php echo ($row->tenaker_jkel == "L" ? "Laki-Laki" : "Perempuan") ?></td>
                                <td><?php echo $row->skat ?></td>
                                <td>
                                    <button class="btn btn-warning btn-xs validasi" data-uid="<?php echo $row->id_tenaga_kerja_url ?>">Detail</button>
                                </td>
                            </tr>
                        <?php $no++; } ?>
                    </tbody>
                </table>
			</div>
		</div>
	</section>
</div>

<div class="modal fade" id="validasiTenaker">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="form-horizontal" action="#" method="post" style="font-size: 15px">
                <div class="modal-header bg-yellow">
                    <h4 class="modal-title hapus-title">SKA / SKT</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Sertifikat</th>
                                <th>Nomor Sertifikat</th>
                                <th>Jenis</th>
                                <th>Tanggal Terbit</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table-sertifikat"></tbody>
                    </table>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="validasiSertifikat">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" action="#" method="post" style="font-size: 15px">
                <div class="modal-header bg-yellow">
                    <h4 class="modal-title hapus-title">Validasi Kompeten SKA / SKT</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Apakah anda yakin akan memvalidasi sertifikat ini?
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <a href="#" class="btn btn-success" id="validasi-sertifikat">Validasi</a>
                </div>
            </form>
        </div>
    </div>
</div>

<?php echo $footer ?>

<script>
    $(document).ready(function() {
		<?php 
            $statusAct = $this->session->flashdata("status");
            $act = $this->session->flashdata("act");
            
            if($statusAct == 1) { ?>
                toastr.success('<?php echo $act ?> berhasil dilakukan')
            <?php } else if($statusAct == 2) { ?>
                toastr.error('Terjadi kesalahan pada saat <?php echo $act ?>')
            <?php }
        ?>

        $("#example1").DataTable();

        $(".validasi").on("click", function(e) {
            let uid = $(this).data('uid');
            $("#validasiTenaker").modal("show");
            
            $.ajax({
                url: "<?php echo site_url('tenaker/get_sertifikat_tenaker_iappi/') ?>"+uid,
				dataType: "JSON",
                success: function(data) {
                    $("#table-sertifikat").html(data.output);
                },
                error: function() {
                    toastr.error('Gagal mengambil data');
                }
            })        
        });
    });

    function validasiSertifikat() {
        let uid = $(".btn-validasi").data("uid");
        $("#validasi-sertifikat").attr("href", "<?php echo site_url('tenaker/validasi_sertifikat_kompeten/') ?>"+uid);
        $("#validasiSertifikat").modal("show");
    }
</script>
