<?php echo $header ?>

<div class="content-wrapper">
  	<section class="content-header">
    	<div class="container-fluid">
      		<div class="row mb-2">
        		<div class="col-sm-6">
          			<h1>Data User Tenaga Kerja</h1>
        		</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Manajemen User</a></li>
						<li class="breadcrumb-item active"><a href="#">Tenaga Kerja</a></li>
					</ol>
				</div>
      		</div>
    	</div>
  	</section>
	<section class="content">
        <a href="<?php echo site_url('user/tenaga_kerja/tambah/form') ?>" class="btn btn-sm btn-warning mx-2 mb-3">
            <i class="fas fa-plus"></i> Tambah Data
		</a>
		<div class="row">
			<div class="col-8">
				<div class="card card-warning card-outline mx-2">
					<div class="card-header">
						<h3 class="card-title">Data User</h3>
						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fas fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="card-body">
						<table id="tableTenaker" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Nama Lengkap</th>
									<th>TTL</th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
			<div class="col-4">
				<div id="empty-data" class="card">
					<div class="card-body pt-4">
						<img src="<?php echo base_url() ?>assets/images/people.svg" style="max-width: 75%; height: auto" class="col mx-auto d-block">
						<p class="mt-4 text-center">Pilih salah satu data untuk melihat data lebih detail</p>
					</div>
				</div>
				<div id="detail-data" class="d-none">
					<div class="card card-widget widget-user">
						<div class="widget-user-header bg-warning">
							<h3 class="widget-user-username mt-3" id="nama-tenaker"><strong>Alexander Pierce</strong></h3>
						</div>
						<div class="widget-user-image">
							<img class="img-circle elevation-2" src="<?php echo base_url() ?>assets/images/user.png" alt="User Avatar">
						</div>
						<div class="card-body">
							<div class="mt-5">
								<div class="row">
									<div class="col-sm-12">
										<table class="table table-borderless table-hover detail-data-table">
											<tr>
												<th width="120">TTL</th>
												<td id="ttl-tenaker"></td>
											</tr>
											<tr>
												<th>Jenis Kelamin</th>
												<td id="jkel-tenaker"></td>
											</tr>
											<tr>
												<th>Jabatan</th>
												<td id="jabatan-tenaker"></td>
											</tr>
											<tr>
												<th>Email</th>
												<td id="email-tenaker"></td>
											</tr>
											<tr>
												<th>No HP</th>
												<td id="nohp-tenaker"></td>
											</tr>
											<tr>
												<th>SKA / SKT</th>
												<td id="skat-tenaker"></td>
											</tr>
											<tr>
												<th>Username</th>
												<td id="username-tenaker"></td>
											</tr>
										</table>
										<div class="m-auto">
											<a class="btn btn-info btn-sm" id="edit-tenaker">Edit</a>
											<button class="btn btn-danger btn-sm" id="hapus-tenaker">Hapus</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<div class="modal fade" id="hapusTenaker">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <form class="form-horizontal formHapusTenaker" action="#" method="post" style="font-size: 15px">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title hapus-title"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="uid">
                    <p>Anda yakin mau menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-danger">Hapus Data</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php echo $footer ?>

<script>
    $(document).ready(function() {
		<?php 
            $statusAct = $this->session->flashdata("status");
		$act = $this->session->flashdata("act");

		if($statusAct == 1) { ?>
                toastr.success('<?php echo $act ?> berhasil dilakukan')
            <?php } else if($statusAct == 2) { ?>
                toastr.error('Terjadi kesalahan pada saat <?php echo $act ?>')
            <?php }
        ?>

		$('.modal').on("hidden.bs.modal", function(e) {
			if($('.modal:visible').length) {
				$('body').addClass('modal-open');
			}
		});

		// Datemask
		$('#tgl_lhr').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
		$('#tgl_lulus').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
		$('#tgl_terbit').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
		$('#tgl_lhr_edit').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
		$('#tgl_lulus_edit').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
		$('#tgl_terbit_edit').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })

		// Select2
		$('.select2').select2()

		// Init Datatables
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };
        
        // Show data in datatables
        var tableTenaker = $("#tableTenaker").dataTable({
            initComplete: function() {
                var api = this.api();
                    $('#tableTenaker_filter input')
                        .off('.DT')
                        .on('input.DT', function() {
                    api.search(this.value).draw();
                });
            },
			"autoWidth": false,
            oLanguage: {
                sProcessing: "Sedang Memuat..."
            },
            processing: true,
            serverSide: true,
            ajax: {
                "url": "<?php echo site_url('user/get_data_tenaga_kerja') ?>", 
                "type": "POST"
            },
            columns: [
                {"data": "tenaker_nama"},
                {
                    "data": "tenaker_tanggal_lahir", 
                    render: function(data, type, row) {
                        let dateSplit = data.split("-");
                        return type === 'display' || type === 'filter' ?
                            row.tenaker_tempat_lahir+', '+dateSplit[2]+' '+bulanIndo(dateSplit[1])+' '+dateSplit[0] :
                            data;
                    }
                },
                {"data": "action"}
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                $('td:eq(0)', row).html();
            }
        });

		// Modal detail
		$('#tableTenaker').on('click', '.lihat_tenaker', function() {
			let uid = $(this).data('uid');
			$("#empty-data").addClass('d-none');
			$("#detail-data").removeClass('d-none');

			$.ajax({
				url: "<?php echo site_url('user/get_detail_tenaga_kerja/') ?>"+uid,
				dataType: "JSON",
                success: function(data) {
					let dateSplit = data.tenaker_tanggal_lahir.split("-"); 
					let email = data.tenaker_email;
					let nohp = data.tenaker_nohp;
					if(email == null || email == "") {
						email = "<span class='badge badge-danger'>Tidak ada email</span>";
					}

					if(nohp == null || nohp == "") {
						nohp = "<span class='badge badge-danger'>Tidak ada no hp</span>";
					}
					
					$("#nama-tenaker").text(data.tenaker_nama);
					$("#ttl-tenaker").text(data.tenaker_tempat_lahir+", "+dateSplit[2]+" "+bulanIndo(dateSplit[1])+" "+dateSplit[0]);
					$("#jkel-tenaker").text(data.tenaker_jkel);
					$("#jabatan-tenaker").text(data.jabatan_nama);
					$("#email-tenaker").html(email);
					$("#nohp-tenaker").html(nohp);
					$("#skat-tenaker").text(0);
					$("#username-tenaker").text(data.tenaker_username);

					$("#edit-tenaker").attr("href", "<?php echo site_url('user/tenaga_kerja/') ?>"+data.id_tenaga_kerja_url+"/edit/form");
					$("#hapus-tenaker").attr("data-uid", data.id_tenaga_kerja_url);
					$("#hapus-tenaker").attr("data-nama", data.tenaker_nama);
				},
				error: function() {
					toastr.error('Gagal mengambil data');
				}
			});
		});
			
		// Modal hapus
		$('#detail-data').on('click','#hapus-tenaker', function() {
			let uid = $(this).data('uid');
			let nama = $(this).data('nama');

			$('#hapusTenaker').modal('show');
			$('.formHapusTenaker').attr("action", "<?php echo site_url('user/tenaga_kerja/') ?>"+uid+'/hapus');
			$('.hapus-title').text("Hapus Data - "+nama);
		});
	});

	function bulanIndo(val) {
		if(val == 1) {
			return "Januari";
		} else if(val == 2) {
			return "Februari";
		} else if(val == 3) {
			return "Maret";
		} else if(val == 4) {
			return "April";
		} else if(val == 5) {
			return "Mei";
		} else if(val == 6) {
			return "Juni";
		} else if(val == 7) {
			return "Juli";
		} else if(val == 8) {
			return "Agustus";
		} else if(val == 9) {
			return "September";
		} else if(val == 10) {
			return "Oktober";
		} else if(val == 11) {
			return "November";
		} else if(val == 12) {
			return "Desember";
		}
	}

	function get_kabkot(val) {
		let idprov = val;
		$.ajax({
			url: "<?php echo site_url('user/get_kabkot_by_id_prov/') ?>"+idprov,
			dataType: "JSON",
			success: function(data) {
				$(".kabkot").html(data.output);
			}
		})
	}
</script>
