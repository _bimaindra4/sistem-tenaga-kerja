<?php echo $header ?>

<div class="content-wrapper">
  	<section class="content-header">
    	<div class="container-fluid">
      		<div class="row mb-2">
        		<div class="col-sm-6">
          			<h1>Tambah User Tenaga Kerja</h1>
        		</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Manajemen User</a></li>
						<li class="breadcrumb-item"><a href="#">Tenaga Kerja</a></li>
						<li class="breadcrumb-item active"><a href="#">Tambah User</a></li>
					</ol>
				</div>
      		</div>
    	</div>
  	</section>
	<section class="content">
		<div class="card card-outline card-warning mx-2">
			<div class="card-header ">
				<h3 class="card-title">
                    <i class="fas fa-plus mr-2"></i>
                    Tambah User
                </h3>
				<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
						<i class="fas fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="card-body">
                <div class="row">
                    <div class="col-5 col-sm-3">
                        <div class="nav flex-column nav-tabs h-100" id="tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="dd-tab" data-toggle="pill" href="#dd" role="tab" aria-controls="dd" aria-selected="true">Data Diri</a>
                            <a class="nav-link" id="pendidikan-tab" data-toggle="pill" href="#pendidikan" role="tab" aria-controls="pendidikan" aria-selected="false">Pendidikan & Proyek</a>
                            <!--<a class="nav-link" id="skat-tab" data-toggle="pill" href="#skat" role="tab" aria-controls="skat" aria-selected="false">SKA / SKT</a>-->
                            <a class="nav-link" id="upload-tab" data-toggle="pill" href="#upload" role="tab" aria-controls="upload" aria-selected="false">Upload File</a>
                        </div>
                    </div>
                    <div class="col-7 col-sm-9">
                        <form method="post" class="form-horizontal" enctype="multipart/form-data" style="font-size: 15px" id="form-tambah">
                            <div class="tab-content" id="tabContent">
                                <div class="tab-pane fade show active" id="dd" role="tabpanel" aria-labelledby="dd-tab">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="nik">NIK</label>
                                                <input type="text" class="form-control" name="nik" placeholder="NIK">
                                            </div>
                                            <div class="form-group">
                                                <label for="nama">Nama Lengkap</label>
                                                <input type="text" class="form-control" name="nama_lengkap" placeholder="Nama Lengkap">
                                            </div>
                                            <div class="form-group">
                                                <label for="nama">Tempat Lahir</label>
                                                <input type="text" class="form-control" name="tempat_lhr" placeholder="Tempat Lahir">
                                            </div>
                                            <div class="form-group">
                                                <label for="nama">Tanggal Lahir</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                    </div>
                                                    <input type="text" name="tgl_lhr" id="tgl_lhr" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="mm/dd/yyyy" data-mask>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama">Jenis Kelamin</label>
                                                <select class="form-control" name="jkel">
                                                    <option value="">-- Pilih Jenis Kelamin --</option>
                                                    <option value="L">Laki-Laki</option>
                                                    <option value="P">Perempuan</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama">No HP</label>
                                                <input type="text" class="form-control" name="nohp" placeholder="Nomor HP">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="nama">Alamat</label>
                                                <input type="text" class="form-control" name="alamat" placeholder="Alamat">
                                            </div>
                                            <div class="form-group">
                                                <label for="nama">Provinsi</label>
                                                <select class="form-control select2" name="provinsi" onchange="get_kabkot(this.value)">
                                                    <?php foreach($provinsi as $row) { ?> 
                                                        <option value="<?php echo $row->id_provinsi ?>">
                                                            <?php echo $row->provinsi_nama ?>
                                                        </option>	
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama">Kabupaten / Kota</label>
                                                <select class="form-control select2 kabkot" name="kabkot"></select>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama">Warga Negara</label>
                                                <input type="text" class="form-control" name="kewarganegaraan" placeholder="Warga Negara">
                                            </div>
                                            <div class="form-group">
                                                <label for="nama">E-Mail</label>
                                                <input type="email" class="form-control" name="email" placeholder="E-Mail">
                                            </div>   
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pendidikan" role="tabpanel" aria-labelledby="pendidikan-tab">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="nama">Pendidikan</label>
                                                <select class="form-control select2" name="pendidikan">
                                                    <option value="SD">SD</option>
                                                    <option value="SMP">SMP</option>
                                                    <option value="SMA">SMA</option>
                                                    <option value="SMK">SMK</option>
                                                    <option value="D1">D1</option>
                                                    <option value="D2">D2</option>
                                                    <option value="D3">D3</option>
                                                    <option value="D4">D4</option>
                                                    <option value="S1">S1</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama">Institusi Pendidikan</label>
                                                <input type="text" class="form-control" name="institusi" placeholder="Institusi">
                                            </div>
                                            <div class="form-group">
                                                <label for="nama">Jurusan</label>
                                                <input type="text" class="form-control" name="jurusan" placeholder="Jurusan">
                                            </div>
                                            <div class="form-group">
                                                <label for="nama">Tanggal Lulus</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                    </div>
                                                    <input type="text" name="tgl_lulus" id="tgl_lulus" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="utusan">Utusan</label>
                                                <div class="input-group">
                                                    <select class="form-control select2" name="utusan" id="utusanPilihan" style="width: 85%"></select>
                                                    <div class="input-group-prepend">
                                                        <a class="btn btn-primary btn-flat" data-toggle="modal" data-target="#tambahUtusan" style="color: white">
                                                            <i class="fas fa-plus"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama">Nama Proyek</label>
                                                <input type="text" class="form-control" name="nama_proyek" placeholder="Nama Proyek">
                                            </div>
                                            <div class="form-group">
                                                <label for="nama">Jabatan Kerja</label>
                                                <div class="input-group">
                                                    <select class="form-control select2" name="jabatan" id="jabatanPilihan" style="width: 85%"></select>
                                                    <div class="input-group-prepend">
                                                        <a class="btn btn-primary btn-flat" data-toggle="modal" data-target="#tambahJabatan" style="color: white">
                                                            <i class="fas fa-plus"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="tab-pane fade" id="skat" role="tabpanel" aria-labelledby="skat-tab">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="nama">Jenis Sertifikat</label>
                                                <select class="form-control" name="jenis_sertif">
                                                    <option value="ahli">Ahli</option>
                                                    <option value="terampil">Terampil</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama">Nama Keahlian / Keterampilan</label>
                                                <select class="form-control select2" name="nama_sertif" id="keahlianPilihan"></select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="nama">Nomor Sertifikat</label>
                                                <input type="text" class="form-control" name="no_sertif" placeholder="No Sertifikat">
                                            </div>
                                            <div class="form-group">
                                                <label for="nama">Tanggal Sertifikat</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                    </div>
                                                    <input type="text" name="tgl_sertif" id="tgl_sertif" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask>
                                                </div>
                                            </div>
                                        </div>
										<div class="col-6">
											<div class="form-group">
												<label for="nama">Asosiasi</label>
												<select class="form-control" name="asosiasi">
													<option value="iappi">IAPPI</option>
												</select>
											</div>
										</div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-12">
                                            <button class="btn btn-primary" id="btn-tambah-sertifikat">Tambah Sertifikat</button>
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-12">
                                            <h5>Daftar SKA / SKT</h5>
                                            <table class="table table-borderd table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Nomor</th>
                                                        <th>Nama</th>
                                                        <th>Jenis</th>
                                                        <th>Tanggal</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tbl-sertifikat"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>-->
                                <div class="tab-pane fade" id="upload" role="tabpanel" aria-labelledby="upload-tab">
                                    <div class="row">
                                        <div class="col-8">
                                            <div class="form-group">
                                                <label for="exampleInputFile">Upload KTP</label>
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="exampleInputFile" name="file_foto_ktp">
                                                        <label class="custom-file-label" for="exampleInputFile">Pilih File</label>
                                                    </div>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="">Upload</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputFile">Upload Foto Diri</label>
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="exampleInputFile" name="file_foto_diri">
                                                        <label class="custom-file-label" for="exampleInputFile">Pilih File</label>
                                                    </div>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="">Upload</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputFile">Upload Ijazah</label>
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="exampleInputFile" name="file_ijazah">
                                                        <label class="custom-file-label" for="exampleInputFile">Pilih File</label>
                                                    </div>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="">Upload</span>
                                                    </div>
                                                </div>
                                            </div>
                                            
											<div class="form-group">
												<label for="exampleInputFile">Upload NPWP</label>
												<div class="input-group">
													<div class="custom-file">
														<input type="file" class="custom-file-input" id="exampleInputFile" name="file_npwp">
														<label class="custom-file-label" for="exampleInputFile">Pilih File</label>
													</div>
													<div class="input-group-append">
														<span class="input-group-text" id="">Upload</span>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label for="nama">No NPWP</label>
												<input type="text" class="form-control" name="no_npwp" placeholder="No NPWP">
											</div>
											<div class="form-group">
												<label for="exampleInputFile">Upload CV</label>
												<div class="input-group">
													<div class="custom-file">
														<input type="file" class="custom-file-input" id="exampleInputFile" name="file_cv">
														<label class="custom-file-label" for="exampleInputFile">Pilih File</label>
													</div>
													<div class="input-group-append">
														<span class="input-group-text" id="">Upload</span>
													</div>
												</div>
											</div>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
		</div>
	</section>
</div>

<div class="modal fade" id="tambahUtusan">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
        	<form class="form-horizontal" action="<?php echo site_url('tenaker/utusan/tambah') ?>" id="formTambahUtusan" method="post" style="font-size: 15px">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title">Tambah Utusan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Nama Utusan</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama_utusan" placeholder="Nama Utusan">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Alamat Utusan</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="alamat_utusan"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="tambahJabatan">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
        	<form class="form-horizontal" action="<?php echo site_url('tenaker/jabatan/tambah') ?>" id="formTambahJabatan" method="post" style="font-size: 15px">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title">Tambah Jabatan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Nama Jabatan</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama" placeholder="Nama Jabatan">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Klasifikasi</label>
                        <div class="col-sm-9">
							<input type="text" class="form-control" name="klasifikasi" placeholder="Jabatan Klasifikasi">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Kualifikasi</label>
                        <div class="col-sm-9">
							<input type="text" class="form-control" name="kualifikasi" placeholder="Jabatan Kualifikasi">
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php echo $footer ?>

<script>
    let sertifikat = [];
    let tabel = $("#tbl-sertifikat");
    let isi =
        "<tr>"+
            "<td colspan='5' class='text-center'>Data Kosong</td>"+
        "</tr>";
    tabel.html(isi);

    $(document).ready(function() {
        <?php 
            $statusAct = $this->session->flashdata("status");
            $act = $this->session->flashdata("act");
            
            if($statusAct == 1) { ?>
                toastr.success('<?php echo $act ?> berhasil dilakukan')
            <?php } else if($statusAct == 2) { ?>
                toastr.error('Terjadi kesalahan pada saat <?php echo $act ?>')
            <?php }
        ?>
        
		$('.modal').on("hidden.bs.modal", function (e) {
			if($('.modal:visible').length) {
				$('body').addClass('modal-open');
			}
		});

        // Custom File Input
        bsCustomFileInput.init();

		// Datemask
		$('#tgl_lhr').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
		$('#tgl_lulus').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
		$('#tgl_terbit').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
		$('#tgl_sertif').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
		$('#tgl_lhr_edit').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
		$('#tgl_lulus_edit').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
		$('#tgl_terbit_edit').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })

		// Select2
		$('.select2').select2()

		// load dropdown utusan
		$("#utusanPilihan").load("<?php echo site_url('User/show_data_utusan_tenaker') ?>");
		$("#utusanPilihanEdit").load("<?php echo site_url('User/show_data_utusan_tenaker') ?>");
		$("#jabatanPilihan").load("<?php echo site_url('User/show_data_jabatan_tenaker') ?>");
		$("#keahlianPilihan").load("<?php echo site_url('User/show_data_jabatan_tenaker') ?>");

		$("#formTambahUtusan").submit(function(e) {
			e.preventDefault();
			let form = $(this);
			var post_url = form.attr('action');
			let form_data = form.serialize();

			$.ajax({
				url: post_url,
				type: "POST",
				data: form_data,
				success: function(data) {
					toastr.success('Tambah data utusan berhasil dilakukan');
					$("#tambahUtusan").modal("hide");
					$("#utusanPilihan").html("");
					$("#utusanPilihan").load("<?php echo site_url('User/show_data_utusan_tenaker') ?>");

					$("#utusanPilihanEdit").html("");
					$("#utusanPilihanEdit").load("<?php echo site_url('User/show_data_utusan_tenaker') ?>");
				}
			});
		});

		$("#formTambahJabatan").submit(function(e) {
			e.preventDefault();
			let form = $(this);
			var post_url = form.attr('action');
			let form_data = form.serialize();

			$.ajax({
				url: post_url,
				type: "POST",
				data: form_data,
				success: function(data) {
					toastr.success('Tambah data jabatan berhasil dilakukan');
					$("#tambahJabatan").modal("hide");
					$("#jabatanPilihan").html("");
					$("#jabatanPilihan").load("<?php echo site_url('User/show_data_jabatan_tenaker') ?>");

					$("#jabatanPilihanEdit").html("");
					$("#jabatanPilihanEdit").load("<?php echo site_url('User/show_data_jabatan_tenaker') ?>");
				}
			});
		});

        $("#btn-tambah-sertifikat").on("click", function(e) {
            e.preventDefault();
            
            let jenis = $("[name='jenis_sertif']").val();
            let namaVal = $("[name='nama_sertif']").val();
            let namaText = $("#keahlianPilihan").select2("data")[0];
            let no = $("[name='no_sertif']").val();
            let tanggal = $("[name='tgl_sertif']").val();
            let tanggalSplit = tanggal.split("/");
            let tanggalDB = tanggalSplit[2]+"-"+tanggalSplit[1]+"-"+tanggalSplit[0];
            let res = "";
            let i = 0;

            let data_sertif = [namaVal, namaText.text, jenis, no, tanggalDB];
            sertifikat[].push(data_sertif);

            if(sertifikat.length == 0) {
                res = 
                "<tr>"+
                    "<td colspan='5' class='text-center'>Data Kosong</td>"+
                "</tr>";
            } else {
                for(i<0; i<sertifikat.length; i++) {
                    let splitTglDB = sertifikat[i][4].split("-");
                    let propHapus = 'hapusSertifikat("'+sertifikat[i][3]+'","'+sertifikat[i][0]+'")';

                    res += 
                    "<tr>"+
                        "<td>"+sertifikat[i][3]+"</td>"+
                        "<td>"+sertifikat[i][1]+"</td>"+
                        "<td>"+capitalize(sertifikat[i][2])+"</td>"+
                        "<td>"+splitTglDB[2]+" "+bulanIndo(splitTglDB[1])+" "+splitTglDB[0]+"</td>"+
                        "<td><a class='btn btn-danger btn-xs' style='color: white' onclick='"+propHapus+"'>Hapus</a></td>"+
                    "</tr>";
                }
            }

            tabel.html(res);
        });

        $("#form-tambah").submit(function(e) {
            e.preventDefault();
            let form = $(this);
			let form_data = new FormData($(this)[0]);
			console.log(sertifikat);
			form_data.append("sertifikat[]", sertifikat);

            $.ajax({
				url: "<?php echo site_url('user/tenaga_kerja/tambah') ?>",
				type: "POST",
				dataType: 'json',
				data: form_data,
                processData: false,
                contentType: false,
				success: function(data) {
				    console.log(data.sertif);
                    //location.href = "<?php //echo site_url('user/tenaga_kerja') ?>//";
				},
                error: function() {
                    console.log("error");
                }
			});
        });
    });

    function hapusSertifikat(no,nama) {
        let i = 0;
        let j = 0;
        let res = ""
        let isAvailable = false;

        for(i<0; i<sertifikat.length; i++) {
            if(sertifikat[i].includes(no) && sertifikat[i].includes(nama)) {
                isAvailable = true;
                sertifikat.splice(i, 1);
            }
        }

        tabel.html("");
        if(sertifikat.length == 0) {
            res = 
            "<tr>"+
                "<td colspan='5' class='text-center'>Data Kosong</td>"+
            "</tr>";
        } else {
            for(j<0; j<sertifikat.length; j++) {
                let splitTglDB = sertifikat[j][4].split("-");
                let propHapus = 'hapusSertifikat("'+sertifikat[j][3]+'","'+sertifikat[j][0]+'")';

                res += 
                "<tr>"+
                    "<td>"+sertifikat[j][3]+"</td>"+
                    "<td>"+sertifikat[j][1]+"</td>"+
                    "<td>"+capitalize(sertifikat[j][2])+"</td>"+
                    "<td>"+splitTglDB[2]+" "+bulanIndo(splitTglDB[1])+" "+splitTglDB[0]+"</td>"+
                    "<td><a class='btn btn-danger btn-xs' style='color: white' onclick='"+propHapus+"'>Hapus</a></td>"+
                "</tr>";
            }
        }

        tabel.html(res);
    }

    function get_kabkot(val) {
        let idprov = val;
        $.ajax({
            url: "<?php echo site_url('user/get_kabkot_by_id_prov/') ?>"+idprov,
            dataType: "JSON",
            success: function(data) {
                $(".kabkot").html(data.output);
            }
        })
    }

    function bulanIndo(val) {
        if(val == 1) {
            return "Januari";
        } else if(val == 2) {
            return "Februari";
        } else if(val == 3) {
            return "Maret";
        } else if(val == 4) {
            return "April";
        } else if(val == 5) {
            return "Mei";
        } else if(val == 6) {
            return "Juni";
        } else if(val == 7) {
            return "Juli";
        } else if(val == 8) {
            return "Agustus";
        } else if(val == 9) {
            return "September";
        } else if(val == 10) {
            return "Oktober";
        } else if(val == 11) {
            return "November";
        } else if(val == 12) {
            return "Desember";
        }
    }

    const capitalize = (s) => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    }
</script>
