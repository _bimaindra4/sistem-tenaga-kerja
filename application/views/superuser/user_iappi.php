<?php echo $header ?>

<div class="content-wrapper">
  	<section class="content-header">
    	<div class="container-fluid">
      		<div class="row mb-2">
        		<div class="col-sm-6">
          			<h1>Data User IAPPI</h1>
        		</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Manajemen User</a></li>
						<li class="breadcrumb-item active"><a href="#">IAPPI</a></li>
					</ol>
				</div>
      		</div>
    	</div>
  	</section>
	<section class="content">
        <button type="button" class="btn btn-sm btn-primary mx-2 mb-3" data-toggle="modal" data-target="#tambahIAPPI">
            <i class="fas fa-plus"></i> Tambah Data</button>
		<div class="card mx-2">
			<div class="card-header">
				<h3 class="card-title">Data User</h3>
				<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
						<i class="fas fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="card-body">
                <table id="tableIAPPI" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Nama Lengkap</th>
                            <th>Jenis Kelamin</th>
                            <th>TTL</th>
                            <th>No Telepon</th>
                            <th>Email</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
			</div>
		</div>
	</section>
</div>

<div class="modal fade" id="tambahIAPPI">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
        <form class="form-horizontal" action="<?php echo site_url('user/iappi/tambah') ?>" method="post" style="font-size: 15px">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title">Tambah Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Nama Lengkap</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Tempat Lahir</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="tempat" placeholder="Tempat Lahir">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Tanggal Lahir</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                <input type="text" name="tanggal" id="datemask" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Jenis Kelamin</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="jkel">
                                <option value="">-- Pilih Jenis Kelamin --</option>
                                <option value="L">Laki-Laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Alamat</label>
                        <div class="col-sm-9">
                            <textarea name="alamat" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Pendidikan</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="pendidikan">
                                <option value="">-- Pilih Pendidikan Terakhir --</option>
                                <option value="SD">SD</option>
                                <option value="SMP">SMP</option>
                                <option value="SMA">SMA</option>
                                <option value="SMK">SMK</option>
                                <option value="D1">D1</option>
                                <option value="D2">D2</option>
                                <option value="D3">D3</option>
                                <option value="D4">D4</option>
                                <option value="S1">S1</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Pekerjaan</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="pekerjaan" placeholder="Pekerjaan">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Warga Negara</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="warga_negara" placeholder="Warga Negara">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Sertifikat</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="sertifikat" placeholder="Sertifikat Keahlian">
                        </div>
                    </div> -->
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">No HP</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nohp" placeholder="Nomor HP">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">E-Mail</label>
                        <div class="col-sm-9">
                            <input type="email" class="form-control" name="email" placeholder="E-Mail">
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="updateIAPPI">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <form class="form-horizontal formUpdateIAPPI" action="#" method="post" style="font-size: 15px">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title update-title"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Nama Lengkap</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="u_nama">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Tempat Lahir</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="u_tempat">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Tanggal Lahir</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                <input type="text" name="u_tanggal" id="u_datemask" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Jenis Kelamin</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="u_jkel">
                                <option value="">-- Pilih Jenis Kelamin --</option>
                                <option value="L">Laki-Laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Alamat</label>
                        <div class="col-sm-9">
                            <textarea name="u_alamat" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Pendidikan</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="u_pendidikan">
                                <option value="">-- Pilih Pendidikan Terakhir --</option>
                                <option value="SD">SD</option>
                                <option value="SMP">SMP</option>
                                <option value="SMA">SMA</option>
                                <option value="SMK">SMK</option>
                                <option value="D1">D1</option>
                                <option value="D2">D2</option>
                                <option value="D3">D3</option>
                                <option value="D4">D4</option>
                                <option value="S1">S1</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Pekerjaan</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="u_pekerjaan">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Warga Negara</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="u_warga_negara">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Sertifikat</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="u_sertifikat">
                        </div>
                    </div> -->
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">No HP</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="u_nohp">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">E-Mail</label>
                        <div class="col-sm-9">
                            <input type="email" class="form-control" name="u_email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Username</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="u_username">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Password</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" name="u_password">
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Edit Data</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="hapusIAPPI">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <form class="form-horizontal formHapusIAPPI" action="#" method="post" style="font-size: 15px">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title hapus-title"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="uid">
                    <p>Anda yakin mau menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-danger">Hapus Data</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php echo $footer ?>

<script>
    $(document).ready(function() {
        <?php 
            $statusAct = $this->session->flashdata("status");
            $act = $this->session->flashdata("act");
            
            if($statusAct == 1) { ?>
                toastr.success('<?php echo $act ?> berhasil dilakukan')
            <?php } else if($statusAct == 2) { ?>
                toastr.error('Terjadi kesalahan pada saat <?php echo $act ?>')
            <?php }
        ?>

        function bulanIndo(val) {
            if(val == 1) {
                return "Januari";
            } else if(val == 2) {
                return "Februari";
            } else if(val == 3) {
                return "Maret";
            } else if(val == 4) {
                return "April";
            } else if(val == 5) {
                return "Mei";
            } else if(val == 6) {
                return "Juni";
            } else if(val == 7) {
                return "Juli";
            } else if(val == 8) {
                return "Agustus";
            } else if(val == 9) {
                return "September";
            } else if(val == 10) {
                return "Oktober";
            } else if(val == 11) {
                return "November";
            } else if(val == 12) {
                return "Desember";
            }
        }

        $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
        $('#u_datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })

        // Init Datatables
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };
        
        // Show data in datatables
        var tableIAPPI = $("#tableIAPPI").dataTable({
            initComplete: function() {
                var api = this.api();
                    $('#tableIAPPI_filter input')
                        .off('.DT')
                        .on('input.DT', function() {
                    api.search(this.value).draw();
                });
            },
            oLanguage: {
                sProcessing: "Sedang Memuat..."
            },
            processing: true,
            serverSide: true,
            ajax: {
                "url": "<?php echo site_url('user/get_data_iappi') ?>", 
                "type": "POST"
            },
            columns: [
                {"data": "iappi_nama"},
                {
                    "data": "iappi_jkel",
                    render: function(data, type, row) {
                        return data == "L" ? "Laki-Laki" : "Perempuan";
                    }  
                },
                {
                    "data": "iappi_tanggal_lahir", 
                    render: function(data, type, row) {
                        let dateSplit = data.split("-");
                        return type === 'display' || type === 'filter' ?
                            row.iappi_tempat_lahir+', '+dateSplit[2]+' '+bulanIndo(dateSplit[1])+' '+dateSplit[0] :
                            data;
                    }
                },
                {"data": "iappi_notelp"},
                {"data": "iappi_email"},
                {"data": "action"}
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                $('td:eq(0)', row).html();
            }
        });

        // Modal update
        $('#tableIAPPI').on('click','.edit_iappi',function() {
            let uid = $(this).data('uid');
            $.ajax({
                url: "<?php echo site_url('user/get_detail_iappi/') ?>"+uid,
                dataType: "JSON",
                success: function(data) {
                    let tanggal = data.iappi_tanggal_lahir;
                    let splitTanggal = tanggal.split("-");

                    $('#updateIAPPI').modal('show');
                    $('.formUpdateIAPPI').attr("action", "<?php echo site_url('user/iappi/') ?>"+uid+'/edit');
                    $('.update-title').text("Update Data - "+data.iappi_nama);
                    $('[name="u_nama"]').val(data.iappi_nama);
                    $('[name="u_tempat"]').val(data.iappi_tempat_lahir);
                    $('[name="u_tanggal"]').val(splitTanggal[2]+"/"+splitTanggal[1]+"/"+splitTanggal[0]);
                    $('[name="u_jkel"]').val(data.iappi_jkel);
                    // $('[name="u_alamat"]').text(data.iappi_alamat);
                    // $('[name="u_pendidikan"]').val(data.iappi_pendidikan);
                    // $('[name="u_pekerjaan"]').val(data.iappi_pekerjaan);
                    // $('[name="u_warga_negara"]').val(data.iappi_kewarganegaraan);
                    // $('[name="u_sertifikat"]').val(data.iappi_sertifikat_ahli);
                    $('[name="u_nohp"]').val(data.iappi_notelp);
                    $('[name="u_email"]').val(data.iappi_email);
                    $('[name="u_username"]').val(data.iappi_username);
                },

                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Gagal mengambil data');
                }
            });
        });

        // Modal hapus
        $('#tableIAPPI').on('click','.hapus_iappi',function() {
            let uid = $(this).data('uid');
            let nama = $(this).data('nama');

            $('#hapusIAPPI').modal('show');
            $('.formHapusIAPPI').attr("action", "<?php echo site_url('user/iappi/') ?>"+uid+'/hapus');
            $('.hapus-title').text("Hapus Data - "+nama);
        });
    });
</script>