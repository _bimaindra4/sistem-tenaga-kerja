<?php echo $header ?>

<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Dashboard</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item active"><a href="#">Dashboard</a></li>
					</ol>
				</div>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-3 col-6">
					<div class="small-box bg-info">
						<div class="inner ml-2">
							<h3><?php echo $iappi ?></h3>
							<p>User IAPPI</p>
						</div>
						<div class="icon">
							<i class="fa fa-user"></i>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-6">
					<div class="small-box bg-warning">
						<div class="inner ml-2">
							<h3><?php echo $ap3i ?></h3>
							<p>User AP3I</p>
						</div>
						<div class="icon">
							<i class="fa fa-user"></i>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-6">
					<div class="small-box bg-success">
						<div class="inner ml-2">
							<h3><?php echo $balaijasa ?></h3>
							<p>User Balai Jasa</p>
						</div>
						<div class="icon">
							<i class="fa fa-user"></i>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-6">
					<div class="small-box bg-danger">
						<div class="inner ml-2">
							<h3><?php echo $tenaker ?></h3>
							<p>Tenaga Kerja</p>
						</div>
						<div class="icon">
							<i class="fa fa-user"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Peta Sebaran Tenaga Kerja</h3>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
								title="Collapse">
							<i class="fas fa-minus"></i>
						</button>
						<button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip"
								title="Remove">
							<i class="fas fa-times"></i>
						</button>
					</div>
				</div>
				<div class="card-body">
					<div id='map' style="width: 100%; height: 500px"></div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php echo $footer ?>

<script>
    var googleRoadmap = new L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });
    var googleSatellite = new L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });
    var googleHybrid = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });
    var mpn = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

    var baseMaps = {
        'Google Roadmap': googleRoadmap, 'Google Satellite': googleSatellite,
        'Google Hybrid': googleHybrid, 'Mapnik': mpn
    };

    // Marker
    var poi = L.icon({
        iconUrl: '<?php echo base_url() ?>assets/marker.png',
        iconSize: [30, 40],
        iconAnchor: [15, 35]
    });

    var map = new L.Map('map', {
        center: new L.LatLng(-2.8020048, 118.3481111),
        zoom: 5,
        layers: [googleRoadmap]
    });

    // Init Marker Cluster
    var markers = new L.MarkerClusterGroup();
    var markersList = [];

    function tenaker() {
		<?php foreach($sebaran_tenaker as $row) { ?>
			<?php 
				$lat = (float) $row->lat;
				$lng = (float) $row->lng;
				$nama = $row->utusan_tenaker;
				$alamat = "";

				if($row->utusan_alamat == NULL) {
					$alamat = "Tidak ada alamat";
				} else {
					$alamat = $row->utusan_alamat;
				}
			?>

			var content = '<p><?= $nama ?></p>'+
			'<p><?= $alamat ?></p>'+
			'<button class="btn btn-sm btn-primary">Detail</button>';

            var m = new L.Marker(new L.LatLng(<?= $lat ?>, <?= $lng ?>), {icon: poi}).bindPopup(content);
            markersList.push(m);
            markers.addLayer(m);
		<?php } ?>

        return false;
    }

    tenaker();
    map.addLayer(markers);

    map.addControl(new L.Control.Scale());
    map.addControl(new L.Control.Layers(baseMaps));
</script>