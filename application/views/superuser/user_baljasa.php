<?php echo $header ?>

<div class="content-wrapper">
  	<section class="content-header">
    	<div class="container-fluid">
      		<div class="row mb-2">
        		<div class="col-sm-6">
          			<h1>Data User Balai Jasa</h1>
        		</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Manajemen User</a></li>
						<li class="breadcrumb-item active"><a href="#">Balai Jasa</a></li>
					</ol>
				</div>
      		</div>
    	</div>
  	</section>
	<section class="content">
        <button type="button" class="btn btn-sm btn-primary mx-2 mb-3" data-toggle="modal" data-target="#tambahBaljas">
            <i class="fas fa-plus"></i> Tambah Data</button>
		<div class="card mx-2">
			<div class="card-header">
				<h3 class="card-title">Data User</h3>
				<div class="card-tools">
					<button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
						<i class="fas fa-minus"></i>
					</button>
				</div>
			</div>
			<div class="card-body">
                <table id="tableBaljas" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID User</th>
                            <th>Nama Lengkap</th>
                            <th>Username</th>
                            <th>Created Date</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
			</div>
		</div>
	</section>
</div>

<div class="modal fade" id="tambahBaljas">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
        <form class="form-horizontal" action="<?php echo site_url('user/balai_jasa/tambah') ?>" method="post" style="font-size: 15px">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title">Tambah Data</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="nama" class="col-sm-3 col-form-label">Nama Lengkap</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap">
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="updateBaljas">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <form class="form-horizontal formUpdateBaljas" action="#" method="post" style="font-size: 15px">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title update-title"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="u_nama" class="col-sm-3 col-form-label">Nama Lengkap</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="u_nama">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="u_username" class="col-sm-3 col-form-label">Username</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="u_username">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="u_password" class="col-sm-3 col-form-label">Password</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" name="u_password" placeholder="Password">
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Edit Data</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="hapusBaljas">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <form class="form-horizontal formHapusBaljas" action="#" method="post" style="font-size: 15px">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title hapus-title"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="uid">
                    <p>Anda yakin mau menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-danger">Hapus Data</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php echo $footer ?>

<script>
    $(document).ready(function() {
        <?php 
            $statusAct = $this->session->flashdata("status");
            $act = $this->session->flashdata("act");
            
            if($statusAct == 1) { ?>
                toastr.success('<?php echo $act ?> berhasil dilakukan')
            <?php } else if($statusAct == 2) { ?>
                toastr.error('Terjadi kesalahan pada saat <?php echo $act ?>')
            <?php }
        ?>

        function bulanIndo(val) {
            if(val == 1) {
                return "Januari";
            } else if(val == 2) {
                return "Februari";
            } else if(val == 3) {
                return "Maret";
            } else if(val == 4) {
                return "April";
            } else if(val == 5) {
                return "Mei";
            } else if(val == 6) {
                return "Juni";
            } else if(val == 7) {
                return "Juli";
            } else if(val == 8) {
                return "Agustus";
            } else if(val == 9) {
                return "September";
            } else if(val == 10) {
                return "Oktober";
            } else if(val == 11) {
                return "November";
            } else if(val == 12) {
                return "Desember";
            }
        }

        // Init Datatables
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        // Show data in datatables
        var tableBaljas = $("#tableBaljas").dataTable({
            initComplete: function() {
                var api = this.api();
                    $('#tableBaljas_filter input')
                        .off('.DT')
                        .on('input.DT', function() {
                    api.search(this.value).draw();
                });
            },
            oLanguage: {
                sProcessing: "Sedang Memuat..."
            },
            processing: true,
            serverSide: true,
            ajax: {
                "url": "<?php echo site_url('user/get_data_balai_jasa') ?>", 
                "type": "POST"
            },
            columns: [
                {"data": "id_balai_jasa_url"},
                {"data": "baljas_nama"},
                {"data": "baljas_username"},
                {
                    "data": "created_at", 
                    render: function(data, type, row) {
                        let caSplit = data.split(" ");
                        let dateSplit = caSplit[0].split("-");
                        return type === 'display' || type === 'filter' ?
                            dateSplit[2]+' '+bulanIndo(dateSplit[1])+' '+dateSplit[0] :
                            data;
                    }
                },
                {"data": "action"}
            ],
            order: [1, "ASC"],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                $('td:eq(0)', row).html();
            }
        });

        // Modal update
        $('#tableBaljas').on('click','.edit_baljas',function() {
            let uid = $(this).data('uid');
            $.ajax({
                url: "<?php echo site_url('user/get_detail_balai_jasa/') ?>"+uid,
                dataType: "JSON",
                success: function(data) {
                    $('#updateBaljas').modal('show');
                    $('.formUpdateBaljas').attr("action", "<?php echo site_url('user/balai_jasa/') ?>"+uid+'/edit');
                    $('.update-title').text("Update Data - "+data.baljas_nama);
                    $('[name="u_nama"]').val(data.baljas_nama);
                    $('[name="u_username"]').val(data.baljas_username);
                },

                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Gagal mengambil data');
                }
            });
        });

        // Modal hapus
        $('#tableBaljas').on('click','.hapus_baljas',function() {
            let uid = $(this).data('uid');
            let nama = $(this).data('nama');

            $('#hapusBaljas').modal('show');
            $('.formHapusBaljas').attr("action", "<?php echo site_url('user/balai_jasa/') ?>"+uid+'/hapus');
            $('.hapus-title').text("Hapus Data - "+nama);
        });
    });
</script>