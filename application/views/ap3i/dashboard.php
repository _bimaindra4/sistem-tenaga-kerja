<?php echo $header ?>

<div class="content-wrapper">
  	<section class="content-header">
    	<div class="container-fluid">
      		<div class="row mb-2">
        		<div class="col-sm-6">
          			<h1>Dashboard</h1>
        		</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item active"><a href="#">Dashboard</a></li>
					</ol>
				</div>
      		</div>
    	</div>
  	</section>
	<section class="content">
		<?php
			$status_log = $this->session->flashdata("status_log");
			if($status_log != NULL) {
				if($status_log == "1") {
		?>
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h5><i class="icon fas fa-check"></i> Login Sukses!</h5>
				Selamat Datang <?php echo $usernama ?>
			</div>
		<?php }} ?>
	</section>
</div>
    
<?php echo $footer ?>