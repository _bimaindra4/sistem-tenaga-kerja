        <footer class="main-footer">
            <strong>Copyright &copy; 2020 IAPPI Indonesia.
        </footer>

        <aside class="control-sidebar control-sidebar-dark">
        </aside>
    </div>

    <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/moment/moment.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/toastr/toastr.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/select2/js/select2.full.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
    <script src="<?php echo base_url() ?>assets/dist/js/adminlte.min.js"></script>
		<script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet-src.js"
				integrity="sha512-WXoSHqw/t26DszhdMhOXOkI7qCiv5QWXhH9R7CgvgZMHz1ImlkVQ3uNsiQKu5wwbbxtPzFXd1hK4tzno2VqhpA=="
				crossorigin=""></script>
	<script src="<?php echo base_url() ?>assets/dist/pancontrol/L.Control.Pan.js" ></script>
	<script src="<?php echo base_url() ?>assets/dist/leaflet.markercluster.js"></script>
	<script src="<?php echo base_url() ?>assets/dist/Marker.Text.js"></script>
	<script src="<?php echo base_url() ?>assets/dist/Bing.js"></script>
	<script src="<?php echo base_url() ?>assets/dist/Google.js"></script>
    <script src="<?php echo base_url() ?>assets/dist/js/demo.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    
    </body>
</html>
