<li class="nav-header">MENU SUPER USER</li>
<li class="nav-item has-treeview">
    <a href="#" class="nav-link">
        <i class="nav-icon far fa-user"></i>
        <p>Manajemen User <i class="fas fa-angle-left right"></i></p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="<?php echo site_url('user/iappi') ?>" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Data User IAPPI</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?php echo site_url('user/ap3i') ?>" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Data User AP3I</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?php echo site_url('user/balai_jasa') ?>" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Data User Balai Jasa</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?php echo site_url('user/tenaga_kerja') ?>" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                <p>Data User Tenaga Kerja</p>
            </a>
        </li>
    </ul>
</li>
