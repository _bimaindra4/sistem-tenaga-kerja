<li class="nav-header">MENU BALAI JASA</li>
<li class="nav-item">
    <a href="<?php echo site_url('tenaker') ?>" class="nav-link">
        <i class="nav-icon fas fa-user"></i>
        <p>Tenaga Kerja</p>
    </a>
</li>
<li class="nav-item">
	<a href="<?php echo site_url('bimtek') ?>" class="nav-link">
		<i class="nav-icon fas fa-pen"></i>
		<p>Bimbingan Teknis</p>
	</a>
</li>
