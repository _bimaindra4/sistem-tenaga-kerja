<li class="nav-header">MENU TENAGA KERJA</li>
<li class="nav-item">
	<a href="<?php echo site_url('profil') ?>" class="nav-link">
		<i class="nav-icon fas fa-user"></i>
		<p>Profil Tenaga Kerja</p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo site_url('sertifikat') ?>" class="nav-link">
		<i class="nav-icon fas fa-certificate"></i>
		<p>Sertifikat Keahlian</p>
	</a>
</li>
<li class="nav-item">
	<a href="<?php echo site_url('bimtek') ?>" class="nav-link">
		<i class="nav-icon fas fa-pen"></i>
		<p>Bimbingan Teknis</p>
	</a>
</li>
