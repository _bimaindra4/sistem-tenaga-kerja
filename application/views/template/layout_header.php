<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Sistem Tenaga Kerja Konstruksi</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/fontawesome-free/css/all.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/toastr/toastr.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/select2/css/select2.min.css">
  	<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/adminlte.min.css">
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css"
		  integrity="sha512-07I2e+7D8p6he1SIM+1twR5TIrhUQn9+I6yjqD53JQjFiMf8EtC93ty0/5vJTZGF8aAocvHYNEDJajGdNx1IsQ=="
		  crossorigin=""/>
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/MarkerCluster.css"/>
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/MarkerCluster.Default.css"/>
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/pancontrol/L.Control.Pan.css" />
	<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	<link href='https://api.mapbox.com/mapbox-gl-js/v1.4.1/mapbox-gl.css' rel='stylesheet'>
	<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet">
	<link rel="icon" href="<?php echo base_url() ?>/favicon.ico" type="image/x-icon">
	
	<style>
		.fa-xs, .fa-circle {
			font-size: .5em !important;
		}

		.table td, .table th {
			padding: .5em;
			font-size: 15px;
		}

		.nav-pills .nav-link.active, .nav-pills .show>.nav-link {
			color: #1f2d3d;
			background-color: #ffc107;
		}

		.nav-pills .nav-link:not(.active):hover {
			color: #ffc107;
		}
	</style>
</head>

<?php 
	if($menu == 6) {
		$menu_show = $this->load->view('template/menu_superadmin', '', TRUE);
		$icon = "user.png";
	} else if($menu == 1) {
		$menu_show = $this->load->view('template/menu_pupr', '', TRUE);
		$icon = "logo/pupr_circle.png";
	} else if($menu == 2) {
		$menu_show = $this->load->view('template/menu_ap3i', '', TRUE);
		$icon = "logo/ap3i_circle.png";
	} else if($menu == 3) {
		$menu_show = $this->load->view('template/menu_iappi', '', TRUE);
		$icon = "logo/iappi_circle.png";
	} else if($menu == 4) {
		$menu_show = $this->load->view('template/menu_balai_jasa', '', TRUE);
		$icon = "logo/pupr_circle.png";
	} else if($menu == 5) {
		$menu_show = $this->load->view('template/menu_tenaga_kerja', '', TRUE);
		$icon = "user.png";
	}
?>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
	<nav class="main-header navbar navbar-expand navbar-white navbar-light">
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
			</li>
		</ul>

		<form class="form-inline ml-3">
			<div class="input-group input-group-sm">
				<input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
				<div class="input-group-append">
					<button class="btn btn-navbar" type="submit">
						<i class="fas fa-search"></i>
					</button>
				</div>
			</div>
		</form>

		<ul class="navbar-nav ml-auto">
			<li class="nav-item">
				<a style="color: white" class="nav-link btn btn-primary btn-sm" href="<?php echo site_url('logout') ?>">
					<b>Logout</b> <i class="ml-2 fas fa-sign-out-alt"></i>
				</a>
			</li>
		</ul>
	</nav>

	<aside class="main-sidebar sidebar-dark-primary elevation-4">
		<a href="<?php echo site_url('dashboard') ?>" class="brand-link ml-3">
			<span class="brand-text font-weight-light">Sistem Tenaga<br> Kerja Konstruksi</span>
		</a>

		<div class="sidebar">
			<div class="user-panel mt-3 pb-3 mb-3 d-flex">
				<div class="image">
					<img src="<?php echo base_url() ?>assets/images/<?php echo $icon ?>" class="img-circle elevation-2" alt="User Image">
				</div>
				<div class="info text-wrap">
					<a href="#" class="d-block"><?php echo $usernama ?></a>
				</div>
			</div>

			<nav class="mt-2">
				<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
					<li class="nav-item has-treeview">
						<a href="<?php echo site_url('dashboard') ?>" class="nav-link">
							<i class="nav-icon fas fa-tachometer-alt"></i>
							<p>Dashboard</p>
						</a>
					</li>
					
					<?php echo $menu_show ?>
				</ul>
			</nav>
		</div>
	</aside>
