<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Uri
if(!function_exists('segment')) {
	function segment($index) {
		$CI = &get_instance();
        if($CI->uri->segment($index)) {
		  return strtolower($CI->uri->segment($index));
        } else {
            return false;
        }
	}
}

// Config
if(!function_exists('item')) {
	function item($item) {
		$CI = &get_instance();
        if($CI->config->item($item)) {
		  return strtolower($CI->config->item($item));
        } else {
            return false;
        }
	}
}

// Input
if(!function_exists('post')) {
	function post($input, $check = TRUE) {
		$CI = &get_instance();
        if($check){
		  return $CI->input->post($input);
        } else {
            return $CI->input->post($input);
        }
	}
}

// Router
if(!function_exists('fetch_class')) {
	function fetch_class() {
		$CI = &get_instance();
        if($CI->router->fetch_class()) {
		  return strtolower($CI->router->fetch_class());
        } else {
            return false;
        }
	}
}

if(!function_exists('fetch_method')) {
	function fetch_method() {
		$CI = &get_instance();
        if($CI->router->fetch_method()) {
		  return strtolower($CI->router->fetch_method());
        } else {
            return false;
        }
	}
}

// Session
if(!function_exists('set_userdata')) {
	function set_userdata($session) {
		$CI = &get_instance();
        if($CI->session->set_userdata($session)) {
		  return $CI->session->set_userdata($session);
        } else {
            return false;
        }
	}
}

if(!function_exists('unset_userdata')) {
	function unset_userdata($session) {
		$CI = &get_instance();
        if($CI->session->unset_userdata($session)) {
		  return $CI->session->unset_userdata($session);
        } else {
            return false;
        }
	}
}

if(!function_exists('userdata')) {
	function userdata($session) {
		$CI = &get_instance();
        if($CI->session->userdata($session)) {
		  return $CI->session->userdata($session);
        } else {
            return false;
        }
	}
}