<?php
class BimtekModel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('datatables');
		$this->load->model("AppModel");
	}

	public function GetDataBimtek() {
		$this->db->select("tb.id_bimtek, tb.id_bimtek_url, tb.bimtek_nama, tb.tgl_mulai_bimtek, 
						 tb.tgl_selesai_bimtek, tb.deskripsi, 
						 ttb.jabatan_nama AS kompetensi, tb.status_bimtek AS status");
		$this->db->join("tbl_tenaker_jabatan ttb", "tb.id_tenaker_jabatan = ttb.id_tenaker_jabatan");
		$this->db->where("tb.status", 1);
		$sql = $this->db->get("tbl_bimtek tb");
		return $sql->result();
	}

	public function GetDataBimtekProcess() {
		$this->db->select("tb.id_bimtek, tb.id_bimtek_url, tb.bimtek_nama, tb.tgl_mulai_bimtek, 
						 tb.tgl_selesai_bimtek, tb.deskripsi, 
						 ttb.jabatan_nama AS kompetensi, tb.status_bimtek AS status");
		$this->db->join("tbl_tenaker_jabatan ttb", "tb.id_tenaker_jabatan = ttb.id_tenaker_jabatan");
		$this->db->where("tb.status", 1);
		$this->db->where("tb.status_bimtek !=", 2);
		$sql = $this->db->get("tbl_bimtek tb");
		return $sql->result();
	}

	public function GetDataBimtekDone() {
		$this->db->select("tb.id_bimtek, tb.id_bimtek_url, tb.bimtek_nama, tb.tgl_mulai_bimtek, 
						 tb.tgl_selesai_bimtek, tb.deskripsi, 
						 ttb.jabatan_nama AS kompetensi, tb.status_bimtek AS status");
		$this->db->join("tbl_tenaker_jabatan ttb", "tb.id_tenaker_jabatan = ttb.id_tenaker_jabatan");
		$this->db->where("tb.status", 1);
		$this->db->where("tb.status_bimtek", 2);
		$sql = $this->db->get("tbl_bimtek tb");
		return $sql->result();
	}

	public function GetDetailBimtek($id) {
		$select = "tb.*, tb.tgl_mulai_bimtek, tb.tgl_selesai_bimtek, ttb.jabatan_nama AS kompetensi";
		$from = "tbl_bimtek tb";
		$join = [
			[
				"table" => "tbl_tenaker_jabatan ttb",
				"match" => "tb.id_tenaker_jabatan = ttb.id_tenaker_jabatan",
				"type" => ""
			]
		];
		$where = [
			["column" => "tb.status", "value" => 1],
			["column" => "tb.id_bimtek_url", "value" => $id],
		];

		return $this->AppModel->GetDataRow($select, $from, $join, $where);
	}

	public function GetDataPesertaBimtek($id) {
		$select = "tbp.id_bimtek_peserta_url, tmkk.tenaker_nik, tmkk.tenaker_nama, tmkk.tenaker_jkel, 
				   tbp.status_peserta, tbp.status_lulus, tbp.status_kompeten";
		$from = "tbl_bimtek_peserta tbp";
		$join = [
			[
				"table" => "tbl_master_tenaga_kerja tmkk",
				"match" => "tmkk.id_tenaga_kerja = tbp.id_tenaga_kerja",
				"type" => ""
			],
			[
				"table" => "tbl_bimtek tb",
				"match" => "tbp.id_bimtek = tb.id_bimtek",
				"type" => ""
			]
		];
		$where = [
			["column" => "tbp.status", "value" => 1],
			["column" => "tb.id_bimtek_url", "value" => $id],
		];

		return $this->AppModel->GetDataResult($select, $from, $join, $where, NULL, NULL, NULL, NULL);
	}

	public function GetTotalPesertaLulus($id) {
		$select = "tbp.id_bimtek_peserta";
		$from = "tbl_bimtek_peserta tbp";
		$join = [
			[
				"table" => "tbl_bimtek tb",
				"match" => "tbp.id_bimtek = tb.id_bimtek",
				"type" => ""
			]
		];
		$where = [
			["column" => "tbp.status", "value" => 1],
			["column" => "tbp.status_lulus", "value" => 1],
			["column" => "tb.id_bimtek_url", "value" => $id]
		];

		$res = $this->AppModel->GetDataResult($select, $from, $join, $where, NULL, NULL, NULL, NULL);
		return count($res);
	}

	public function GetPesertaKompeten($id) {
		$select = "tbp.id_bimtek_peserta, tbp.id_tenaga_kerja, tb.id_tenaker_jabatan";
		$from = "tbl_bimtek_peserta tbp";
		$join = [
			[
				"table" => "tbl_bimtek tb",
				"match" => "tbp.id_bimtek = tb.id_bimtek",
				"type" => ""
			]
		];
		$where = [
			["column" => "tbp.status_peserta", "value" => 1],
			["column" => "tbp.status_lulus", "value" => 1],
			["column" => "tbp.status_kompeten", "value" => 1],
			["column" => "tb.id_bimtek_url", "value" => $id]
		];

		$res = $this->AppModel->GetDataResult($select, $from, $join, $where, NULL, NULL, NULL, NULL);
		return $res;
	}

	public function GetTotalPesertaKompeten($id) {
		$res = $this->GetPesertaKompeten($id);
		return count($res);
	}

	public function GetKegiatanBimtek($id) {
		$select = "tbk.id_bimtek_kehadiran_url AS uid, tbk.kegiatan, tbk.tgl_kehadiran, COUNT(tbkd.id_bimtek_peserta) AS hadir";
		$from = "tbl_bimtek_kehadiran tbk";
		$join = [
			[
				"table" => "tbl_bimtek_kehadiran_detail tbkd",
				"match" => "tbk.id_bimtek_kehadiran = tbkd.id_bimtek_kehadiran",
				"type" => ""
			],
			[
				"table" => "tbl_bimtek tb",
				"match" => "tbk.id_bimtek = tb.id_bimtek",
				"type" => ""
			]
		];
		$where = [
			["column" => "tbk.status", "value" => 1],
			["column" => "tbkd.status_hadir", "value" => "hadir"],
			["column" => "tb.id_bimtek_url", "value" => $id]
		];

		$group = "tbk.id_bimtek_kehadiran_url";
		$res = $this->AppModel->GetDataResult($select, $from, $join, $where, $group, NULL, NULL, NULL);
		return $res;
	}

	public function GetKegiatanBimtekDetail($id) {
		$select = "tbk.id_bimtek_kehadiran_url AS uid, tbk.kegiatan, tbk.tgl_kehadiran";
		$from = "tbl_bimtek_kehadiran tbk";
		$where = [
			["column" => "tbk.status", "value" => 1],
			["column" => "tbk.id_bimtek_kehadiran_url", "value" => $id]
		];
		$kegiatan = $this->AppModel->GetDataRow($select, $from, NULL, $where);

		$select = "tbp.id_bimtek_peserta AS id, tmtk.tenaker_nama AS nama, tbkd.status_hadir AS status";
		$from = "tbl_bimtek_kehadiran_detail tbkd";
		$join = [
			[
				"table" => "tbl_bimtek_peserta tbp",
				"match" => "tbkd.id_bimtek_peserta = tbp.id_bimtek_peserta",
				"type" => ""
			],
			[
				"table" => "tbl_master_tenaga_kerja tmtk",
				"match" => "tbp.id_tenaga_kerja = tmtk.id_tenaga_kerja",
				"type" => ""
			],
			[
				"table" => "tbl_bimtek_kehadiran tbk",
				"match" => "tbkd.id_bimtek_kehadiran = tbk.id_bimtek_kehadiran",
				"type" => ""
			]
		];
		$where = [
			["column" => "tbk.id_bimtek_kehadiran_url", "value" => $id]
		];
		$kehadiran = $this->AppModel->GetDataResult($select, $from, $join, $where, NULL, NULL, NULL, NULL);

		$output = [
			"kegiatan" => $kegiatan,
			"kehadiran" => $kehadiran
		];

		return $output;
	}

	public function GetNilaiBimtekPeserta($id) {
		$this->db->select("tbn.id_bimtek_nilai AS id_nilai, tmtk.tenaker_nama AS peserta, 
						   tbn.nilai_hadir AS kehadiran, tbn.nilai_pretest AS pretest, 
						   tbn.nilai_posttest AS posttest, tbn.nilai_mandiri AS mandiri");
		$this->db->join("tbl_bimtek_peserta tbp", "tbn.id_bimtek_peserta = tbp.id_bimtek_peserta");
		$this->db->join("tbl_bimtek tb", "tbn.id_bimtek = tb.id_bimtek");
		$this->db->join("tbl_master_tenaga_kerja tmtk", "tbp.id_tenaga_kerja = tmtk.id_tenaga_kerja");
		$this->db->where("tb.id_bimtek_url", $id);
		$sql = $this->db->get("tbl_bimtek_nilai tbn");
		
		return $sql;
	}

	public function GetBimtekProcessViewOnTenaker($id) {
		$bimtek = $this->GetDataBimtekProcess();
		$id = $this->AppModel->GetIDFromUID($id, "id_tenaga_kerja", "tbl_master_tenaga_kerja");
		foreach($bimtek as $row) {
			$this->db->select("IFNULL(tbp.status_peserta, NULL) AS status_peserta");
			$this->db->join("tbl_bimtek_peserta tbp", "tb.id_bimtek = tbp.id_bimtek");
			$this->db->where("tbp.id_tenaga_kerja", $id);
			$this->db->where("tb.id_bimtek", $row->id_bimtek);
			$res = $this->db->get("tbl_bimtek tb");
			
			if($res->num_rows() == 0) {
				$row->status_peserta = NULL;
			} else {
				$row->status_peserta = $res->row()->status_peserta;
			}

			$data[] = $row;
		}

		return $data;
	}

	public function GetBimtekDoneViewOnTenaker($id) {
		$bimtek = $this->GetDataBimtekDone();
		$id = $this->AppModel->GetIDFromUID($id, "id_tenaga_kerja", "tbl_master_tenaga_kerja");
		foreach($bimtek as $row) {
			$this->db->select("tbp.status_lulus AS lulus, tbp.status_kompeten AS kompeten");
			$this->db->join("tbl_bimtek_peserta tbp", "tb.id_bimtek = tbp.id_bimtek");
			$this->db->where("tbp.id_tenaga_kerja", $id);
			$this->db->where("tb.id_bimtek", $row->id_bimtek);
			$res = $this->db->get("tbl_bimtek tb");
			
			if($res->num_rows() == 0) {
				$row->lulus = NULL;
				$row->kompeten = NULL;
			} else {
				$row->lulus = $res->row()->lulus;
				$row->kompeten = $res->row()->kompeten;
			}

			$data[] = $row;
		}

		return $data;
	}

	public function DaftarBimtekPeserta($data) {
		$sql = $this->db->insert("tbl_bimtek_peserta", $data);
		return $sql;
	}

	public function AturNilaiBimtekPeserta($data) {
		$sql = $this->db->insert_batch("tbl_bimtek_nilai", $data);
		return $sql;
	}

	public function EditNilaiBimtekPeserta($data, $id) {
		$sql = $this->db->update("tbl_bimtek_nilai", $data, ["id_bimtek_nilai" => $id]);
		return $sql;
	}

	public function EditBimtek($data, $id) {
		$this->db->update("tbl_bimtek", $data, ["id_bimtek_url" => $id]);
		if($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function HapusBimtek($id) {
		$this->db->delete("tbl_bimtek", ["id_bimtek_url" => $id]);
		if($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function HapusBimtekKegiatan($id) {
		$id = $this->AppModel->GetIDFromUID($id, "id_bimtek_kehadiran", "tbl_bimtek_kehadiran");

		$this->db->delete("tbl_bimtek_kehadiran_detail", ["id_bimtek_kehadiran" => $id]);
		$this->db->delete("tbl_bimtek_kehadiran", ["id_bimtek_kehadiran" => $id]);
		if($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function HitungAbsensi($id) {
		// Hitung Total Pertemuan
		$this->db->select("COUNT(tbk.id_bimtek_kehadiran) AS total_pertemuan");
		$this->db->join("tbl_bimtek tb", "tbk.id_bimtek = tb.id_bimtek");
		$this->db->where("tb.id_bimtek_url", $id);
		$sql_bimtek = $this->db->get("tbl_bimtek_kehadiran tbk")->row();
		$output['pertemuan'] = $sql_bimtek->total_pertemuan;

		// if($sql_bimtek->total_pertemuan == 0) {
		// 	// Jika Pertemuan = 0, Maka Nilai Kehadiran Semuanya 0
		// 	$this->db->update("tbl_bimtek_nilai", ["nilai_hadir" => 0]);
		// } else {
			// Get Data Peserta
			$this->db->select("tbn.id_bimtek_peserta");
			$this->db->join("tbl_bimtek tb", "tbn.id_bimtek = tb.id_bimtek");
			$this->db->where("tb.id_bimtek_url", $id);
			$sql_peserta = $this->db->get("tbl_bimtek_nilai tbn");

			foreach($sql_peserta->result() as $row) {
				// Hitung Total Kehadiran Per Peserta
				$this->db->select("tbkd.*");
				$this->db->join("tbl_bimtek_kehadiran tbk", "tbkd.id_bimtek_kehadiran = tbk.id_bimtek_kehadiran");
				$this->db->join("tbl_bimtek tb", "tbk.id_bimtek = tb.id_bimtek");
				$this->db->where("tbkd.id_bimtek_peserta", $row->id_bimtek_peserta);
				$this->db->where("tb.id_bimtek_url", $id);
				$this->db->where("tbkd.status_hadir", "hadir");
				$sql_hadir = $this->db->get("tbl_bimtek_kehadiran_detail tbkd");
				$hadir = $sql_hadir->num_rows();

				// Perhitungan dan Update Data
				$nilai_kehadiran = ceil(($hadir / $sql_bimtek->total_pertemuan) * 100);
				$this->db->update("tbl_bimtek_nilai", ["nilai_hadir" => $nilai_kehadiran], ["id_bimtek_peserta" => $row->id_bimtek_peserta]);
			}
		//}
	}

	public function EditStatusPeserta($data, $id) {
		$sql = $this->db->update("tbl_bimtek_peserta", $data, ["id_bimtek_peserta_url" => $id]);
		return $sql;
	}

	public function TambahDataNilaiPeserta($data) {
		$sql = $this->db->insert("tbl_bimtek_nilai", $data);
		return $sql;
	}

	public function HapusNilaiPeserta($data) {
		$sql = $this->db->delete("tbl_bimtek_nilai", $data);
		return $sql;		
	}
}
