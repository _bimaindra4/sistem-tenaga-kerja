<?php
class TenakerModel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('datatables');
	}
	
	public function GetUtusanTenaker($uid="") {
		$this->db->select("id_tenaker_utusan, utusan_tenaker");
		($uid == "" ? "" : $this->db->where("id_tenaker_utusan_url", $uid));
		$sql = $this->db->get("tbl_tenaker_utusan");
		
		if($sql->num_rows() == 0) {
			return [];
		} else {
			if($uid == "") {
				return $sql->result();
			} else {
				return $sql->row();
			}
		}
	}

	public function GetJabatanTenaker($uid="") {
		$this->db->select("id_tenaker_jabatan, jabatan_nama");
		($uid == "" ? "" : $this->db->where("id_tenaker_jabatan_url", $uid));
		$sql = $this->db->get("tbl_tenaker_jabatan");
		
		if($sql->num_rows() == 0) {
			return [];
		} else {
			if($uid == "") {
				return $sql->result();
			} else {
				return $sql->row();
			}
		}
	}

	public function GetSertifikatTenaker($uid) {
		$this->db->select("tts.id_tenaker_sertifikat_url, ttj.jabatan_nama,
						   tts.sertifikat_jenis, tts.sertifikat_nomor,
						   tts.sertifikat_tanggal");
		$this->db->join("tbl_tenaker_jabatan ttj", "tts.id_tenaker_jabatan = ttj.id_tenaker_jabatan");
		$this->db->join("tbl_master_tenaga_kerja tmtk", "tts.id_tenaga_kerja = tmtk.id_tenaga_kerja");
		$this->db->where("tmtk.id_tenaga_kerja_url", $uid);
		$sql = $this->db->get("tbl_tenaker_sertifikat tts");

		if($sql->num_rows() == 0) {
			return [];
		} else {
			return $sql->result();
		}
	}

	public function GetDetailUtusanTenaker($id) {
		$this->db->select("ttu.utusan_tenaker, ttud.jabatan, ttud.tgl_mulai, ttud.tgl_akhir");
		$this->db->join("tbl_tenaker_utusan ttu", "ttud.id_tenaker_utusan = ttu.id_tenaker_utusan");
		$this->db->join("tbl_master_tenaga_kerja tmtk", "ttud.id_tenaga_kerja = tmtk.id_tenaga_kerja");
		$this->db->where("tmtk.id_tenaga_kerja_url", $id);
		$sql = $this->db->get("tbl_tenaker_utusan_detail ttud");

		return $sql->result();
	}

	public function GetDetailSertifikatTenaker($id) {
		$this->db->select("ttj.jabatan_nama, tts.sertifikat_jenis, tts.sertifikat_tanggal, tts.status");
		$this->db->join("tbl_tenaker_jabatan ttj", "tts.id_tenaker_jabatan = ttj.id_tenaker_jabatan");
		$this->db->join("tbl_master_tenaga_kerja tmtk", "tts.id_tenaga_kerja = tmtk.id_tenaga_kerja");
		$this->db->where("tmtk.id_tenaga_kerja_url", $id);
		$sql = $this->db->get("tbl_tenaker_sertifikat tts");

		return $sql->result();
	}

	public function GetBimtekTenaker($id) {
		$this->db->select("bimtek.bimtek_nama AS nama, bimtek.deskripsi,
						   bimtek.tgl_mulai_bimtek AS mulai, bimtek.tgl_selesai_bimtek AS selesai,
						   bimtek.status_bimtek, peserta.status_lulus AS lulus, 
						   peserta.status_kompeten AS kompeten");
		$this->db->join("tbl_bimtek bimtek", "peserta.id_bimtek = bimtek.id_bimtek");
		$this->db->join("tbl_master_tenaga_kerja tenaker", "peserta.id_tenaga_kerja = tenaker.id_tenaga_kerja");
		$this->db->where("tenaker.id_tenaga_kerja_url", $id);
		$this->db->order_by("bimtek.tgl_mulai_bimtek", "DESC");
		$sql = $this->db->get("tbl_bimtek_peserta peserta");

		return $sql->result();
	}
}
