<?php
class SertifikatModel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->database();
    }

    function GetSKATAktif() {
		$this->db->select("tenaker.tenaker_nama AS nama, jabatan.jabatan_nama AS topik,
						   sertif.sertifikat_tanggal AS tanggal, DATEDIFF(sertif.sertifikat_tanggal,NOW()) AS masa_berlaku");
		$this->db->join("tbl_master_tenaga_kerja tenaker", "sertif.id_tenaga_kerja = tenaker.id_tenaga_kerja");
		$this->db->join("tbl_tenaker_jabatan jabatan", "sertif.id_tenaker_jabatan = jabatan.id_tenaker_jabatan");
		$this->db->having("masa_berlaku > 30");
		$sql = $this->db->get("tbl_tenaker_sertifikat sertif");

		return $sql;
	}

	function GetSKATHampirKadaluarsa() {
		$this->db->select("tenaker.tenaker_nama AS nama, jabatan.jabatan_nama AS topik,
						   sertif.sertifikat_tanggal AS tanggal, DATEDIFF(sertif.sertifikat_tanggal,NOW()) AS masa_berlaku");
		$this->db->join("tbl_master_tenaga_kerja tenaker", "sertif.id_tenaga_kerja = tenaker.id_tenaga_kerja");
		$this->db->join("tbl_tenaker_jabatan jabatan", "sertif.id_tenaker_jabatan = jabatan.id_tenaker_jabatan");
		$this->db->having("masa_berlaku < 30");
		$this->db->having("masa_berlaku > 0");
		$sql = $this->db->get("tbl_tenaker_sertifikat sertif");

		return $sql;
	}

	function GetSKATKadaluarsa() {
		$this->db->select("tenaker.tenaker_nama AS nama, jabatan.jabatan_nama AS topik,
						   sertif.sertifikat_tanggal AS tanggal, DATEDIFF(sertif.sertifikat_tanggal,NOW()) AS masa_berlaku");
		$this->db->join("tbl_master_tenaga_kerja tenaker", "sertif.id_tenaga_kerja = tenaker.id_tenaga_kerja");
		$this->db->join("tbl_tenaker_jabatan jabatan", "sertif.id_tenaker_jabatan = jabatan.id_tenaker_jabatan");
		$this->db->having("masa_berlaku < 0");
		$sql = $this->db->get("tbl_tenaker_sertifikat sertif");

		return $sql;
	}

	function GetSertifikatTenaker($id) {
		$this->db->select("jabatan.jabatan_nama AS jabatan, sertif.sertifikat_tanggal, sertif.sertifikat_jenis,
						   sertifikat_nomor, DATEDIFF(sertif.sertifikat_tanggal,NOW()) AS masa_berlaku");
		$this->db->join("tbl_tenaker_jabatan jabatan", "sertif.id_tenaker_jabatan = jabatan.id_tenaker_jabatan");
		$this->db->join("tbl_master_tenaga_kerja tenaker", "sertif.id_tenaga_kerja = tenaker.id_tenaga_kerja");
		$this->db->where("tenaker.id_tenaga_kerja_url", $id);
		$sql = $this->db->get("tbl_tenaker_sertifikat sertif");

		return $sql;
	}
}