<?php
class MenuModel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	function GetMenuGroup($id,$mainrole) {
        $this->db->select("tsrmg.id_role_menu_group AS id, tsrmg.menu_group_nama AS grup_menu");
        $this->db->join("tbl_sys_role_menu_privileges tsrmp", "tsrd.id_role = tsrmp.id_role");
        $this->db->join("tbl_sys_role_menu_group tsrmg", "tsrmp.id_role_menu_group = tsrmg.id_role_menu_group");
        $this->db->where("tsrd.id_user", $id);
        $this->db->where("tsrd.user_table",$mainrole);
        $this->db->order_by("tsrmg.sorting", "ASC");
        $sql = $this->db->get("tbl_sys_role_detail tsrd");

        if($sql->num_rows() == 0) {
            return [];
        } else {
            return $sql->result();
        }
    }

    function GetItemMenu($id, $childID = 0) {
        $this->db->select("id_role_menu AS id, menu_nama AS menu, menu_icon AS icon, menu_link AS link");
        $this->db->where("id_role_menu_group", $id);
        $this->db->where("id_role_menu_parent", $childID);
        $this->db->order_by("sorting", "ASC");
        $sql = $this->db->get("tbl_sys_role_menu");

        if($sql->num_rows() == 0) {
            return [];
        } else {
            foreach($sql->result() as $row) {
                $menu["id"] = $row->id;
                $menu["nama"] = $row->menu;
                $menu["icon"] = $row->icon;
                $menu["link"] = $row->link;
                $menu["child"] = $this->GetItemMenu($id,$row->id);

                $output[] = $menu;
            }

            return $output;
        }
    }
}