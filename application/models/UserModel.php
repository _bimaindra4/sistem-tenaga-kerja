<?php
class UserModel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('datatables');
	}
	
	public function GetDataIAPPI($type,$uid="") {
		if($type == "table") {
			$this->datatables->select("id_iappi_url, iappi_nama, iappi_jkel, iappi_tempat_lahir, iappi_tanggal_lahir, iappi_notelp, iappi_email");
			$this->datatables->from("tbl_master_iappi");
			$this->datatables->add_column('action', 
				'<a href="javascript:void(0);" class="edit_iappi btn btn-info btn-xs" data-uid="$1"">Edit</a>  
				<a href="javascript:void(0);" class="hapus_iappi btn btn-danger btn-xs" data-uid="$1" data-nama="$2">Hapus</a>',
				'id_iappi_url, iappi_nama');

			return $this->datatables->generate();
		} else if($type == "data") {
			($uid != "" ? $this->db->where("id_iappi_url", $uid) : "");
			$sql = $this->db->get("tbl_master_iappi");
			
			if($uid == "") {
				return ($sql->num_rows() == 0 ? [] : $sql->result());
			} else {
				return ($sql->num_rows() == 0 ? [] : $sql->row());
			}
		}
	}

	public function GetDataAP3I($type,$uid="") {
		if($type == "table") {
			$this->datatables->select("id_ap3i_url, ap3i_nama, ap3i_username, created_at");
			$this->datatables->from("tbl_master_ap3i");
			$this->datatables->add_column('action',
				'<a href="javascript:void(0);" class="edit_ap3i btn btn-info btn-xs" data-uid="$1"">Edit</a>  
				<a href="javascript:void(0);" class="hapus_ap3i btn btn-danger btn-xs" data-uid="$1" data-nama="$2">Hapus</a>',
				'id_ap3i_url, ap3i_nama');

			return $this->datatables->generate();
		} else if($type == "data") {
			($uid != "" ? $this->db->where("id_ap3i_url", $uid) : "");
			$sql = $this->db->get("tbl_master_ap3i");

			return ($sql->num_rows() == 0 ? [] : $sql->row());
		}
	}

	public function GetDataBalaiJasa($type,$uid="") {
		if($type == "table") {
			$this->datatables->select("id_balai_jasa_url, baljas_nama, baljas_username, created_at");
			$this->datatables->from("tbl_master_balai_jasa");
			$this->datatables->add_column('action',
				'<a href="javascript:void(0);" class="edit_baljas btn btn-info btn-xs" data-uid="$1"">Edit</a>  
				<a href="javascript:void(0);" class="hapus_baljas btn btn-danger btn-xs" data-uid="$1" data-nama="$2">Hapus</a>',
				'id_balai_jasa_url, baljas_nama');

			return $this->datatables->generate();
		} else if($type == "data") {
			($uid != "" ? $this->db->where("id_balai_jasa_url", $uid) : "");
			$sql = $this->db->get("tbl_master_balai_jasa");

			return ($sql->num_rows() == 0 ? [] : $sql->row());
		}
	}

	public function GetDataTenagaKerja($type,$uid="") {
		if($type == "table") {
			$this->datatables->select("id_tenaga_kerja_url, tenaker_nama, 
									   tenaker_nik, tenaker_tempat_lahir, 
									   tenaker_tanggal_lahir, tenaker_jkel, 
									   tenaker_kewarganegaraan");
			$this->datatables->from("tbl_master_tenaga_kerja");
			$this->datatables->add_column('action',
				'<a href="javascript:void(0);" class="lihat_tenaker btn btn-warning btn-xs" data-uid="$1">Lihat</a>',
				'id_tenaga_kerja_url');

			return $this->datatables->generate();
		} else if($type == "data") {
			$this->db->select("tmtk.*, ttj.jabatan_nama, ttu.utusan_tenaker, COUNT(tts.id_tenaker_sertifikat) AS skat, 
							   tsp.provinsi_nama, tsk.kabkot_nama");
			($uid != "" ? $this->db->where("tmtk.id_tenaga_kerja_url", $uid) : "");
			$this->db->join("tbl_tenaker_jabatan ttj", "ttj.id_tenaker_jabatan = tmtk.id_tenaker_jabatan");
			$this->db->join("tbl_tenaker_utusan ttu", "ttu.id_tenaker_utusan = tmtk.id_tenaker_utusan");
			$this->db->join("tbl_tenaker_sertifikat tts", "tmtk.id_tenaga_kerja = tts.id_tenaga_kerja", "left");
			$this->db->join("tbl_sys_provinsi tsp", "tmtk.tenaker_provinsi = tsp.id_provinsi");
			$this->db->join("tbl_sys_kabkot tsk", "tmtk.tenaker_kabkot = tsk.id_kabkot");
			$this->db->group_by("tmtk.id_tenaga_kerja");
			$sql = $this->db->get("tbl_master_tenaga_kerja tmtk");

			if($uid == "") {
				return ($sql->num_rows() == 0 ? [] : $sql->result());
			} else {
				return ($sql->num_rows() == 0 ? [] : $sql->row());
			}
		}
	}

	public function TambahDataIAPPI($data) {
		$sql = $this->db->insert("tbl_master_iappi", $data);
		return $sql;
	}
	
	public function TambahDataAP3I($data) {
		$sql = $this->db->insert("tbl_master_ap3i", $data);
		return $sql;
	}

	public function TambahDataBalaiJasa($data) {
		$sql = $this->db->insert("tbl_master_balai_jasa", $data);
		return $sql;
	}

	public function TambahDataTenagaKerja($data) {
		$this->db->insert("tbl_master_tenaga_kerja", $data);
		$sql = $this->db->insert_id();
		return $sql;
	}

	public function EditDataIAPPI($data,$uid) {
		$sql = $this->db->update("tbl_master_iappi", $data, ["id_iappi_url" => $uid]);
		return $sql;
	}

	public function EditDataAP3I($data,$uid) {
		$sql = $this->db->update("tbl_master_ap3i", $data, ["id_ap3i_url" => $uid]);
		return $sql;
	}

	public function EditDataBalaiJasa($data,$uid) {
		$sql = $this->db->update("tbl_master_balai_jasa", $data, ["id_balai_jasa_url" => $uid]);
		return $sql;
	}

	public function EditDataTenagaKerja($data,$uid) {
		$sql = $this->db->update("tbl_master_tenaga_kerja", $data, ["id_tenaga_kerja_url" => $uid]);
		return $sql;
	}

	public function HapusDataIAPPI($uid) {
		$sql = $this->db->delete("tbl_master_iappi", ["id_iappi_url" => $uid]);
		return $sql;
	}

	public function HapusDataAP3I($uid) {
		$sql = $this->db->delete("tbl_master_ap3i", ["id_ap3i_url" => $uid]);
		return $sql;
	}

	public function HapusDataBalaiJasa($uid) {
		$sql = $this->db->delete("tbl_master_balai_jasa", ["id_balai_jasa_url" => $uid]);
		return $sql;
	}

	public function HapusDataTenagaKerja($uid) {
		$sql = $this->db->delete("tbl_master_tenaga_kerja", ["id_tenaga_kerja_url" => $uid]);
		return $sql;
	}
}
