<?php
class LoginModel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->database();
    }

    function Verification($user,$pass) {
        // Super Admin Query
        $this->db->select("tsu.id_sadmin_url AS id, tsu.sadmin_username AS username, tsu.sadmin_password AS password, tsu.sadmin_nama AS nama, GROUP_CONCAT(tsr.id_role) AS role");
        $this->db->from("tbl_super_user tsu");
        $this->db->join("tbl_sys_role_detail tsrd", "tsrd.id_user = tsu.id_sadmin AND tsrd.id_role=6");
        $this->db->join("tbl_sys_role tsr", "tsr.id_role = tsrd.id_role");
        $this->db->where("tsu.sadmin_username", $user);
        $this->db->where("tsu.sadmin_password", $pass);
        $tsu = $this->db->get_compiled_select();

        // IAPPI Query
        $this->db->select("tmi.id_iappi_url AS id, tmi.iappi_username AS username, tmi.iappi_password AS password, tmi.iappi_nama AS nama, GROUP_CONCAT(tsr.id_role) AS role");
        $this->db->from("tbl_master_iappi tmi");
        $this->db->join("tbl_sys_role_detail tsrd", "tsrd.id_user = tmi.id_iappi AND tsrd.id_role=3");
        $this->db->join("tbl_sys_role tsr", "tsr.id_role = tsrd.id_role");
        $this->db->where("tmi.iappi_username", $user);
        $this->db->where("tmi.iappi_password", $pass);
        $tmi = $this->db->get_compiled_select();

        // AP3I Query
        $this->db->select("tma.id_ap3i_url AS id, tma.ap3i_username AS username, tma.ap3i_password AS password, tma.ap3i_nama AS nama, GROUP_CONCAT(tsr.id_role) AS role");
        $this->db->from("tbl_master_ap3i tma");
        $this->db->join("tbl_sys_role_detail tsrd", "tsrd.id_user = tma.id_ap3i AND tsrd.id_role=2");
        $this->db->join("tbl_sys_role tsr", "tsr.id_role = tsrd.id_role");
        $this->db->where("tma.ap3i_username", $user);
        $this->db->where("tma.ap3i_password", $pass);
        $tma = $this->db->get_compiled_select();

        // Balai Jasa Query
        $this->db->select("tmbj.id_balai_jasa_url AS id, tmbj.baljas_username AS username, tmbj.baljas_password AS password, tmbj.baljas_nama AS nama, GROUP_CONCAT(tsr.id_role) AS role");
        $this->db->from("tbl_master_balai_jasa tmbj");
        $this->db->join("tbl_sys_role_detail tsrd", "tsrd.id_user = tmbj.id_balai_jasa AND tsrd.id_role=4");
        $this->db->join("tbl_sys_role tsr", "tsr.id_role = tsrd.id_role");
        $this->db->where("tmbj.baljas_username", $user);
        $this->db->where("tmbj.baljas_password", $pass);
        $tmbj = $this->db->get_compiled_select();

        // Tenaga Kerja Query
        $this->db->select("tmtk.id_tenaga_kerja_url AS id, tmtk.tenaker_username AS username, tmtk.tenaker_password AS password, tmtk.tenaker_nama AS nama, GROUP_CONCAT(tsr.id_role) AS role");
        $this->db->from("tbl_master_tenaga_kerja tmtk");
        $this->db->join("tbl_sys_role_detail tsrd", "tsrd.id_user = tmtk.id_tenaga_kerja AND tsrd.id_role=5");
        $this->db->join("tbl_sys_role tsr", "tsr.id_role = tsrd.id_role");
        $this->db->where("tmtk.tenaker_username", $user);
        $this->db->where("tmtk.tenaker_password", $pass);
        $tmtk = $this->db->get_compiled_select();

        // Kemenpupr Query
        $this->db->select("tmp.id_kemenpupr_url AS id, tmp.pupr_username AS username, tmp.pupr_password AS password, tmp.pupr_nama AS nama, GROUP_CONCAT(tsr.id_role) AS role");
        $this->db->from("tbl_master_pupr tmp");
        $this->db->join("tbl_sys_role_detail tsrd", "tsrd.id_user = tmp.id_kemenpupr AND tsrd.id_role=1");
        $this->db->join("tbl_sys_role tsr", "tsr.id_role = tsrd.id_role");
        $this->db->where("tmp.pupr_username", $user);
        $this->db->where("tmp.pupr_password", $pass);
        $tmp = $this->db->get_compiled_select();

        $sql = $this->db->query($tsu.' UNION '.$tmi.' UNION '.$tma.' UNION '.$tmbj.' UNION '.$tmtk.' UNION '.$tmp);
        if($sql->num_rows() == 0) {
            return [];
        } else {
            foreach($sql->result() as $row) {
                if($row->id != NULL) {
                    $result = $row;
                    $user_db = $result->username;
                    $pass_db = $result->password;

                    break;
                } else {
                    $user_db = "";
                    $pass_db = "";
                }
            }

            if($user_db == $user && $pass_db == $pass) {
                return $result;
            } else {
                return [];
            }
        }
    }
}