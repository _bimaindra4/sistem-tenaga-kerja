<?php
class UtusanModel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->library('datatables');
		$this->load->model("AppModel");
    }

    public function GetDetailUtusan($id) {
        $this->db->select("id_tenaker_utusan AS id, utusan_tenaker, utusan_alamat");
        $this->db->where("id_tenaker_utusan", $id);
        $sql = $this->db->get("tbl_tenaker_utusan");

        return $sql->row();
    }

    public function GetTenakerUtusan($id) {
        $this->db->select("tenaker.tenaker_nama AS nama, ud.jabatan, ud.tgl_mulai, ud.tgl_akhir");
        $this->db->join("tbl_master_tenaga_kerja tenaker", "ud.id_tenaga_kerja = tenaker.id_tenaga_kerja");
        $this->db->where("ud.id_tenaker_utusan", $id);
        $sql = $this->db->get("tbl_tenaker_utusan_detail ud");

        return $sql->result();
    }
}