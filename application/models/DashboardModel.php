<?php
class DashboardModel extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	function GetTotalUser($name) {
		$this->db->select("*");
		$this->db->where("status", 1);

		if($name == "iappi") {
			$sql = $this->db->get("tbl_master_iappi");
			return $sql->num_rows();
		} else if($name == "ap3i") {
			$sql = $this->db->get("tbl_master_ap3i");
			return $sql->num_rows();
		} else if($name == "balaijasa") {
			$sql = $this->db->get("tbl_master_balai_jasa");
			return $sql->num_rows();
		} else if($name == "tenaker") {
			$sql = $this->db->get("tbl_master_tenaga_kerja");
			return $sql->num_rows();
		} else {
			return 0;
		}
	}

	function SebaranTenaker() {
		$this->db->select("ttu.id_tenaker_utusan AS id, ttu.utusan_tenaker, ttu.utusan_alamat, 
						   ttu.utusan_lat AS lat, ttu.utusan_long AS lng");
		$this->db->join("tbl_tenaker_utusan ttu", "tmtt.id_tenaker_utusan = ttu.id_tenaker_utusan");
		$sql = $this->db->get("tbl_master_tenaga_kerja tmtt");
		return $sql->result();
	}
}
