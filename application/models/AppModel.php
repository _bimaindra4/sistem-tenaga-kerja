<?php
class AppModel extends CI_Model {
	public function __construct() {
		parent::__construct();
        $this->load->database();
        $this->load->model("MenuModel");
    }

    // Date
	function DateTimeNow() {
		date_default_timezone_set("Asia/Jakarta");
		return date("Y-m-d H:i:s");
	}

	function DateNow() {
		date_default_timezone_set("Asia/Jakarta");
		return date("Y-m-d");
	}

	function YearNow() {
		date_default_timezone_set("Asia/Jakarta");
		return date("Y");
	}

	function MonthNow() {
		date_default_timezone_set("Asia/Jakarta");
		return date("m");
	}

	function DayNow() {
		date_default_timezone_set("Asia/Jakarta");
		return date("d");
	}

	function BulanIndo($val="") {
		if($val == "") {
			$month = [
				'-- Bulan --' => 0,
				'Januari' => 1,
				'Februari' => 2,
				'Maret' => 3,
				'April' => 4,
				'Mei' => 5,
				'Juni' => 6,
				'Juli' => 7,
				'Agustus' => 8,
				'September' => 9,
				'Oktober' => 10,
				"November" => 11,
				"Desember" => 12
			];

			return $month;
		} else {
			$month = [
				0 => '-- Bulan --',
				1 => 'Januari',
				2 => 'Februari',
				3 => 'Maret',
				4 => 'April',
				5 => 'Mei',
				6 => 'Juni',
				7 => 'Juli',
				8 => 'Agustus',
				9 => 'September',
				10 => 'Oktober',
				11 => 'November',
				12 => 'Desember'
			];

			return $month[$val];
		}
	}

	function HariIndo() {
		$hari = [
			'-- Hari --' => 0,
			'Senin' => 'senin',
			'Selasa' => 'selasa',
			'Rabu' => 'rabu',
			'Kamis' => 'kamis',
			'Jumat' => 'jumat',
			'Sabtu' => 'sabtu',
			'Minggu' => 'minggu'
		];

		return $hari;
	}

	function DateIndo($data) {
		if($data == NULL || $data == "") {
			return "";
		} else {
			$x_data = explode("-", $data);
			$bulan = $this->bulanIndo((int) $x_data[1]);
			$output = $x_data[2]." ".$bulan." ".$x_data[0];
			
			return $output;
		}
	}

	function DateTimeIndo($data) {
		$x_data = explode(" ", $data);
		$x2_data = explode(":", $x_data[1]);
		$date = $this->dateIndo($x_data[0]);
		$time = $x2_data[0].":".$x2_data[1];
		$output = $date." - ".$time;

		return $output;
    }

    // CRUD
	function GetDataResult($select, $from, $join, $where, $group, $order, $start, $limit, $array = FALSE) {
		$this->db->select($select);
		$this->db->from($from);

		// Define Join Clause
		if($join != NULL && is_array($join)) {
			foreach($join as $row) {
				$this->db->join($row['table'], $row['match'], $row['type']);
			}
		}

		// Define Where Clause
		if($where != NULL && is_array($where)) {
			foreach($where as $row) {
				$this->db->where($row['column'], $row['value']);
			}
		}

		// Define Group Clause
		($group != NULL ? $this->db->group_by($group) : "");

		// Define Order Clause
		if($order != NULL && is_array($order)) {
			foreach($order as $row) {
				$this->db->order_by($row['column'], $row['sort']);
			}
		}

		// Define Limit Clause
		if((int) $limit != 0 && (int) $start != 0) {
			$this->db->limit($start, $limit);
		}

		$db = $this->db->get();

		// Choosing Array or Object
		return ($array == FALSE ?  $db->result() : $db->result_array());
	}

	function GetDataRow($select, $from, $join, $where) {
		$this->db->select($select);
		$this->db->from($from);

		// Define Join Clause
		if($join != NULL && is_array($join)) {
			foreach($join as $row) {
				$this->db->join($row['table'], $row['match'], $row['type']);
			}
		}

		// Define Where Clause
		if($where != NULL && is_array($where)) {
			foreach($where as $row) {
				$this->db->where($row['column'], $row['value']);
			}
		}

		$db = $this->db->get();
		return $db->row();
	}

	function InsertData($data, $table, $returnID = FALSE) {
		$this->db->insert($table, $data);
		if($this->db->affected_rows() > 0) {
			if($returnID) {
				return $this->db->insert_id();
			} else {
				return TRUE;
			}
		} else {
			return FALSE;
		}
	}

	function InsertBatchData($data, $table) {
		$this->db->insert_batch($table, $data);
		if($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function UpdateData($where, $table, $data) {
		if(is_array($where) && count($where) > 0) {
			foreach($where as $key => $value) {
				$this->db->where($value['column'], $value['value']);
			}
		}

		$this->db->update($table, $data);
		if($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function DeleteData($where, $table) {
		if(is_array($where) && count($where) > 0) {
			foreach($where as $key => $value) {
				$this->db->where($value['column'], $value['value']);
			}
		}

		$this->db->delete($table);
		if($this->db->affected_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function SoftDeleteData($where, $table, $data) {
		return $this->UpdateData($where, $table, $data);
	}

    // Misc
	function RandomString($length) {
		$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		return substr(str_shuffle($str), 0, $length);
	}

	function ShowMenu($arrRole,$mainrole) {
		$menu = [];
		for($i=0; $i<count($arrRole); $i++) {
			$gm = $this->MenuModel->GetMenuGroup($arrRole[$i],$mainrole);
			foreach($gm as $row) {
				$grup_menu["nama"] = $row->grup_menu;
				$grup_menu["isi"] = $this->MenuModel->GetItemMenu($row->id);

				$menu["grup_menu"][] = $grup_menu;
			}
		}

		return $menu;
    }

    function GetIDFromUID($uid,$field,$table) {
        $f_url = $field."_url";
    
        $this->db->select($field);
        $this->db->where($f_url, $uid);
        $q = $this->db->get($table);
          
        if($q->num_rows() == 0) {
            return "";
        } else {
            $res = $q->row();
            return $res->$field;
        }	
    }

	function GetUIDFromID($id,$field,$table) {
		$f_url = $field."_url";

		$this->db->select($f_url);
		$this->db->where($field, $id);
		$q = $this->db->get($table);

		if($q->num_rows() == 0) {
			return "";
		} else {
			$res = $q->row();
			return $res->$f_url;
		}
	}
}
