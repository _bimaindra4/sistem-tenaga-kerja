<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utusan extends CI_Controller {
    protected $roles_user;

	// session
	protected $sess;
	protected $sess_not_con;
	protected $sess_data;

	public function __construct() {
        parent::__construct();
        $this->load->model("SessionModel");
        $this->load->model("UtusanModel");

        $this->sess = $this->SessionModel->GetSession();
		$this->sess_not_con = !$this->sess['session_userid'] && !$this->sess['session_role'];
		$this->roles_user = explode(",", $this->sess['session_role']);
        $this->sess_data = [
			"userid" => $this->sess['session_userid'],
			"userrole" => $this->sess['session_role'],
			"usernama" => $this->sess['session_nama'],
			"menu" => $this->sess['session_role']
		];
    }
    
    public function detail($id) {
        $data['header'] = $this->load->view('template/layout_header', $this->sess_data, TRUE);
        $data['footer'] = $this->load->view('template/layout_footer', $this->sess_data, TRUE);

        if(in_array('4', $this->roles_user)) {
            $data['utusan'] = $this->UtusanModel->GetDetailUtusan($id);
            $data['tenaker'] = $this->UtusanModel->GetTenakerUtusan($id);
            $this->load->view("balai_jasa/utusan_detail", $data);
        }
    }
}