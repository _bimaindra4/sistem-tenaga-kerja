<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tenaker extends CI_Controller {
	protected $roles_user;

	// session
	protected $sess;
	protected $sess_not_con;
	protected $sess_data;

	public function __construct() {
        parent::__construct();
        $this->load->library('datatables');
        $this->load->model("AppModel");
        $this->load->model("SessionModel");
        $this->load->model("TenakerModel");
        $this->load->model("UserModel");

        $this->sess = $this->SessionModel->GetSession();
		$this->sess_not_con = !$this->sess['session_userid'] && !$this->sess['session_role'];
		$this->roles_user = explode(",", $this->sess['session_role']);
        $this->sess_data = [
			"userid" => $this->sess['session_userid'],
			"userrole" => $this->sess['session_role'],
			"usernama" => $this->sess['session_nama'],
			"menu" => $this->sess['session_role']
		];

		if($this->sess_not_con) {
			redirect("login");
		}

    }

    public function index() {
		$data['header'] = $this->load->view('template/layout_header', $this->sess_data, TRUE);
		$data['footer'] = $this->load->view('template/layout_footer', $this->sess_data, TRUE);

		if(in_array('4', $this->roles_user)) {
			$data['tenaker'] = $this->UserModel->GetDataTenagaKerja("data");
			$this->load->view("balai_jasa/tenaker_validasi", $data);
		} else if(in_array('3', $this->roles_user)) {
			$data['tenaker'] = $this->UserModel->GetDataTenagaKerja("data");
			$this->load->view("iappi/tenaker_validasi", $data);
		}
	}
	
	public function detail($id) {
		$data['header'] = $this->load->view('template/layout_header', $this->sess_data, TRUE);
		$data['footer'] = $this->load->view('template/layout_footer', $this->sess_data, TRUE);

		if(in_array('4', $this->roles_user)) {
			$data['tenaker'] = $this->UserModel->GetDataTenagaKerja("data",$id);
			$data['pekerjaan'] = $this->TenakerModel->GetDetailUtusanTenaker($id);
			$data['sertifikat'] = $this->TenakerModel->GetDetailSertifikatTenaker($id);
			$this->load->view("balai_jasa/detail_tenaker", $data);
		}
	}

    public function TambahUtusan() {
		$data = [
			"id_tenaker_utusan" => "",
			"id_tenaker_utusan_url" => $this->AppModel->RandomString(20),
			"utusan_tenaker" => $this->input->post("nama_utusan"),
			"utusan_alamat" => $this->input->post("alamat_utusan"),
			"created_at" => $this->AppModel->DateTimeNow(),
			"status" => 1
		];

		$proc = $this->db->insert("tbl_tenaker_utusan", $data);
		$status = ($proc ? 1 : 2);
		$this->session->set_flashdata("status", $status);
		$this->session->set_flashdata("act", "Tambah Data");
    }

    public function TambahProyek() {
		if(in_array('6', $this->roles_user)) {
			// $data = [
			//     "id_tenaker_jabatan" => "",
			//     "id_tenaker_jabatan_url" => $this->AppModel->RandomString(20),
			//     "jabatan_nama" => $this->input->post("utusan"),
			//     "utusan_alamat" => $this->input->post("alamat"),
			//     "created_at" => $this->AppModel->DateTimeNow(),
			//     "status" => 1
			// ];

			// $proc = $this->db->insert("tbl_tenaker_jabatan", $data);
			// $status = ($proc ? 1 : 2);
			// $this->session->set_flashdata("status", $status);
			// $this->session->set_flashdata("act", "Tambah Data");
		}
    }

    public function TambahJabatan() {
		$data = [
			"id_tenaker_jabatan" => "",
			"id_tenaker_jabatan_url" => $this->AppModel->RandomString(20),
			"jabatan_nama" => $this->input->post("nama"),
			"jabatan_klasifikasi" => $this->input->post("klasifikasi"),
			"jabatan_kualifikasi" => $this->input->post("kualifikasi"),
			"created_at" => $this->AppModel->DateTimeNow(),
			"status" => 1
		];

		$proc = $this->db->insert("tbl_tenaker_jabatan", $data);
		$status = ($proc ? 1 : 2);
		$this->session->set_flashdata("status", $status);
		$this->session->set_flashdata("act", "Tambah Data");
    }

    public function ValidasiKelulusan($uidtk) {
        $this->TenakerModel->ValidasiKelulusan($uidtk);
        redirect('tenaker');
    }

    public function ValidasiKompeten($uidtk) {
        $this->TenakerModel->ValidasiKompeten($uidtk);
        redirect('tenaker');
    }

    public function ProfilTenaker() {
		$data['header'] = $this->load->view('template/layout_header', $this->sess_data, TRUE);
		$data['footer'] = $this->load->view('template/layout_footer', $this->sess_data, TRUE);

		if(in_array('5', $this->roles_user)) {
			$data['tenaker'] = $this->UserModel->GetDataTenagaKerja("data", $this->sess['session_userid']);
			$data['pekerjaan'] = $this->TenakerModel->GetDetailUtusanTenaker($this->sess['session_userid']);
			$data['bimtek'] = $this->TenakerModel->GetBimtekTenaker($this->sess['session_userid']);
			$this->load->view("tenaga_kerja/profil", $data);
		}
	}

	public function EditProfilTenaker() {
		$data['header'] = $this->load->view('template/layout_header', $this->sess_data, TRUE);
		$data['footer'] = $this->load->view('template/layout_footer', $this->sess_data, TRUE);

		if(in_array('5', $this->roles_user)) {
			$data['provinsi'] = $this->db->select("id_provinsi, provinsi_nama")->from("tbl_sys_provinsi")->get()->result();
			$data['utusan'] = $this->db->select("id_tenaker_utusan, utusan_tenaker")->from("tbl_tenaker_utusan")->get()->result();
			$data['jabker'] = $this->db->select("id_tenaker_jabatan, jabatan_nama")->from("tbl_tenaker_jabatan")->get()->result();
			$data['tenaker'] = $this->UserModel->GetDataTenagaKerja("data", $this->sess['session_userid']);
			$this->load->view("tenaga_kerja/profil_edit", $data);
		}
	}

    public function get_sertifikat_tenaker($uidtk) {
        $res = $this->TenakerModel->GetSertifikatTenaker($uidtk);
        $output = "";
        $no = 1;
        foreach($res as $row) {
            $output .= "
                <tr>
                    <td>".$no."</td>
                    <td>".$row->jabatan_nama."</td>
                    <td>".$row->sertifikat_nomor."</td>
                    <td>".ucfirst($row->sertifikat_jenis)."</td>
                    <td>".$this->AppModel->DateIndo($row->sertifikat_tanggal)."</td>
                    <td></td>
                </tr>
            ";

            $no++;
        }

        $res = ["output" => $output];
        echo json_encode($res);
    }

    public function get_sertifikat_tenaker_iappi($uidtk) {
        $res = $this->TenakerModel->GetSertifikatTenaker($uidtk);
        $output = "";
        $no = 1;
        foreach($res as $row) {
            $output .= "
                <tr>
                    <td>".$no."</td>
                    <td>".$row->jabatan_nama."</td>
                    <td>".$row->sertifikat_nomor."</td>
                    <td>".ucfirst($row->sertifikat_jenis)."</td>
                    <td>".$this->AppModel->DateIndo($row->sertifikat_tanggal)."</td>
                    <td></td>
                </tr>
            ";

            $no++;
        }

        $res = ["output" => $output];
        echo json_encode($res);
    }
}
