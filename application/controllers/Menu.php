<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {
    protected $roles_user;

	// session
	protected $sess;
	protected $sess_not_con;
    protected $sess_data;
    
	public function __construct() {
		parent::__construct();
        $this->load->model("SessionModel");
        
        $this->sess = $this->SessionModel->GetSession();
		$this->sess_not_con = !$this->sess['session_userid'] && !$this->sess['session_role'];
		$this->roles_user = explode(",", $this->sess['session_role']);
        $this->sess_data = [
			"userid" => $this->sess['session_userid'],
			"userrole" => $this->sess['session_role'],
			"usernama" => $this->sess['session_nama']
		];
    }
    
    public function index() {
        if($this->sess_not_con) {
			redirect("login");
		} else {
			$data['header'] = $this->load->view('template/layout_header', $this->sess_data, TRUE);
            $data['footer'] = $this->load->view('template/layout_footer', $this->sess_data, TRUE);
        
            if(in_array($this->roles_user, '6')) {
                //$data['menu'] = $this->MenuModel->GetDataMenu();
                
                $this->load->view('superuser/menu_index', $data);
			} else {
                redirect("login");
            }
        }
    }

    public function TambahMenu() {

    }

    public function EditMenu() {

    }

    public function HapusMenu() {
        
    }