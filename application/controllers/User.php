<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	protected $roles_user;

	// session
	protected $sess;
	protected $sess_not_con;
	protected $sess_data;

	public function __construct() {
		parent::__construct();

		date_default_timezone_set("Asia/Jakarta");

		$this->load->library('datatables');
		$this->load->model("AppModel");
		$this->load->model("SessionModel");
		$this->load->model("UserModel");
		$this->load->model("TenakerModel");

		$this->sess = $this->SessionModel->GetSession();
		$this->sess_not_con = !$this->sess['session_userid'] && !$this->sess['session_role'];
		$this->roles_user = explode(",", $this->sess['session_role']);
		$this->sess_data = [
			"userid" => $this->sess['session_userid'],
			"userrole" => $this->sess['session_role'],
			"usernama" => $this->sess['session_nama'],
			"menu" => $this->sess['session_role']
		];

		if ($this->sess_not_con) {
			redirect("login");
		}
	}

	public function DataIAPPI() {
		$data['header'] = $this->load->view('template/layout_header', $this->sess_data, TRUE);
		$data['footer'] = $this->load->view('template/layout_footer', $this->sess_data, TRUE);

		if (in_array('6', $this->roles_user)) {
			$this->load->view('superuser/user_iappi', $data);
		}
		else {
			redirect("login");
		}
	}

	public function TambahIAPPI() {
		if (in_array('6', $this->roles_user)) {
			date_default_timezone_set("Asia/Jakarta");
			$data = [
				"id_iappi" => '',
				"id_iappi_url" => $this->AppModel->RandomString(15),
				"iappi_nama" => $this->input->post("nama"),
				// "iappi_alamat" => $this->input->post("alamat"),
				"iappi_tempat_lahir" => $this->input->post("tempat"),
				"iappi_tanggal_lahir" => date("Y-m-d", strtotime($this->input->post("tanggal"))),
				"iappi_jkel" => $this->input->post("jkel"),
				// "iappi_pendidikan" => $this->input->post("pendidikan"),
				// "iappi_pekerjaan" => $this->input->post("pekerjaan"),
				// "iappi_kewarganegaraan" => $this->input->post("warga_negara"),
				// "iappi_sertifikat_ahli" => $this->input->post("sertifikat_ahli"),
				"iappi_notelp" => $this->input->post("nohp"),
				"iappi_email" => $this->input->post("email"),
				"iappi_username" => $this->AppModel->RandomString(8),
				"iappi_password" => sha1("password"),
				"created_at" => $this->AppModel->datetimeNow(),
				"status" => 1
			];

			$proc = $this->UserModel->TambahDataIAPPI($data);

			$status = ($proc ? 1 : 2);
			$this->session->set_flashdata("status", $status);
			$this->session->set_flashdata("act", "Tambah Data");

			redirect("user/iappi");
		}
	}

	public function EditIAPPI($uid) {
		if (in_array('6', $this->roles_user)) {
			date_default_timezone_set("Asia/Jakarta");
			$pass = $this->input->post("u_password");

			$data = [
				"iappi_nama" => $this->input->post("u_nama"),
				//"iappi_alamat" => $this->input->post("u_alamat"),
				"iappi_tempat_lahir" => $this->input->post("u_tempat"),
				"iappi_tanggal_lahir" => date("Y-m-d", strtotime($this->input->post("u_tanggal"))),
				"iappi_jkel" => $this->input->post("u_jkel"),
				// "iappi_pendidikan" => $this->input->post("u_pendidikan"),
				// "iappi_pekerjaan" => $this->input->post("u_pekerjaan"),
				// "iappi_kewarganegaraan" => $this->input->post("u_warga_negara"),
				// "iappi_sertifikat_ahli" => $this->input->post("u_sertifikat_ahli"),
				"iappi_notelp" => $this->input->post("u_nohp"),
				"iappi_email" => $this->input->post("u_email"),
				"iappi_username" => $this->input->post("u_username"),
				"iappi_password" => sha1($this->input->post("u_password")),
				"modified_at" => $this->AppModel->datetimeNow(),
			];

			if ($pass == "" || empty($pass) || $pass == NULL) {
				$data["iappi_password"] = sha1($this->input->post("u_password"));
			}

			$proc = $this->UserModel->EditDataIAPPI($data, $uid);

			$status = ($proc ? 1 : 2);
			$this->session->set_flashdata("status", $status);
			$this->session->set_flashdata("act", "Edit Data");

			redirect("user/iappi");
		}
	}

	public function HapusIAPPI($uid) {
		if (in_array('6', $this->roles_user)) {
			$proc = $this->UserModel->HapusDataIAPPI($uid);

			$status = ($proc ? 1 : 2);
			$this->session->set_flashdata("status", $status);
			$this->session->set_flashdata("act", "Hapus Data");

			redirect("user/iappi");
		}
	}

	public function DataAP3I() {
		$data['header'] = $this->load->view('template/layout_header', $this->sess_data, TRUE);
		$data['footer'] = $this->load->view('template/layout_footer', $this->sess_data, TRUE);

		if (in_array('6', $this->roles_user)) {
			$this->load->view('superuser/user_ap3i', $data);
		}
	}

	public function TambahAP3I() {
		if (in_array('6', $this->roles_user)) {
			$data = [
				"id_ap3i" => "",
				"id_ap3i_url" => $this->AppModel->RandomString(15),
				"ap3i_nama" => $this->input->post("nama"),
				"ap3i_username" => $this->AppModel->RandomString(8),
				"ap3i_password" => sha1("password"),
				"created_at" => $this->AppModel->DateTimeNow(),
				"status" => 1
			];

			$proc = $this->UserModel->TambahDataAP3I($data);

			$status = ($proc ? 1 : 2);
			$this->session->set_flashdata("status", $status);
			$this->session->set_flashdata("act", "Tambah Data");

			redirect("user/ap3i");
		}
	}

	public function EditAP3I($uid) {
		if (in_array('6', $this->roles_user)) {
			date_default_timezone_set("Asia/Jakarta");
			$pass = $this->input->post("u_password");

			$data = [
				"ap3i_nama" => $this->input->post("u_nama"),
				"ap3i_username" => $this->input->post("u_username"),
				"ap3i_password" => sha1($this->input->post("u_password")),
				"modified_at" => $this->AppModel->datetimeNow(),
			];

			if ($pass == "" || empty($pass) || $pass == NULL) {
				$data["ap3i_password"] = sha1($this->input->post("u_password"));
			}

			$proc = $this->UserModel->EditDataAP3I($data, $uid);

			$status = ($proc ? 1 : 2);
			$this->session->set_flashdata("status", $status);
			$this->session->set_flashdata("act", "Edit Data");

			redirect("user/ap3i");
		}
	}

	public function HapusAP3I($uid) {
		if ($this->sess_not_con) {
			redirect("login");
		}
		else {
			if (in_array('6', $this->roles_user)) {
				$proc = $this->UserModel->HapusDataAP3I($uid);

				$status = ($proc ? 1 : 2);
				$this->session->set_flashdata("status", $status);
				$this->session->set_flashdata("act", "Hapus Data");

				redirect("user/ap3i");
			}
			else {
				redirect("login");
			}
		}
	}

	public function DataBalaiJasa() {
		$data['header'] = $this->load->view('template/layout_header', $this->sess_data, TRUE);
		$data['footer'] = $this->load->view('template/layout_footer', $this->sess_data, TRUE);

		if (in_array('6', $this->roles_user)) {
			$this->load->view('superuser/user_baljasa', $data);
		}
	}

	public function TambahBalaiJasa() {
		if (in_array('6', $this->roles_user)) {
			$data = [
				"id_balai_jasa" => "",
				"id_balai_jasa_url" => $this->AppModel->RandomString(15),
				"baljas_nama" => $this->input->post("nama"),
				"baljas_username" => $this->AppModel->RandomString(8),
				"baljas_password" => sha1("password"),
				"created_at" => $this->AppModel->DateTimeNow(),
				"status" => 1
			];

			$proc = $this->UserModel->TambahDataBalaiJasa($data);

			$status = ($proc ? 1 : 2);
			$this->session->set_flashdata("status", $status);
			$this->session->set_flashdata("act", "Tambah Data");

			redirect("user/balai_jasa");
		}
	}

	public function EditBalaiJasa($uid) {
		if (in_array('6', $this->roles_user)) {
			date_default_timezone_set("Asia/Jakarta");
			$pass = $this->input->post("u_password");

			$data = [
				"baljas_nama" => $this->input->post("u_nama"),
				"baljas_username" => $this->input->post("u_username"),
				"baljas_password" => sha1($this->input->post("u_password")),
				"modified_at" => $this->AppModel->datetimeNow(),
			];

			if ($pass == "" || empty($pass) || $pass == NULL) {
				$data["baljas_password"] = sha1($this->input->post("u_password"));
			}

			$proc = $this->UserModel->EditDataBalaiJasa($data, $uid);

			$status = ($proc ? 1 : 2);
			$this->session->set_flashdata("status", $status);
			$this->session->set_flashdata("act", "Edit Data");

			redirect("user/balai_jasa");
		}
		else {
			redirect("login");
		}
	}

	public function HapusBalaiJasa($uid) {
		if (in_array('6', $this->roles_user)) {
			$proc = $this->UserModel->HapusDataBalaiJasa($uid);

			$status = ($proc ? 1 : 2);
			$this->session->set_flashdata("status", $status);
			$this->session->set_flashdata("act", "Hapus Data");

			redirect("user/balai_jasa");
		}
		else {
			redirect("login");
		}
	}

	public function DataTenagaKerja() {
		$data['header'] = $this->load->view('template/layout_header', $this->sess_data, TRUE);
		$data['footer'] = $this->load->view('template/layout_footer', $this->sess_data, TRUE);

		if (in_array('6', $this->roles_user)) {
			$data['provinsi'] = $this->db->select("id_provinsi, provinsi_nama")->from("tbl_sys_provinsi")->get()->result();
			$data['jabker'] = $this->db->select("id_tenaker_jabatan, jabatan_nama")->from("tbl_tenaker_jabatan")->get()->result();
			$this->load->view('superuser/user_tenaker', $data);
		}
		else {
			redirect("login");
		}
	}

	public function FormTambahTenagaKerja() {
		$data['header'] = $this->load->view('template/layout_header', $this->sess_data, TRUE);
		$data['footer'] = $this->load->view('template/layout_footer', $this->sess_data, TRUE);

		if (in_array('6', $this->roles_user)) {
			$data['provinsi'] = $this->db->select("id_provinsi, provinsi_nama")->from("tbl_sys_provinsi")->get()->result();
			$data['jabker'] = $this->db->select("id_tenaker_jabatan, jabatan_nama")->from("tbl_tenaker_jabatan")->get()->result();
			$this->load->view('superuser/user_tenaker_tambah', $data);
		}
	}

	public function TambahTenagaKerja() {
		if (in_array('6', $this->roles_user)) {
			$config['upload_path'] = './assets/images/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['max_size'] = 2048;
			$config['overwrite'] = TRUE;
			$config['encrypt_name'] = TRUE;
			$this->upload->initialize($config);

			if ($_FILES['file_foto_ktp']['name']) {
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('file_foto_ktp')) {
					$error = $this->upload->display_errors();
					$fotoKTP = NULL;
				}
				else {
					$fotoKTP = $this->upload->data("file_name");
				}
			}

			if ($_FILES['file_foto_diri']['name']) {
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('file_foto_diri')) {
					$error = $this->upload->display_errors();
					$fotoDiri = NULL;
				}
				else {
					$fotoDiri = $this->upload->data("file_name");
				}
			}

			if ($_FILES['file_ijazah']['name']) {
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('file_ijazah')) {
					$error = $this->upload->display_errors();
					$fotoIjazah = NULL;
				}
				else {
					$fotoIjazah = $this->upload->data("file_name");
				}
			}

			if ($_FILES['file_npwp']['name']) {
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('file_npwp')) {
					$error = $this->upload->display_errors();
					$fotoNPWP = NULL;
				}
				else {
					$fotoNPWP = $this->upload->data("file_name");
				}
			}

			$data = [
				"id_tenaga_kerja" => "",
				"id_tenaga_kerja_url" => $this->AppModel->RandomString(20),
				"id_tenaker_utusan" => $this->input->post("utusan"),
				"id_tenaker_jabatan" => $this->input->post("jabatan"),
				"tenaker_nama" => $this->input->post("nama_lengkap"),
				"tenaker_nik" => $this->input->post("nik"),
				"tenaker_alamat" => $this->input->post("alamat"),
				"tenaker_provinsi" => $this->input->post("provinsi"),
				"tenaker_kabkot" => $this->input->post("kabkot"),
				"tenaker_tempat_lahir" => $this->input->post("tempat_lhr"),
				"tenaker_tanggal_lahir" => date("Y-m-d", strtotime($this->input->post("tgl_lhr"))),
				"tenaker_jkel" => $this->input->post("jkel"),
				"tenaker_kewarganegaraan" => $this->input->post("kewarganegaraan"),
				"tenaker_npwp" => $fotoNPWP,
				"tenaker_no_npwp" => $this->input->post("no_npwp"),
				"tenaker_proyek" => $this->input->post("nama_proyek"),
				"tenaker_pendidikan" => $this->input->post("pendidikan"),
				"tenaker_institusi" => $this->input->post("institusi"),
				"tenaker_jurusan" => $this->input->post("jurusan"),
				"tenaker_tanggal_lulus" => date("Y-m-d", strtotime($this->input->post("tgl_lulus"))),
				"tenaker_email" => $this->input->post("email"),
				"tenaker_nohp" => $this->input->post("nohp"),
				"tenaker_foto" => $fotoDiri,
				"tenaker_ktp" => $fotoKTP,
				"tenaker_ijazah" => $fotoIjazah,
				"tenaker_username" => $this->AppModel->RandomString(8),
				"tenaker_password" => sha1("password"),
				"created_at" => $this->AppModel->DateTimeNow(),
				"status" => 1
			];

			$proc = $this->UserModel->TambahDataTenagaKerja($data);

			$datarole = [
				"id_user" => $proc,
				"id_role" => 5,
				"user_table" => NULL,
				"created_at" => $this->AppModel->DateTimeNow(),
				"status" => 1
			];

			$this->db->insert("tbl_sys_role_detail", $datarole);
			$status = (isset($proc) ? 1 : 2);
			$this->session->set_flashdata("status", $status);
			$this->session->set_flashdata("act", "Tambah Data");

			redirect("user/tenaga_kerja");
		}
	}

	public function FormEditTenagaKerja($id) {
		$data['header'] = $this->load->view('template/layout_header', $this->sess_data, TRUE);
		$data['footer'] = $this->load->view('template/layout_footer', $this->sess_data, TRUE);

		if (in_array('6', $this->roles_user)) {
			$data['provinsi'] = $this->db->select("id_provinsi, provinsi_nama")->from("tbl_sys_provinsi")->get()->result();
			$data['jabker'] = $this->db->select("id_tenaker_jabatan, jabatan_nama")->from("tbl_tenaker_jabatan")->get()->result();
			$data['tenaker'] = $this->UserModel->GetDataTenagaKerja('data', $id);
			$this->load->view('superuser/user_tenaker_edit', $data);
		}
	}

	public function EditDataDiriTenagaKerja($uid) {
		$data = [
			"tenaker_nama" => $this->input->post("nama_lengkap"),
			"tenaker_nik" => $this->input->post("nik"),
			"tenaker_alamat" => $this->input->post("alamat"),
			"tenaker_provinsi" => $this->input->post("provinsi"),
			"tenaker_kabkot" => $this->input->post("kabkot"),
			"tenaker_tempat_lahir" => $this->input->post("tempat_lhr"),
			"tenaker_tanggal_lahir" => date("Y-m-d", strtotime($this->input->post("tgl_lhr"))),
			"tenaker_jkel" => $this->input->post("jkel"),
			"tenaker_kewarganegaraan" => $this->input->post("kewarganegaraan"),
			"tenaker_email" => $this->input->post("email"),
			"tenaker_nohp" => $this->input->post("nohp"),
			"tenaker_username" => $this->input->post("username"),
			"tenaker_password" => sha1($this->input->post("password")),
			"modified_at" => $this->AppModel->DateTimeNow()
		];

		if ($this->input->post("password_edit") != NULL || $this->input->post("password_edit") != "") {
			$data["tenaker_password"] = sha1($this->input->post("password_edit"));
		}

		$proc = $this->UserModel->EditDataTenagaKerja($data, $uid);
		$status = ($proc ? 1 : 2);
		$this->session->set_flashdata("status", $status);
		$this->session->set_flashdata("act", "Edit Data");

		if (in_array('5', $this->roles_user)) {
			redirect("profil/edit");
		}
		else if (in_array('6', $this->roles_user)) {
			redirect("user/tenaga_kerja/" . $uid . "/edit/form");
		}
	}

	public function EditPendidikanTenagaKerja($uid) {
		$data = [
			"id_tenaker_utusan" => $this->input->post("utusan"),
			"id_tenaker_jabatan" => $this->input->post("jabatan"),
			"tenaker_proyek" => $this->input->post("nama_proyek"),
			"tenaker_pendidikan" => $this->input->post("pendidikan"),
			"tenaker_institusi" => $this->input->post("institusi"),
			"tenaker_jurusan" => $this->input->post("jurusan"),
			"tenaker_tanggal_lulus" => date("Y-m-d", strtotime($this->input->post("tgl_lulus"))),
			"modified_at" => $this->AppModel->DateTimeNow()
		];

		if ($this->input->post("password_edit") != NULL || $this->input->post("password_edit") != "") {
			$data["tenaker_password"] = sha1($this->input->post("password_edit"));
		}

		$proc = $this->UserModel->EditDataTenagaKerja($data, $uid);
		$status = ($proc ? 1 : 2);
		$this->session->set_flashdata("status", $status);
		$this->session->set_flashdata("act", "Edit Data");

		if (in_array('5', $this->roles_user)) {
			redirect("profil/edit");
		}
		else if (in_array('6', $this->roles_user)) {
			redirect("user/tenaga_kerja/" . $uid . "/edit/form");
		}
	}

	public function EditLampiranTenagaKerja($uid) {
		$config['upload_path'] = './assets/images/';
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size'] = 2048;
		$config['overwrite'] = TRUE;
		$config['encrypt_name'] = TRUE;
		$this->upload->initialize($config);

		if ($_FILES['file_foto_ktp']['name']) {
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('file_foto_ktp')) {
				$error = $this->upload->display_errors();
				$fotoKTP = NULL;
			}
			else {
				$fotoKTP = $this->upload->data("file_name");
			}
		}

		if ($_FILES['file_foto_diri']['name']) {
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('file_foto_diri')) {
				$error = $this->upload->display_errors();
				$fotoDiri = NULL;
			}
			else {
				$fotoDiri = $this->upload->data("file_name");
			}
		}

		if ($_FILES['file_ijazah']['name']) {
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('file_ijazah')) {
				$error = $this->upload->display_errors();
				$fotoIjazah = NULL;
			}
			else {
				$fotoIjazah = $this->upload->data("file_name");
			}
		}

		$noNPWP = $this->input->post("no_npwp");

		if ($_FILES['file_npwp']['name']) {
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('file_npwp')) {
				$error = $this->upload->display_errors();
				$fotoNPWP = NULL;
			}
			else {
				$fotoNPWP = $this->upload->data("file_name");
			}
		}

		if ($_FILES['file_cv']['name']) {
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('file_cv')) {
				$error = $this->upload->display_errors();
				$fotoCV = NULL;
			}
			else {
				$fotoCV = $this->upload->data("file_name");
			}
		}

		$data = [
			"tenaker_foto" => $fotoDiri,
			"tenaker_ktp" => $fotoKTP,
			"tenaker_ijazah" => $fotoIjazah,
			"tenaker_no_npwp" => $noNPWP,
			"tenaker_npwp" => $fotoNPWP,
			"tenaker_cv" => $fotoCV,
			"modified_at" => $this->AppModel->DateTimeNow()
		];

		$proc = $this->UserModel->EditDataTenagaKerja($data, $uid);
		$status = ($proc ? 1 : 2);
		$this->session->set_flashdata("status", $status);
		$this->session->set_flashdata("act", "Edit Data");

		if (in_array('5', $this->roles_user)) {
			redirect("profil/edit");
		}
		else if (in_array('6', $this->roles_user)) {
			redirect("user/tenaga_kerja/" . $uid . "/edit/form");
		}
	}

	public function HapusTenagaKerja($uid) {
		if (in_array('6', $this->roles_user)) {
			$proc = $this->UserModel->HapusDataTenagaKerja($uid);
			$status = ($proc ? 1 : 2);
			$this->session->set_flashdata("status", $status);
			$this->session->set_flashdata("act", "Hapus Data");

			redirect("user/tenaga_kerja");
		}
		else {
			redirect("login");
		}
	}

	public function get_data_iappi() {
		header('Content-Type: application/json');
		echo $this->UserModel->GetDataIAPPI('table');
	}

	public function get_data_ap3i() {
		header('Content-Type: application/json');
		echo $this->UserModel->GetDataAP3I('table');
	}

	public function get_data_balai_jasa() {
		header('Content-Type: application/json');
		echo $this->UserModel->GetDataBalaiJasa('table');
	}

	public function get_data_tenaga_kerja() {
		header('Content-Type: application/json');
		echo $this->UserModel->GetDataTenagaKerja('table');
	}

	public function get_detail_iappi($uid) {
		$sql = $this->UserModel->GetDataIAPPI('data', $uid);
		echo json_encode($sql);
	}

	public function get_detail_ap3i($uid) {
		$sql = $this->UserModel->GetDataAP3I('data', $uid);
		echo json_encode($sql);
	}

	public function get_detail_balai_jasa($uid) {
		$sql = $this->UserModel->GetDataBalaiJasa('data', $uid);
		echo json_encode($sql);
	}

	public function get_detail_tenaga_kerja($uid) {
		$sql = $this->UserModel->GetDataTenagaKerja('data', $uid);
		echo json_encode($sql);
	}

	public function get_kabkot_by_id_prov($id) {
		$output = "";

		$sql = $this->db->select("id_kabkot, kabkot_nama")->from("tbl_sys_kabkot")->where("id_provinsi", $id)->get();
		if ($sql->num_rows() == 0) {
		}
		else {
			foreach ($sql->result() as $row) {
				$output .= "<option value='" . $row->id_kabkot . "'>" . $row->kabkot_nama . "</option>";
			}
		}

		$data['output'] = $output;
		echo json_encode($data);
	}

	public function show_data_utusan_tenaker() {
		$output = "";

		$sql = $this->TenakerModel->GetUtusanTenaker();
		foreach ($sql as $row) {
			$output .= "<option value='" . $row->id_tenaker_utusan . "'>" . $row->utusan_tenaker . "</option>";
		}

		echo $output;
	}

	public function show_data_jabatan_tenaker() {
		$output = "";

		$sql = $this->TenakerModel->GetJabatanTenaker();
		foreach ($sql as $row) {
			$output .= "<option value='" . $row->id_tenaker_jabatan . "'>" . $row->jabatan_nama . "</option>";
		}

		echo $output;
	}
}
