<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	protected $roles_user;

	// session
	protected $sess;
	protected $sess_not_con;
	protected $sess_data;

	public function __construct() {
        parent::__construct();
        $this->load->model("SessionModel");
		$this->load->model("DashboardModel");
		$this->load->model("SertifikatModel");

        $this->sess = $this->SessionModel->GetSession();
		$this->sess_not_con = !$this->sess['session_userid'] && !$this->sess['session_role'];
		$this->roles_user = explode(",", $this->sess['session_role']);
        $this->sess_data = [
			"userid" => $this->sess['session_userid'],
			"userrole" => $this->sess['session_role'],
			"usernama" => $this->sess['session_nama'],
			"menu" => $this->sess['session_role']
		];
	}
	
	public function index()	{
		if($this->sess_not_con) {
			redirect("login");
		} else {
			$data['header'] = $this->load->view('template/layout_header', $this->sess_data, TRUE);
			$data['footer'] = $this->load->view('template/layout_footer', $this->sess_data, TRUE);
			$data['iappi'] = $this->DashboardModel->GetTotalUser("iappi");
			$data['ap3i'] = $this->DashboardModel->GetTotalUser("ap3i");
			$data['balaijasa'] = $this->DashboardModel->GetTotalUser("balaijasa");
			$data['tenaker'] = $this->DashboardModel->GetTotalUser("tenaker");

			if(in_array('6', $this->roles_user)) {
				$data['sebaran_tenaker'] = $this->DashboardModel->SebaranTenaker();
				$this->load->view('superuser/dashboard', $data);
			} else if(in_array('1', $this->roles_user)) {
				$this->load->view('kemenpupr/dashboard', $data);
			} else if(in_array('2', $this->roles_user)) {
				$this->load->view('ap3i/dashboard', $data);
			} else if(in_array('3', $this->roles_user)) {
				$this->load->view('iappi/dashboard', $data);
			} else if(in_array('4', $this->roles_user)) {
				$data['sebaran_tenaker'] = $this->DashboardModel->SebaranTenaker();
				$data['skat_aktif'] = $this->SertifikatModel->GetSKATAktif();
				$data['skat_hampir'] = $this->SertifikatModel->GetSKATHampirKadaluarsa();
				$data['skat_kadaluarsa'] = $this->SertifikatModel->GetSKATKadaluarsa();
				$this->load->view('balai_jasa/dashboard', $data);
			} else if(in_array('5', $this->roles_user)) {
				$this->load->view('tenaga_kerja/dashboard', $data);
			} else {
				redirect("login");
			}
		}
	}

	public function get_skat_aktif() {
		$sql = $this->SertifikatModel->GetSKATAktif();
		$no = 1;
		$output = "<table class='table table-bordered table-hover'>";
		$output .= "<tr><th>No</th><th>Tenaker</th><th>Tanggal</th></tr>";
		foreach($sql->result() as $row) {
			$output .= "<tr><td>".$no++."</td><td>".$row->nama."</td><td>".$row->tanggal."</td></tr>";
		}
		$output .= "</table>";
		$data = ["output" => $output];
		echo json_encode($data);
	}

	public function get_skat_hampir_kadaluarsa() {
		$sql = $this->SertifikatModel->GetSKATHampirKadaluarsa();
		$no = 1;
		$output = "<table class='table table-bordered table-hover'>";
		$output .= "<tr><th>No</th><th>Tenaker</th><th>Tanggal</th></tr>";
		foreach($sql->result() as $row) {
			$output .= "<tr><td>".$no++."</td><td>".$row->nama."</td><td>".$row->tanggal."</td></tr>";
		}
		$output .= "</table>";
		$data = ["output" => $output];
		echo json_encode($data);
	}

	public function get_skat_kadaluarsa() {
		$sql = $this->SertifikatModel->GetSKATKadaluarsa();
		$no = 1;
		$output = "<table class='table table-bordered table-hover'>";
		$output .= "<tr><th>No</th><th>Tenaker</th><th>Tanggal</th></tr>";
		foreach($sql->result() as $row) {
			$output .= "<tr><td>".$no++."</td><td>".$row->nama."</td><td>".$row->tanggal."</td></tr>";
		}
		$output .= "</table>";
		$data = ["output" => $output];
		echo json_encode($data);
	}
}
