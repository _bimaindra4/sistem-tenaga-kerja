<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model("LoginModel");
		$this->load->model("SessionModel");
	}
	
	public function index()	{
		$this->load->view('login');
	}

	public function Auth() {
		$user = $this->input->post("username");
		$pass = sha1($this->input->post("password"));

		$result = $this->LoginModel->Verification($user,$pass);
		if($result != []) {
			$iduser = $result->id;
			$nama = $result->nama;
			$role = $result->role;

			$this->SessionModel->StoreSession($iduser,$role,$nama);
			$this->session->set_flashdata("status_log", "1");
			redirect("dashboard");
		} else {
			$this->session->set_flashdata("status_log", "0");
			redirect("login");
		}
	}
}
