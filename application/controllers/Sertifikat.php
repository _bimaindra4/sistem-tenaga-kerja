<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sertifikat extends CI_Controller {
	protected $roles_user;

	// session
	protected $sess;
	protected $sess_not_con;
	protected $sess_data;

	public function __construct() {
        parent::__construct();
        $this->load->model("SessionModel");
		$this->load->model("DashboardModel");
		$this->load->model("SertifikatModel");
		$this->load->model("AppModel");

        $this->sess = $this->SessionModel->GetSession();
		$this->sess_not_con = !$this->sess['session_userid'] && !$this->sess['session_role'];
		$this->roles_user = explode(",", $this->sess['session_role']);
        $this->sess_data = [
			"userid" => $this->sess['session_userid'],
			"userrole" => $this->sess['session_role'],
			"usernama" => $this->sess['session_nama'],
			"menu" => $this->sess['session_role']
		];
    }

    function index() {
        if($this->sess_not_con) {
			redirect("login");
		} else {
			$data['header'] = $this->load->view('template/layout_header', $this->sess_data, TRUE);
            $data['footer'] = $this->load->view('template/layout_footer', $this->sess_data, TRUE);
            
            if(in_array('5', $this->roles_user)) {
				$data['sertifikat'] = $this->SertifikatModel->GetSertifikatTenaker($this->sess['session_userid']);
				$this->load->view('tenaga_kerja/sertifikat', $data);
			}
        }
    }
} 