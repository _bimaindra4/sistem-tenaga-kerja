<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bimtek extends CI_Controller {
	protected $roles_user;

	// session
	protected $sess;
	protected $sess_not_con;
	protected $sess_data;

	public function __construct() {
		parent::__construct();
		$this->load->model("SessionModel");
		$this->load->model("AppModel");
		$this->load->model("BimtekModel");
		$this->load->model("TenakerModel");

		date_default_timezone_set("Asia/Jakarta");
		$this->sess = $this->SessionModel->GetSession();
		$this->sess_not_con = !$this->sess['session_userid'] && !$this->sess['session_role'];
		$this->roles_user = explode(",", $this->sess['session_role']);
		$this->sess_data = [
			"userid" => $this->sess['session_userid'],
			"userrole" => $this->sess['session_role'],
			"usernama" => $this->sess['session_nama'],
			"menu" => $this->sess['session_role']
		];

		if ($this->sess_not_con) {
			redirect("login");
		}
	}

	public function Index() {
		$data['header'] = $this->load->view('template/layout_header', $this->sess_data, TRUE);
		$data['footer'] = $this->load->view('template/layout_footer', $this->sess_data, TRUE);

		if (in_array('4', $this->roles_user)) {
			$data['bimtek'] = $this->BimtekModel->GetDataBimtek();
			$this->load->view("balai_jasa/bimtek", $data);
		} else if(in_array('5', $this->roles_user)) {
			$data['bimtek'] = $this->BimtekModel->GetBimtekProcessViewOnTenaker($this->sess_data['userid']);
			$data['bimtek_done'] = $this->BimtekModel->GetBimtekDoneViewOnTenaker($this->sess_data['userid']);
			$data['jabatan'] = $this->TenakerModel->GetJabatanTenaker();
			$this->load->view("tenaga_kerja/bimtek", $data);
		}
	}

	public function TambahBimtek() {
		$nama = post('nama');
		$asosiasi = post('asosiasi');
		$kompetensi = post('kompetensi');
		$tgl_mulai = post('tgl_mulai');
		$tgl_selesai = post('tgl_selesai');
		$deskripsi = post('deskripsi');

		$data = [
			"id_bimtek_url" => $this->AppModel->RandomString(20),
			"id_tenaker_jabatan" => $kompetensi,
			"bimtek_nama" => $nama,
			"tgl_mulai_bimtek" => date("Y-m-d", strtotime($tgl_mulai)),
			"tgl_selesai_bimtek" => date("Y-m-d", strtotime($tgl_selesai)),
			"deskripsi" => $deskripsi,
			"status_bimtek" => 1,
			"created_at" => $this->AppModel->DateTimeNow(),
			"status" => 1
		];

		$result = $this->AppModel->InsertData($data, "tbl_bimtek");
		$status = ($result ? 1 : 2);
		$this->session->set_flashdata("status", $status);
		$this->session->set_flashdata("act", "Tambah Data");
		redirect("bimtek");
	}

	public function DetailBimtek($id) {
		$data['header'] = $this->load->view('template/layout_header', $this->sess_data, TRUE);
		$data['footer'] = $this->load->view('template/layout_footer', $this->sess_data, TRUE);

		if (in_array('4', $this->roles_user)) {
			// Get Data Bimtek
			$data['bimtek'] = $this->BimtekModel->GetDetailBimtek($id);

			// Get Data Peserta
			$data['peserta'] = $this->BimtekModel->GetDataPesertaBimtek($id);
			$data['lulus'] = $this->BimtekModel->GetTotalPesertaLulus($id);
			$data['kompeten'] = $this->BimtekModel->GetTotalPesertaKompeten($id);

			// Get Data Kegiatan
			$data['kegiatan'] = $this->BimtekModel->GetKegiatanBimtek($id);

			// Get Data Nilai
			$data['nilai'] = $this->BimtekModel->GetNilaiBimtekPeserta($id);

			$data['idbimtek'] = $id;
			$this->load->view("balai_jasa/bimtek_detail", $data);
		}
	}

	public function UbahStatus($id) {
		$data = [
			"status_bimtek" => post('status'),
			"modified_at" => $this->AppModel->DateTimeNow()
		];
		$where = [
			[
				"column" => "id_bimtek_url",
				"value" => $id
			]
		];

		$proc = $this->AppModel->UpdateData($where, "tbl_bimtek", $data);
		if($proc) {
			redirect('bimtek/'.$id);
		}
	}

	public function FormTambahKegiatanBimtek($id) {
		$data['header'] = $this->load->view('template/layout_header', $this->sess_data, TRUE);
		$data['footer'] = $this->load->view('template/layout_footer', $this->sess_data, TRUE);

		if (in_array('4', $this->roles_user)) {
			$data['peserta'] = $this->BimtekModel->GetDataPesertaBimtek($id);
			$this->load->view("balai_jasa/bimtek_kegiatan_tambah", $data);
		}
	}

	public function TambahKegiatanBimtek($id) {
		$data = [
			"id_bimtek_kehadiran" => "",
			"id_bimtek_kehadiran_url" => $this->AppModel->RandomString(20),
			"id_bimtek" => $this->AppModel->GetIDFromUID(post('id_bimtek'), "id_bimtek", "tbl_bimtek"),
			"tgl_kehadiran" => date("Y-m-d", strtotime(post('tgl_bimtek'))),
			"kegiatan" => post('kegiatan'),
			"created_at" => $this->AppModel->DateNow(),
			"status" => 1
		];
		$keg_bimtek = $this->AppModel->InsertData($data, "tbl_bimtek_kehadiran", TRUE);

		for($i=0; $i<count(post('id')); $i++) {
			$id_peserta = $this->AppModel->GetIDFromUID(post('id')[$i], "id_bimtek_peserta", "tbl_bimtek_peserta");
			$peserta = [
				"id_bimtek_kehadiran" => $keg_bimtek,
				"id_bimtek_peserta" => $id_peserta,
				"status_hadir" => post('keterangan')[$i]
			];

			$data_peserta[] = $peserta;
		}
		$result = $this->AppModel->InsertBatchData($data_peserta, "tbl_bimtek_kehadiran_detail");

		// Hitung nilai absensi
		$this->BimtekModel->HitungAbsensi($id);

		$status = ($result ? 1 : 2);
		$this->session->set_flashdata("status", $status);
		$this->session->set_flashdata("act", "Tambah Data");
		redirect("bimtek/".$id);
	}

	public function FormEditKegiatanBimtek($id) {
		$data['header'] = $this->load->view('template/layout_header', $this->sess_data, TRUE);
		$data['footer'] = $this->load->view('template/layout_footer', $this->sess_data, TRUE);

		if (in_array('4', $this->roles_user)) {
			$data['detail'] = $this->BimtekModel->GetKegiatanBimtekDetail($id);
			$this->load->view("balai_jasa/bimtek_kegiatan_edit", $data);
		}
	}

	public function EditKegiatanBimtek($idurl) {
		$id = $this->AppModel->GetIDFromUID($idurl, "id_bimtek_kehadiran", "tbl_bimtek_kehadiran");
		$id_peserta = $this->input->post("id");
		$keterangan = $this->input->post("keterangan");

		// Update Kehadiran
		for($i = 0; $i<count($id_peserta); $i++) {
			$data = [
				"status_hadir" => $keterangan[$i]
			];
			$where = [
				["column" => "id_bimtek_kehadiran", "value" => $id],
				["column" => "id_bimtek_peserta", "value" => $id_peserta[$i]]
			];
			$this->AppModel->UpdateData($where, "tbl_bimtek_kehadiran_detail", $data);
		}

		// Update Kegiatan
		$data = [
			"tgl_kehadiran" => date("Y-m-d", strtotime($this->input->post('tgl_bimtek'))),
			"kegiatan" => $this->input->post("kegiatan")
		];
		$where = [["column" => "id_bimtek_kehadiran", "value" => $id]];
		$this->AppModel->UpdateData($where, "tbl_bimtek_kehadiran", $data);

		// Get ID Bimtek
		$this->db->select("id_bimtek");
		$this->db->where("id_bimtek_kehadiran_url", $idurl);
		$sql = $this->db->get("tbl_bimtek_kehadiran")->row()->id_bimtek;
		$id_bimtek = $this->AppModel->GetUIDFromID($sql, "id_bimtek", "tbl_bimtek");
		
		// Hitung nilai absensi
		$this->BimtekModel->HitungAbsensi($id_bimtek);
		
		redirect("bimtek/".$id_bimtek);
	}

	public function HapusKegiatanBimtek($id) {
		$id_bimtek = post("id_bimtek");
		$result = $this->BimtekModel->HapusBimtekKegiatan($id);

		// Hitung nilai absensi
		$this->BimtekModel->HitungAbsensi($id_bimtek);

		$status = ($result ? 1 : 2);
		$this->session->set_flashdata("status", $status);
		$this->session->set_flashdata("act", "Hapus Data");
		redirect("bimtek/".$id_bimtek);
	}

	public function EditBimtek($id) {
		$data = [
			"id_tenaker_jabatan" => $this->input->post("kompetensi"),
			"id_asosiasi" => $this->input->post("asosiasi"),
			"bimtek_nama" => $this->input->post("nama"),
			"tgl_bimtek" => date("Y-m-d", strtotime($this->input->post("tgl_bimtek"))),
			"deskripsi" => $this->input->post("deskripsi"),
			"modified_at" => $this->AppModel->DateTimeNow()
		];

		$result = $this->BimtekModel->EditBimtek($data, $id);
		$status = ($result ? 1 : 2);
		$this->session->set_flashdata("status", $status);
		$this->session->set_flashdata("act", "Edit Data");
		redirect("bimtek");
	}

	public function HapusBimtek($id) {
		$result = $this->BimtekModel->HapusBimtek($id);
		$status = ($result ? 1 : 2);
		$this->session->set_flashdata("status", $status);
		$this->session->set_flashdata("act", "Hapus Data");
		redirect("bimtek");
	}

	public function KirimIjazah($id) {
		$peserta = $this->BimtekModel->GetPesertaKompeten($id);

		foreach($peserta as $row) {
			$sertifikat[] = [
				"id_tenaker_sertifikat" => "",
				"id_tenaker_sertifikat_url" => $this->AppModel->RandomString(20),
				"id_tenaga_kerja" => $row->id_tenaga_kerja,
				"id_tenaker_jabatan" => $row->id_tenaker_jabatan,
				"sertifikat_jenis" => "ahli",
				"sertifikat_nomor" => NULL,
				"sertifikat_tanggal" => date("Y-m-d", strtotime("+3 years")),
				"sertifikat_asosiasi" => NULL,
				"sertifikat_file" => NULL,
				"created_at" => $this->AppModel->DateTimeNow(),
				"status" => 1
			];
		}

		$sql = $this->db->insert_batch("tbl_tenaker_sertifikat", $sertifikat);
		redirect("bimtek/".$id);
	}

	public function DaftarBimtek($id_bimtek) {
		if (in_array('5', $this->roles_user)) {
			$userid = $this->sess_data['userid'];
			$data = [
				"id_bimtek_peserta_url" => $this->AppModel->RandomString(20),
				"id_bimtek" => $this->AppModel->GetIDFromUID($id_bimtek, "id_bimtek", "tbl_bimtek"),
				"id_tenaga_kerja" => $this->AppModel->GetIDFromUID($userid, "id_tenaga_kerja", "tbl_master_tenaga_kerja"),
				"status_peserta" => 0,
				"status_lulus" => NULL,
				"status_kompeten" => NULL,
				"created_at" => $this->AppModel->DateTimeNow(),
				"status" => 1
			];

			$result = $this->BimtekModel->DaftarBimtekPeserta($data);
			$status = ($result ? 1 : 2);
			$this->session->set_flashdata("status", $status);
			$this->session->set_flashdata("act", "Daftar Peserta");
			redirect("bimtek");
		}
	}

	public function FormAturNilaiBimtek($id) {
		$data['header'] = $this->load->view('template/layout_header', $this->sess_data, TRUE);
		$data['footer'] = $this->load->view('template/layout_footer', $this->sess_data, TRUE);

		if (in_array('4', $this->roles_user)) {
			$nilai = $this->BimtekModel->GetNilaiBimtekPeserta($id);

			$data['id_bimtek'] = $id;
			$data['detail'] = $this->BimtekModel->GetNilaiBimtekPeserta($id);

			if($nilai->num_rows() == 0) {
				$data['mode'] = 'insert';
				$data['peserta'] = $this->BimtekModel->GetDataPesertaBimtek($id);
			} else {
				$data['mode'] = 'edit';
				$data['peserta'] = $nilai->result();
			}

			$this->load->view("balai_jasa/bimtek_atur_nilai", $data);
		}
	}

	public function AturNilaiBimtek($id) {
		$bimtek = $this->input->post('id_bimtek');
		$peserta = $this->input->post('id_peserta');
		$kehadiran = $this->input->post('kehadiran');
		$pretest = $this->input->post('pretest');
		$posttest = $this->input->post('posttest');
		$mandiri = $this->input->post('mandiri');

		for($i=0; $i<count($peserta); $i++) {
			$nilai = [
				"id_bimtek_nilai_url" => $this->AppModel->RandomString(20),
				"id_bimtek" => $this->AppModel->GetIDFromUID($bimtek, "id_bimtek", "tbl_bimtek"),
				"id_bimtek_peserta" => $this->AppModel->GetIDFromUID($peserta[$i], "id_bimtek_peserta", "tbl_bimtek_peserta"),
				"nilai_hadir" => $kehadiran[$i],
				"nilai_pretest" => $pretest[$i],
				"nilai_posttest" => $posttest[$i],
				"nilai_mandiri" => $mandiri[$i],
				"created_at" => $this->AppModel->DateTimeNow(),
				"status" => 1
			];

			$data[] = $nilai;
		}

		$res = $this->BimtekModel->AturNilaiBimtekPeserta($data);
		if($res) {
			redirect('bimtek/'.$id);
		}
	}

	public function EditNilaiBimtek($id) {
		$id_nilai = $this->input->post('id_nilai');
		$kehadiran = $this->input->post('kehadiran');
		$pretest = $this->input->post('pretest');
		$posttest = $this->input->post('posttest');
		$mandiri = $this->input->post('mandiri');
		
		for($i=0; $i<count($id_nilai); $i++) {
			$nilai = [
				"nilai_pretest" => $pretest[$i],
				"nilai_posttest" => $posttest[$i],
				"nilai_mandiri" => $mandiri[$i],
				"modified_at" => $this->AppModel->DateTimeNow()
			];

			$res = $this->BimtekModel->EditNilaiBimtekPeserta($nilai, $id_nilai[$i]);
		}

		redirect('bimtek/'.$id);
	}

	public function EditStatusPeserta($id) {
		$uid_bimtek = $this->input->post("url");
		$id_bimtek = $this->AppModel->GetIDFromUID($uid_bimtek, "id_bimtek", "tbl_bimtek");
		$id_peserta = $this->AppModel->GetIDFromUID($id, "id_bimtek_peserta", "tbl_bimtek_peserta");
		$status = $this->input->post("status");

		if(isset($status)) {
			$data = ["status_peserta" => $status];
			$result = $this->BimtekModel->EditStatusPeserta($data, $id);

			// Tambah data pada tabel penilaian
			if($status == 1) {
				$data = [
					"id_bimtek_nilai" => "",
					"id_bimtek_nilai_url" => $this->AppModel->RandomString(20),
					"id_bimtek" => $id_bimtek,
					"id_bimtek_peserta" => $id_peserta,
					"nilai_hadir" => 0,
					"nilai_pretest" => 0,
					"nilai_posttest" => 0,
					"nilai_mandiri" => 0,
					"nilai_akhir" => NULL,
					"created_at" => $this->AppModel->DateTimeNow(),
					"status" => 1
				];
				$this->BimtekModel->TambahDataNilaiPeserta($data);
			} else if($status == 0) {
				$data = [
					"id_bimtek" => $id_bimtek,
					"id_bimtek_peserta" => $id_peserta,
				];
				$this->BimtekModel->HapusNilaiPeserta($data);
			}
		} else {
			$data = [
				"status_lulus" => $this->input->post("lulus"),
				"status_kompeten" => $this->input->post("kompeten")
			];
			$result = $this->BimtekModel->EditStatusPeserta($data, $id);
		}

		$status = ($result ? 1 : 2);
		$this->session->set_flashdata("status", $status);
		$this->session->set_flashdata("act", "Edit Status Peserta");
		redirect("bimtek/".$uid_bimtek);
	}

	public function show_data_asosiasi() {
		$select = "id_asosiasi, nama_asosiasi, singkatan";
		$from = "tbl_asosiasi";
		$where = [["column" => "status", "value" => 1]];

		$sql = $this->AppModel->GetDataResult($select, $from, NULL, $where, NULL, NULL, NULL, NULL);
		$output = "";
		foreach ($sql as $row) {
			$output .= "<option value='".$row->id_asosiasi."'>".$row->nama_asosiasi." (".$row->singkatan.")</option>";
		}

		echo $output;
	}

	public function get_data_bimtek($id) {
		$this->db->select("bimtek_nama, id_tenaker_jabatan AS kompetensi, 
						  tgl_mulai_bimtek, tgl_selesai_bimtek, deskripsi");
		$this->db->where("id_bimtek_url", $id);
		$sql = $this->db->get("tbl_bimtek")->row();

		echo json_encode($sql);
	}
}

