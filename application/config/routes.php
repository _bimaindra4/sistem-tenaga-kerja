<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login'] = 'Login';
$route['login/auth'] = 'Login/Auth';
$route['logout'] = 'Logout';
$route['dashboard'] = 'Dashboard';

$route['menu'] = 'Menu';
$route['menu/tambah'] = 'Menu/TambahMenu';
$route['menu/(:any)/edit'] = 'Menu/EditMenu/$1';
$route['menu/(:any)/hapus'] = 'Menu/HapusMenu/$1';

$route['user'] = 'User';
$route['user/iappi'] = 'User/DataIAPPI';
$route['user/iappi/tambah'] = 'User/TambahIAPPI';
$route['user/iappi/(:any)/edit'] = 'User/EditIAPPI/$1';
$route['user/iappi/(:any)/hapus'] = 'User/HapusIAPPI/$1';
$route['user/ap3i'] = 'User/DataAP3I';
$route['user/ap3i/tambah'] = 'User/TambahAP3I';
$route['user/ap3i/(:any)/edit'] = 'User/EditAP3I/$1';
$route['user/ap3i/(:any)/hapus'] = 'User/HapusAP3I/$1';
$route['user/balai_jasa'] = 'User/DataBalaiJasa';
$route['user/balai_jasa/tambah'] = 'User/TambahBalaiJasa';
$route['user/balai_jasa/(:any)/edit'] = 'User/EditBalaiJasa/$1';
$route['user/balai_jasa/(:any)/hapus'] = 'User/HapusBalaiJasa/$1';
$route['user/tenaga_kerja'] = 'User/DataTenagaKerja';
$route['user/tenaga_kerja/tambah'] = 'User/TambahTenagaKerja';
$route['user/tenaga_kerja/tambah/form'] = 'User/FormTambahTenagaKerja';
//$route['user/tenaga_kerja/(:any)/edit'] = 'User/EditTenagaKerja/$1';
$route['user/tenaga_kerja/(:any)/edit/datadiri'] = 'User/EditDataDiriTenagaKerja/$1';
$route['user/tenaga_kerja/(:any)/edit/pendidikan'] = 'User/EditPendidikanTenagaKerja/$1';
$route['user/tenaga_kerja/(:any)/edit/lampiran'] = 'User/EditLampiranTenagaKerja/$1';
$route['user/tenaga_kerja/(:any)/edit/form'] = 'User/FormEditTenagaKerja/$1';
$route['user/tenaga_kerja/(:any)/hapus'] = 'User/HapusTenagaKerja/$1';
$route['user/konfigurasi'] = 'User/Konfigurasi';

$route['tenaker/utusan/tambah'] = 'Tenaker/TambahUtusan';
$route['tenaker/proyek/tambah'] = 'Tenaker/TambahProyek';
$route['tenaker/jabatan/tambah'] = 'Tenaker/TambahJabatan';

$route['profil'] = 'Tenaker/ProfilTenaker';
$route['profil/edit'] = 'Tenaker/EditProfilTenaker';

$route['bimtek/tambah'] = 'Bimtek/TambahBimtek';
$route['bimtek/(:any)'] = 'Bimtek/DetailBimtek/$1';
$route['bimtek/(:any)/daftar'] = 'Bimtek/DaftarBimtek/$1';
$route['bimtek/(:any)/edit'] = 'Bimtek/EditBimtek/$1';
$route['bimtek/(:any)/hapus'] = 'Bimtek/HapusBimtek/$1';
$route['bimtek/(:any)/ijazah'] = 'Bimtek/KirimIjazah/$1';
$route['bimtek/(:any)/kegiatan/tambah'] = 'Bimtek/TambahKegiatanBimtek/$1';
$route['bimtek/(:any)/kegiatan/tambah/form'] = 'Bimtek/FormTambahKegiatanBimtek/$1';
$route['bimtek/(:any)/kegiatan/edit'] = '';
$route['bimtek/(:any)/kegiatan/edit/form'] = 'Bimtek/FormEditKegiatanBimtek/$1';
$route['bimtek/(:any)/nilai/atur'] = 'Bimtek/AturNilaiBimtek/$1';
$route['bimtek/(:any)/nilai/edit'] = 'Bimtek/EditNilaiBimtek/$1';
$route['bimtek/(:any)/nilai/atur/form'] = 'Bimtek/FormAturNilaiBimtek/$1';